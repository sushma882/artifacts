IF NOT EXISTS (SELECT *  FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cfg_alert_severity_threshold]') AND type in (N'U'))
  CREATE TABLE cfg_alert_severity_threshold (
    alert_type         varchar(60) NOT NULL,
    medium_threshold   int,
    high_threshold     int,
    critical_threshold int,
    create_date        datetime,
    create_user_id     varchar(20),
    update_date        datetime,
    update_user_id     varchar(20),
  CONSTRAINT PK_cfg_alert_severity_threshold PRIMARY KEY (alert_type) WITH (FILLFACTOR = 80))
go

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cfg_code_value]') AND type in (N'U'))
  CREATE TABLE cfg_code_value(
		code_id           numeric(19,0)   NOT NULL IDENTITY (0,1),
		category          varchar(30)     NOT NULL,
		config_name       varchar(255)    NOT NULL,
		code              varchar(255)    NOT NULL,
		sub_category      varchar(30)     NOT NULL,
		description       varchar(255),
		sort_order        int,
		data1             varchar(120),
		data2             varchar(120),
		data3             varchar(120),
		create_date       datetime,
		create_user_id    varchar(20),
		update_date       datetime,
		update_user_id	 varchar(20),
  CONSTRAINT PK_cfg_code_value PRIMARY KEY (code_id),
  CONSTRAINT uq_cfg_code_value UNIQUE (category, config_name, sub_category, code) WITH (FILLFACTOR = 80))
go

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cfg_config_properties]') AND type in (N'U'))
  CREATE TABLE cfg_config_properties(
	  property_id        numeric(19,0)   NOT NULL IDENTITY (0,1),
	  category           varchar(30)     NOT NULL,
	  config_name        varchar(255)    NOT NULL,
	  sub_category       varchar(30)     NOT NULL,
	  description        varchar(max),
	  short_description  varchar(max),
	  sort_order         int,
	  create_date        datetime,
	  create_user_id     varchar(20),
	  update_date        datetime,
	  update_user_id     varchar(20),
  CONSTRAINT PK_cfg_config_properties PRIMARY KEY (property_id),
  CONSTRAINT uq_cfg_config_properties UNIQUE (category, config_name, sub_category) WITH (FILLFACTOR = 80))
go

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cfg_deployed_session_journal]') AND type in (N'U'))
  CREATE TABLE cfg_deployed_session_journal(
    deployed_journal_id   numeric(19,0)   NOT NULL IDENTITY (0,1),
    session_id            numeric(19,0)   NOT NULL,
    session_journal_id    numeric(19,0)   NOT NULL,
    config_category       varchar(30),
    config_type           varchar(30),
    config_subtype        varchar(30),
    statuscode            varchar(30),
    action_type           varchar(30),
    object_id             varchar(255),
    config_data           varchar(max),
    target_location_id    varchar(max),
    config_path           varchar(120),
    organization_id       int             DEFAULT 1,
    rtl_loc_id            int,
    target_effective_date datetime,
    last_activity_date    datetime,
    comment_text          varchar(255),
    create_date           datetime,
    create_user_id        varchar(20),
    update_date           datetime,
    update_user_id        varchar(20),
  CONSTRAINT PK_cfg_deployed_session_journal PRIMARY KEY (deployed_journal_id) WITH (FILLFACTOR = 80))
go

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cfg_deployment]') AND type in (N'U'))
  CREATE TABLE cfg_deployment(
    deployment_id       numeric(19,0)   NOT NULL IDENTITY (0,1),
    deployment_plan_id  numeric(19,0)   NOT NULL,
    organization_id     int             DEFAULT 1,
    rtl_loc_id          int             NOT NULL,
    download_id         varchar(254),
    status_code         varchar(30),
    create_date         datetime,
    create_user_id      varchar(20),
    update_date         datetime,
    update_user_id      varchar(20),
  CONSTRAINT PK_cfg_deployment PRIMARY KEY (deployment_id) WITH (FILLFACTOR = 80))
go

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cfg_deployment_event_log]') AND type in (N'U'))
  CREATE TABLE cfg_deployment_event_log (
    log_id            numeric(19,0)   NOT NULL IDENTITY (0,1),
    deployment_id     numeric(19,0)   NOT NULL,
    organization_id   int             DEFAULT 1,
    rtl_loc_id        int             NOT NULL,
    wkstn_id          bigint,
    log_level         varchar(20),
    log_timestamp     datetime,
    source            varchar(254),
    thread_name       varchar(254),
    logger_category   varchar(254),
    log_message       varchar(max),
    create_date       datetime,
    create_user_id    varchar(20),
    update_date       datetime,
    update_user_id    varchar(20),
  CONSTRAINT PK_cfg_deployment_event_log PRIMARY KEY (log_id) WITH (FILLFACTOR = 80))
go

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cfg_deployment_payload]') AND type in (N'U'))
  CREATE TABLE cfg_deployment_payload(
    deployment_payload_id   numeric(19,0)   NOT NULL IDENTITY (0,1),
    deployment_plan_id      numeric(19,0)   NOT NULL,
    session_id              numeric(19,0),
    target_location_id      int,
    config_type             varchar(30),
    file_blob               varbinary(MAX),
    file_name               varchar(60),
    deployment_date         datetime,
    generation_date         datetime,
    create_date             datetime,
    create_user_id          varchar(20),
    update_date             datetime,
    update_user_id          varchar(20),
  CONSTRAINT PK_cfg_deployment_payload_id PRIMARY KEY (deployment_payload_id) WITH (FILLFACTOR = 80))
go

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cfg_deployment_plan]') AND type in (N'U'))
  CREATE TABLE cfg_deployment_plan(
    session_id          numeric(19,0)   NOT NULL,
    deployment_plan_id  numeric(19,0)   NOT NULL IDENTITY (0,1),
    status_code         varchar(30),
    deploy_category     varchar(30),
    deploy_date         datetime,
    generation_date     datetime,
    organization_id     int,
    target_location     varchar(max),
    comment_text        varchar(max),
    create_date         datetime,
    create_user_id      varchar(20),
    update_date         datetime,
    update_user_id      varchar(20),
  CONSTRAINT PK_cfg_deployment_plan PRIMARY KEY (deployment_plan_id) WITH (FILLFACTOR = 80))
go

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cfg_deployment_plan_event_log]') AND type in (N'U'))
  CREATE TABLE cfg_deployment_plan_event_log (
    log_id              numeric(19,0)   NOT NULL IDENTITY (0,1),
    deployment_plan_id  numeric(19,0)   NOT NULL,
    log_level           varchar(20),
    log_timestamp       datetime,
    log_message         varchar(max),
    create_date         datetime,
    create_user_id      varchar(20),
    update_date         datetime,
    update_user_id      varchar(20),
  CONSTRAINT PK_cfg_dplymnt_plan_evnt_log PRIMARY KEY (log_id) WITH (FILLFACTOR = 80))
go

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cfg_menu_config]') AND type in (N'U'))
  CREATE TABLE cfg_menu_config(
    menu_id                   numeric(19,0)   NOT NULL IDENTITY (0,1),
    category                  varchar(60)     NOT NULL,
    menu_name                 varchar(100)    NOT NULL,
    parent_menu_name          varchar(100),
    config_type               varchar(120),
    title                     varchar(60),
    menu_type                 varchar(30),
    sort_order                int,
    view_id                   varchar(200),
    action_expression         varchar(200),
    active_flag               bit,
    propagation               varchar(30),
    security_privilege        varchar(30),
    custom_datasource_class   varchar(200),
    custom_datasource_method  varchar(30),
    menu_small_icon           varchar(254),
    menu_separator            bit             DEFAULT 0,
    create_date               datetime,
    create_user_id            varchar(20),
    update_date               datetime,
    update_user_id            varchar(20),
  CONSTRAINT PK_cfg_menu_config PRIMARY KEY (menu_id),
  CONSTRAINT uq_cfg_menu_config UNIQUE (category, menu_name) WITH (FILLFACTOR = 80))
go

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cfg_privilege]') AND type in (N'U'))
  CREATE TABLE cfg_privilege(
    privilege_id            varchar(30)   NOT NULL,
    privilege_desc          varchar(255),
    short_desc              varchar(60),
    category                varchar(30),
    has_attributes_flag     bit           NOT NULL, 
    create_date             datetime,
    create_user_id          varchar(20),
    update_date             datetime,
    update_user_id          varchar(20),
  CONSTRAINT PK_cfg_privileges PRIMARY KEY (privilege_id) WITH (FILLFACTOR = 80))
go

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cfg_resource]') AND type in (N'U'))
  CREATE TABLE cfg_resource (
     bundle_name            varchar(60)   NOT NULL,
     locale                 varchar(6)    NOT NULL,
     data                   varchar(max),
     create_date            datetime,
     create_user_id         varchar(20),
     update_date            datetime,
     update_user_id         varchar(20),
   CONSTRAINT PK_cfg_resource_bundle PRIMARY KEY (bundle_name, locale) WITH (FILLFACTOR = 80));
go

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cfg_role]') AND type in (N'U'))
  CREATE TABLE cfg_role(
    role_id           varchar(30)   NOT NULL,
    role_desc         varchar(255),
    system_role_flag  bit           DEFAULT 0 NOT NULL,
    create_date       datetime,
    create_user_id    varchar(20),
    update_date       datetime,
    update_user_id    varchar(20),
  CONSTRAINT PK_cfg_role PRIMARY KEY (role_id) WITH (FILLFACTOR = 80))
go

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cfg_role_privilege]') AND type in (N'U'))
  CREATE TABLE cfg_role_privilege(
    role_id               varchar(30)   NOT NULL,
    privilege_id          varchar(30)   NOT NULL,
    has_attributes_flag   bit           NOT NULL,
    allow_create_flag     bit,
    allow_read_flag       bit,
    allow_update_flag     bit,
    allow_delete_flag     bit,
    create_date           datetime,
    create_user_id        varchar(20),
    update_date           datetime,
    update_user_id        varchar(20),
  CONSTRAINT PK_cfg_role_privilege PRIMARY KEY (role_id, privilege_id) WITH (FILLFACTOR = 80))
go

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cfg_session]') AND type in (N'U'))
  CREATE TABLE cfg_session(
    session_id          numeric(19,0)   NOT NULL IDENTITY (0,1),
    statuscode          varchar(30),
    organization_id     int,
    typcode             varchar(30),
    deployed_user_id    varchar(30),
    description         varchar(255),
    target_location_id  varchar(max),
    create_date         datetime,
    create_user_id      varchar(20),
    update_date         datetime,
    update_user_id      varchar(20),
  CONSTRAINT PK_cfg_session PRIMARY KEY (session_id) WITH (FILLFACTOR = 80))
go

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cfg_session_journal]') AND type in (N'U'))
  CREATE TABLE cfg_session_journal(
    session_id            numeric(19,0)   NOT NULL,
    session_journal_id    numeric(19,0)   NOT NULL IDENTITY (0,1),
    config_category       varchar(30),
    config_type           varchar(30),
    config_subtype        varchar(30),
    statuscode            varchar(30),
    action_type           varchar(30),
    object_id             varchar(255),
    config_data           varchar(max),
    target_location_id    varchar(max),
    config_path           varchar(120),
    organization_id       int             DEFAULT 1,
    rtl_loc_id            int,
    target_effective_date datetime,
    last_activity_date    datetime,
    comment_text          varchar(255),
    create_date           datetime,
    create_user_id        varchar(20),
    update_date           datetime,
    update_user_id        varchar(20),
  CONSTRAINT PK_cfg_session_journal PRIMARY KEY (session_journal_id) WITH (FILLFACTOR = 80))
go

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cfg_support_dataload_journal]') AND type in (N'U'))
  CREATE TABLE cfg_support_dataload_journal(
    cfg_dataload_journal_id numeric(19,0)   NOT NULL IDENTITY (0,1),
    organization_id         int             NOT NULL,
    dataload_id             varchar(20),
    rtl_loc_id              int             NOT NULL,
    wkstn_id                bigint,
    status                  varchar(10)     NOT NULL,
    success_count           int,
    failure_count           int,
    details                 varchar(max),
    create_date             datetime,
    create_user_id          varchar(20),
    update_date             datetime,
    update_user_id          varchar(20),
  CONSTRAINT PK_cfg_support_dataload_journal PRIMARY KEY (cfg_dataload_journal_id) WITH (FILLFACTOR = 80))
go

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cfg_support_offline_journal]') AND type in (N'U'))
  CREATE TABLE cfg_support_offline_journal(
    cfg_offline_journal_id  numeric(19,0)   NOT NULL IDENTITY (0,1),
    organization_id         int             NOT NULL,
    rtl_loc_id              int             NOT NULL,
    wkstn_id                bigint,
    offline_date            datetime        NOT NULL,
    offline_timestamp       datetime        NOT NULL,
    datasource              varchar(80),
    network_scope           varchar(30),
    details                 varchar(255),
    create_date             datetime,
    create_user_id          varchar(20),
    update_date             datetime,
    update_user_id          varchar(20),
  CONSTRAINT PK_cfg_support_offline_journal PRIMARY KEY (cfg_offline_journal_id) WITH (FILLFACTOR = 80))
go

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cfg_user]') AND type in (N'U'))
  CREATE TABLE cfg_user(
    user_name       varchar(30)   NOT NULL,
    first_name      varchar(30),
    last_name       varchar(30),
    role_id         varchar(30),
    locale          varchar(30),
    organization_id int,
	  org_code        varchar(30),
	  org_value       varchar(30),
    create_date     datetime,
    create_user_id  varchar(20),
    update_date     datetime,
    update_user_id  varchar(20),
  CONSTRAINT PK_cfg_user PRIMARY KEY (user_name) WITH (FILLFACTOR = 80))
go

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cfg_user_password]') AND type in (N'U'))
  CREATE TABLE cfg_user_password(
    user_name       varchar(30)   NOT NULL,
    password_id     numeric(19,0) NOT NULL IDENTITY (0,1),
    password        varchar(255),
    effective_date  datetime,
    create_date     datetime,
    create_user_id  varchar(20),
    update_date     datetime,
    update_user_id  varchar(20),
  CONSTRAINT PK_cfg_user_password PRIMARY KEY (password_id) WITH (FILLFACTOR = 80))
go

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[loc_rtl_loc_collection]') AND type in (N'U'))
  CREATE TABLE loc_rtl_loc_collection(
    collection_id     numeric(19,0)   NOT NULL IDENTITY (0,1),
    collection_name   varchar(60)     NOT NULL,
    delete_flag       bit             DEFAULT 0 NOT NULL,
    description       varchar(256),
    sort_order        int,
    organization_id   int             DEFAULT 1,
    create_date       datetime,
    create_user_id    varchar(20),
    update_date       datetime,
    update_user_id    varchar(20),
  CONSTRAINT PK_loc_rtl_loc_collection PRIMARY KEY (collection_id) WITH (FILLFACTOR = 80))
go

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[loc_rtl_loc_collection_element]') AND type in (N'U'))
  CREATE TABLE loc_rtl_loc_collection_element(
    element_id        numeric(19,0)   NOT NULL IDENTITY (0,1),
    collection_id     numeric(19,0)   NOT NULL,
    org_scope_code    varchar(60)     NOT NULL,
    organization_id   int             DEFAULT 1,
    create_date       datetime,
    create_user_id    varchar(20),
    update_date       datetime,
    update_user_id    varchar(20),
  CONSTRAINT PK_loc_rtl_loc_collection_element PRIMARY KEY (element_id) WITH (FILLFACTOR = 80))
go

--Tasks and messages
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cfg_sequence]') AND type in (N'U'))
CREATE TABLE cfg_sequence(
    organization_id  int              NOT NULL,
    rtl_loc_id       int              NOT NULL,
    wkstn_id         int              NOT NULL,
    sequence_id      varchar(255)     NOT NULL,
    sequence_nbr     numeric(19,0)    NOT NULL,
    create_date      DATETIME,
    create_user_id   VARCHAR(30),
    update_date      DATETIME,
    update_user_id   VARCHAR(30),
CONSTRAINT PK_cfg_sequence PRIMARY KEY (organization_id, rtl_loc_id, wkstn_id, sequence_id) WITH (FILLFACTOR = 80));
go

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cfg_sequence_part]') AND type in (N'U'))
CREATE TABLE cfg_sequence_part(
    organization_id  int              NOT NULL,
    sequence_id      varchar(255)     NOT NULL,
    prefix           varchar(30),
    suffix           varchar(30),
    encode_flag      bit,
    check_digit_algo varchar(30),
    numeric_flag     bit,
    pad_length       int,
    pad_character    varchar(2),
    initial_value    int,
    max_value        numeric(10,0),
    value_increment  int,
    include_store_id bit,
    store_pad_length int,
    include_wkstn_id bit,
    wkstn_pad_length int,
    create_date      DATETIME,
    create_user_id   VARCHAR(30),
    update_date      DATETIME,
    update_user_id   VARCHAR(30),
  CONSTRAINT PK_cfg_sequence_part PRIMARY KEY (organization_id, sequence_id) WITH (FILLFACTOR = 80));
go




-- CFG_PRIVILEGE
DELETE FROM cfg_privilege WHERE category = 'Menu';
--INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Menu', 'MENU_EJ_CATEGORY', 'Electronic Journal Menu', 'Electronic Journal Menu', 0);
--INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Menu', 'MENU_ADMIN_CATEGORY', 'System Tools Menu', 'System Tools Menu', 0);
--INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Menu', 'MENU_SUPPORT_CATEGORY', 'Xstore System Info Menu', 'Xstore System Info Menu', 0);
--INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Menu', 'MENU_CONFIGURATOR_CATEGORY', 'Configurator Menu', 'Configurator Menu', 0);
--INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Menu', 'MENU_REPORTS_CATEGORY', 'Reports Menu', 'Repoorts Menu', 0);
GO

DELETE FROM cfg_privilege WHERE category = 'Support';
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Support', 'SPT_EJOURNAL', 'Electronic Journal', 'Electronic Journal', 0);
--INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Support', 'SPT_DATALOADER_STATUS', 'Review Dataloader Status', 'Review Dataloader Status', 0);
--INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Support', 'SPT_OFFLINE_STORES_REGISTERS', 'Review Offline Stores Registers', 'Review Offline Stores Registers', 0);
--INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Support', 'SPT_XSTORE_VERSIONS_RUNNING', 'Review Xstore Versions Running', 'Review XStore Versions Running', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Support', 'SPT_VIEW_SUPPORT_DASHBOARD', 'View Support Dashboard', 'View Support Dashboard', 0);
GO

DELETE FROM cfg_privilege WHERE category = 'AdminSecurity';
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('AdminSecurity', 'ADMN_ADD_EDIT_USERS', 'Add/Edit Users', 'Add/Edit Users', 1);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('AdminSecurity', 'ADMN_USER_ROLES', 'Maintain User Roles', 'Maintain User Roles', 1);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('AdminSecurity', 'ADMN_VIEW_ADMIN_VERSION', 'Display Xcenter-Admin Version', 'Display Xcenter-Admin Version', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('AdminSecurity', 'ADMN_AVAILABLE_LOCALES', 'Maintain Available Locales', 'Maintain Available Locales', 1);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('AdminSecurity', 'ADMN_ORG_HIERARCHY', 'Maintain Organizational Hierarchy', 'Maintain Organizational Hierarchy', 1);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('AdminSecurity', 'ADMN_ORG_IDS', 'Maintain Organization IDs', 'Maintain Organization IDs', 1);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('AdminSecurity', 'ADMN_STORE_COLLECTIONS', 'Manage Store Collections', 'Manage Store Collections', 1);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('AdminSecurity', 'ADMN_CONFIG_PATH', 'Configuration Path', 'Configuration Path', 1);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('AdminSecurity', 'ADMN_XCENTER_ADMIN_CONFIG', 'Xcenter-Admin Configuration', 'Xcenter-Admin Configuration', 1);
GO

DELETE FROM cfg_privilege WHERE category = 'Configurator';
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Configurator', 'CFG_CODE', 'Code Value', 'Code Value', 1);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Configurator', 'CFG_CURRENCY_EXCHANGE', 'Currency Exchange', 'Currency Exchange', 1);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Configurator', 'CFG_DISCOUNTS', 'Discounts', 'Discounts', 1);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Configurator', 'CFG_EMPLOYEE', 'Employee', 'Employee', 1);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Configurator', 'CFG_EMPLOYEE_MESSAGE', 'Employee Message', 'Employee Message', 1);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Configurator', 'CFG_EMPLOYEE_TASK', 'Employee Task', 'Employee Task', 1);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Configurator', 'CFG_FILE_TRANSFER', 'File Transfer', 'File Transfer', 1);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Configurator', 'CFG_ITEM', 'Item', 'Item', 1);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Configurator', 'CFG_ITEM_PRICING', 'Item Pricing', 'Item Pricing', 1);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Configurator', 'CFG_MENU_CONFIG', 'Menu', 'Menu', 1);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Configurator', 'CFG_ORGANIZATION_HIERARCHY', 'Organizational Hierarchy', 'Organizational Hierarchy', 1);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Configurator', 'CFG_REASON_CODE', 'Reason Codes', 'Reason Codes', 1);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Configurator', 'CFG_RECEIPT_CONFIG', 'Receipt', 'Receipt', 1);
--INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Configurator', 'CFG_RECEIPT_TEXT', 'Receipt Text', 'Receipt Text', 1);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Configurator', 'CFG_SECURITY_GROUP', 'Security Group', 'Secrity Group', 1);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Configurator', 'CFG_SECURITY_PERMISSION', 'Security Permission', 'Security Permission', 1);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Configurator', 'CFG_SECURITY_PRIVILEGE', 'Security Privilege', 'Security Privilege', 1);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Configurator', 'CFG_STORES', 'Stores', 'Stores', 1);
--INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Configurator', 'CFG_SYSTEM_CONFIGURATION', 'System Configuration', 'System Configuration', 1);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Configurator', 'CFG_TAX_AUTHORITY', 'Tax Authority', 'Tax Authority', 1);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Configurator', 'CFG_TAX_GROUP', 'Tax Group', 'Tax Group', 1);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Configurator', 'CFG_TAX_GROUP_RULE', 'Tax Group Rule', 'Tax Group Rule', 1);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Configurator', 'CFG_TAX_LOCATION', 'Tax Location', 'Tax Location', 1);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Configurator', 'CFG_TENDER', 'Tenders', 'Tenders', 1);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Configurator', 'CFG_TENDER_AVAILABILITY', 'Tender Availability', 'Tender Availability', 1);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Configurator', 'CFG_TENDER_USER_SETTINGS', 'Tender User Settings', 'Tender User Settings', 1);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Configurator', 'CFG_VIEW_DEPLOYED_JOURNALS', 'View Deployed Journals', 'View Deployed Journals', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Configurator', 'CFG_VENDOR', 'Vendor', 'Vendor', 1);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Configurator', 'CFG_NEW_SESSION', 'Create New Session', 'Create New Session', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Configurator', 'CFG_EDIT_SESSION', 'Edit Existing Sessions', 'Edit Existing Sessions', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Configurator', 'CFG_DEPLOYED_SESSION', 'View Deployed Sessions', 'View Deployed Sessions', 0);
GO

DELETE FROM cfg_privilege WHERE category = 'Reports';
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_BEST_SELLERS_ITEM', 'Best Sellers Item Report', 'Best Sellers Item Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_BEST_SELLERS_STYLE', 'Best Sellers Style Report', 'Best Sellers Style Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_CREDIT_CARD', 'Credit Card Report', 'Credit Card Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_CUSTOMER_LIST', 'Customer List Report', 'Customer List Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_DAILY_SALES_CASH', 'Daily Sales & Cash Report', 'Daily Sales & Cash Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_DAILY_SALES', 'Daily Sales Report', 'Daily Sales Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_DAILY_SALES_TOTAL', 'Daily Sales Total Report', 'Daily Sales Total Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_EMPLOYEE_PERFORMANCE', 'Employee Performance Report', 'Employee Performance Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_FLASH_SALES_DEPARTMENT', 'Flash Sales Department Report', 'Flash Sales Department Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_FLASH_SALES_EMPLOYEE', 'Flash Sales Employee Report', 'Flash Sales Employee Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_FLASH_SALES_HOUR', 'Flash Sales By Hour Report', 'Flash Sales By Hour Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_FLASH_SALES_SUMMARY', 'Flash Sales Summary Report', 'Flash Sales Summary Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_GIFT_CERTIFICATE', 'Gift Certificate Report', 'Gift Certificate Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_ITEM_LIST', 'Item List Report', 'Item List Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_EMP_TASKS', 'Employee Tasks Report', 'Employee Tasks Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_JOURNAL', 'Journal Report', 'Journal Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_LAYAWAY_ACCT_ACTIVITY_DTL', 'Layaway Account Activity Detail Report', 'Layaway Account Activity Detail Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_LAYAWAY_ACCT_ACTIVITY_SUM', 'Layaway Account Activity Summary Report', 'Layaway Account Activity Summary Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_LAYAWAY_AGING_DETAIL', 'Layaway Aging Detail Report', 'Layaway Aging Detail Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_LAYAWAY_AGING_SUMMARY', 'Layaway Aging Summary Report', 'Layaway Aging Summary Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_LINE_VOID', 'Line Void Report', 'Line Void Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_NO_SALE', 'No Sale Report', 'No Sale Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_POST_VOID_DETAIL', 'Post Void Detail Report', 'Post Void Detail Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_POST_VOID_SUMMARY', 'Post Void Summary Report', 'Post Void Summary Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_PRICE_CHANGE', 'Price Change Report', 'Price Change Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_PRICE_OVERRIDE', 'Price Override Report', 'Price Override Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_RECEIVING_EXCEPTION', 'Receiving Exception Report', 'Receiving Exception Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_RETURNED_MERCHANDISE', 'Returned Merchandise Report', 'Returned Merchandise Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_SALES_DEPT_EMPLOYEE', 'Sales Department Employee Report', 'Sales Department Employee Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_SALES_DEPARTMENT', 'Sales Department Report', 'Sales Department Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_SALES_HOUR_ANALYSIS', 'Sales By Hour Analysis Report', 'Sales By Hour Analysis Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_SALES_HOUR', 'Sales By Hour Report', 'Sales By Hour Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_SHIPPING_EXCEPTION', 'Shipping Exception Report', 'Shipping Exception Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_SPECIAL_ORDERS', 'Special Orders Report', 'Special Orders Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_STORE_LOCATIONS', 'Store Locations Report', 'Store Locations Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_SUSPENDED_TRANS_DETAIL', 'Suspended Transaction Detail Report', 'Suspended Transaction Detail Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_SUSPENDED_TRANS_SUMMARY', 'Suspended Transaction Summary Report', 'Suspended Transaction Summary Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_TAX_EXEMPTION', 'Tax Exemption Report', 'Tax Exemption Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_TRANS_CANCEL_DETAIL', 'Transaction Cancel Detail Report', 'Transaction Cancel Detail Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_TRANS_CANCEL_SUMMARY', 'Transaction Cancel Summary Report', 'Transaction Cancel Summary Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_WORST_SELLERS_ITEM', 'Worst Sellers Item Report', 'Worst Sellers Item Report', 0);
INSERT INTO cfg_privilege (category, privilege_id, privilege_desc, short_desc, has_attributes_flag) VALUES ('Reports', 'RPT_WORST_SELLERS_STYLE', 'Worst Sellers Style Report', 'Worst Sellers Style Report', 0);
GO

-- **************************************************** --
-- * Manual referential-integrity cleanup: keep the   * --
-- * cfg_role_privilege table consistent with the     * --
-- * actual list of privileges from cfg_privilege.    * --
-- * Always do this AFTER cfg_privileges are          * --
-- * (re)created!
-- **************************************************** --
DELETE FROM cfg_role_privilege WHERE privilege_id NOT IN (SELECT privilege_id FROM cfg_privilege)
GO

-- CFG_MENU_CONFIG
DELETE FROM cfg_menu_config WHERE category = 'REDIRECT_MENU_ACTION';
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('REDIRECT_MENU_ACTION', 'CHANGE_PASSWORD', NULL, NULL, NULL, 'REDIRECT_ACTION', 10, '/changePassword.xhtml', NULL, 1, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('REDIRECT_MENU_ACTION', 'DEPLOYED_SESSION_LIST', NULL, NULL, NULL, 'REDIRECT_ACTION', 10, '/configurator/deployedSessionList.xhtml', NULL, 1, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('REDIRECT_MENU_ACTION', 'ELECTRONIC_JOURNAL_RESULT', NULL, NULL, NULL, 'REDIRECT_ACTION', 10, '/ej/ejSearch.xhtml', NULL, 1, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('REDIRECT_MENU_ACTION', 'INDEX', NULL, NULL, NULL, 'REDIRECT_ACTION', 10, '/index.xhtml', NULL, 1, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('REDIRECT_MENU_ACTION', 'NON_AUTHORIZED', NULL, NULL, NULL, 'REDIRECT_ACTION', 10, '/notAuthorized.xhtml', NULL, 1, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('REDIRECT_MENU_ACTION', 'OPEN_SESSION_LIST', NULL, NULL, NULL, 'REDIRECT_ACTION', 10, '/configurator/openSessionList.xhtml', NULL, 1, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('REDIRECT_MENU_ACTION', 'REPORT_PARAMETER_ENTRY', NULL, NULL, NULL, 'REDIRECT_ACTION', 10, '/reports/reportParameterEntry.xhtml', NULL, 1, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('REDIRECT_MENU_ACTION', 'SELECT_RECEIPT_TYPE', NULL, NULL, NULL, 'REDIRECT_ACTION', 10, '/ej/selectReceiptType.xhtml', NULL, 1, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('REDIRECT_MENU_ACTION', 'SESSION_DEPLOYMENT', NULL, NULL, NULL, 'REDIRECT_ACTION', 10, '/configurator/sessionDeployment.xhtml', NULL, 1, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('REDIRECT_MENU_ACTION', 'SESSION_DEPLOYMENT_CONFIRMATION', NULL, NULL, NULL, 'REDIRECT_ACTION', 10, '/configurator/sessionDeploymentConfirmation.xhtml', NULL, 1, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('REDIRECT_MENU_ACTION', 'SESSION_DEPLOYMENT_HISTORY', NULL, NULL, NULL, 'REDIRECT_ACTION', 10, '/configurator/deploymentHistory.xhtml', NULL, 1, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('REDIRECT_MENU_ACTION', 'SESSION_DEPLOYMENT_LIST', NULL, NULL, NULL, 'REDIRECT_ACTION', 10, '/configurator/deploymentList.xhtml', NULL, 1, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('REDIRECT_MENU_ACTION', 'SESSION_DEPLOYMENT_PLAN_DETAIL', NULL, NULL, NULL, 'REDIRECT_ACTION', 10, '/configurator/deploymentPlanDetails.xhtml', NULL, 1, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('REDIRECT_MENU_ACTION', 'SESSION_DEPLOYMENT_TRIAL', NULL, NULL, NULL, 'REDIRECT_ACTION', 10, '/configurator/sessionDeploymentTrial.xhtml', NULL, 1, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('REDIRECT_MENU_ACTION', 'SESSION_JOURNAL_DETAIL', NULL, NULL, NULL, 'REDIRECT_ACTION', 10, '/configurator/sessionJournalDetail.xhtml', NULL, 1, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('REDIRECT_MENU_ACTION', 'SESSION_JOURNAL_VIEW', NULL, NULL, NULL, 'REDIRECT_ACTION', 10, '/configurator/sessionJournalView.xhtml', NULL, 1, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('REDIRECT_MENU_ACTION', 'SESSION_REDEPLOYMENT', NULL, NULL, NULL, 'REDIRECT_ACTION', 10, '/configurator/sessionReDeployment.xhtml', NULL, 1, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('REDIRECT_MENU_ACTION', 'SESSION_RETRY_DEPLOYMENT', NULL, NULL, NULL, 'REDIRECT_ACTION', 10, '/configurator/sessionRetryDeployment.xhtml', NULL, 1, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('REDIRECT_MENU_ACTION', 'SESSION_TRIAL_DEPLOYMENT_CONFIRMATION', NULL, NULL, NULL, 'REDIRECT_ACTION', 10, '/configurator/sessionDeploymentTrialConfirmation.xhtml', NULL, 1, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('REDIRECT_MENU_ACTION', 'STORE_FOR_VERSIONCHEKER', NULL, NULL, NULL, 'REDIRECT_ACTION', 10, '/support/versions/storesForVersion.xhtml', NULL, 1, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('REDIRECT_MENU_ACTION', 'VIEW_RECEIPT', NULL, NULL, NULL, 'REDIRECT_ACTION', 10, '/ej/viewReceipt.xhtml', NULL, 1, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('REDIRECT_MENU_ACTION', 'VIEW_REPORT', NULL, NULL, NULL, 'REDIRECT_ACTION', 10, '/reports/viewReport.xhtml', NULL, 1, NULL, NULL, NULL, NULL, NULL, 0);
GO

DELETE FROM cfg_menu_config WHERE category = 'MAIN_MENU' and parent_menu_name IS NULL;
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'REPORTS_MENU', NULL, 'REPORTS', '_menuReports', 'LINK', 10, NULL, NULL, 1, NULL, NULL, NULL, NULL, '/images/nav_icon_reporting_off.gif', 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'ELECTRONIC_JOURNAL', NULL, 'ELECTRONIC_JOURNAL', '_menuEj', 'LINK', 20, NULL, NULL, 1, 'join', NULL, NULL, NULL, '/images/nav_icon_search_off.gif', 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'CONFIGURATOR_MENU', NULL, 'CONFIGURATOR', '_enterConfigurator', 'LINK', 30, NULL, NULL, 1, NULL, NULL, NULL, NULL, '/images/nav_icon_content_off.gif', 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'SUPPORT_MENU', NULL, 'SUPPORT', '_xstoreInformationSupport', 'LINK', 40, NULL, NULL, 1, 'join', NULL, NULL, NULL, '/images/nav_icon_prodcatalog_off.gif', 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'XCENTER_ADMIN_MENU', NULL, 'ADMIN', '_systemToolPanelTitle', 'LINK', 50, NULL, NULL, 1, 'join', NULL, NULL, NULL, '/images/nav_icon_systemtools_off.gif', 0);
GO

DELETE FROM cfg_menu_config WHERE category = 'MAIN_MENU' and parent_menu_name = 'ELECTRONIC_JOURNAL';
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'EJ_SEARCH', 'ELECTRONIC_JOURNAL', 'ELECTRONIC_JOURNAL', '_menuEj', 'ACTION', 20, NULL, '@{ejResetAction.execute()}', 1, 'join', 'SPT_EJOURNAL', NULL, NULL, NULL, 0);
GO

DELETE FROM cfg_menu_config WHERE category = 'MAIN_MENU' and parent_menu_name = 'CONFIGURATOR_MENU';
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'CONFIGURATOR_NEW_SESSIONS', 'CONFIGURATOR_MENU', NULL, '_newSession', 'ACTION', 10, NULL, '@{cfgAddNewSessionAction.execute()}', 1, NULL, 'CFG_NEW_SESSION', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'CONFIGURATOR_OPEN_SESSIONS', 'CONFIGURATOR_MENU', NULL, '_menuCfgOpenSessionList', 'LINK', 20, NULL, '@{cfgOpenSessionListDisplayAction.execute()}', 1, NULL, 'CFG_EDIT_SESSION', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'CONFIGURATOR_DEPLOYED_SESSIONS', 'CONFIGURATOR_MENU', NULL, '_menuCfgDeployedSessionList', 'LINK', 30, NULL, '@{cfgDeployedSessionListDisplayAction.execute()}', 1, NULL, 'CFG_DEPLOYED_SESSION', NULL, NULL, NULL, 1);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'CONFIGURATOR_FEATURE', 'CONFIGURATOR_MENU', NULL, '_menuCfgFeatures', 'LINK', 40, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'CONFIGURATOR_SESSION_JOURNAL', 'CONFIGURATOR_MENU', NULL, '_currentSessionChanges', 'LINK', 50, '/configurator/sessionJournalDetail.xhtml', NULL, 1, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'DEPLOY_SESSION', 'CONFIGURATOR_MENU', NULL, '_deployCurrentSession', 'LINK', 60, '/configurator/sessionDeployment.xhtml', NULL, 1, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'CLOSE_SESSION', 'CONFIGURATOR_MENU', NULL, '_closeSession', 'LINK', 70, NULL, '@{cfgSaveNExitSessionAction.execute(''OPEN_CONFIGURATOR_LIST'')}', 1, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'DEPLOYED_JOURNALS', 'CONFIGURATOR_MENU', NULL, '_deployedJournals', 'LINK', 80, '/configurator/deployedJournals.xhtml', NULL, 1, NULL, 'CFG_VIEW_DEPLOYED_JOURNALS', NULL, NULL, NULL, 0);
GO

DELETE FROM cfg_menu_config WHERE category = 'MAIN_MENU' and parent_menu_name = 'CONFIGURATOR_FEATURE';
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'REASON_CODE', 'CONFIGURATOR_FEATURE', 'REASON_CODE', '_reasonCode', 'LINK', 10, '/configurator/reasoncode/reasonCodeConfig.xhtml', NULL, 1, NULL, 'CFG_REASON_CODE', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'CODE', 'CONFIGURATOR_FEATURE', 'CODE', '_code', 'LINK', 20, '/configurator/code/codeValueConfig.xhtml', NULL, 1, NULL, 'CFG_CODE', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'ORG_HIERARCHY', 'CONFIGURATOR_FEATURE', 'ORG_HIERARCHY', '_organizationHierarchy', 'LINK', 30, '/configurator/orghierarchy/orghierarchyConfig.xhtml', NULL, 1, NULL, 'CFG_ORGANIZATION_HIERARCHY', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'DISCOUNT', 'CONFIGURATOR_FEATURE', 'DISCOUNT', '_discountConfigFeatureTitle', 'LINK', 40, '/configurator/discount/discountConfig.xhtml', NULL, 1, NULL, 'CFG_DISCOUNTS', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'RETAIL_LOCATION', 'CONFIGURATOR_FEATURE', 'RETAIL_LOCATION', '_retailLocationConfigFeatureTitle', 'LINK', 50, '/configurator/store/retailLocationConfig.xhtml', NULL, 1, NULL, 'CFG_STORES', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'ITEM_CONFIG', 'CONFIGURATOR_FEATURE', NULL, '_itemMenuName', 'LINK_GROUP', 70, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'SECURITY_CONFIG', 'CONFIGURATOR_FEATURE', NULL, '_security', 'LINK_GROUP', 80, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'TAX_CONFIG', 'CONFIGURATOR_FEATURE', NULL, '_tax', 'LINK_GROUP', 90, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'TENDER_CONFIG', 'CONFIGURATOR_FEATURE', NULL, '_tender', 'LINK_GROUP', 100, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'CURRENCY_EXCHANGE', 'CONFIGURATOR_FEATURE', 'CURRENCY_EXCHANGE', '_currencyExchange', 'LINK', 110, '/configurator/tender/currencyExchangeConfig.xhtml', NULL, 1, NULL, 'CFG_CURRENCY_EXCHANGE', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'VENDOR', 'CONFIGURATOR_FEATURE', 'VENDOR', '_vendor', 'LINK', 130, '/configurator/vendor/vendorConfig.xhtml', NULL, 1, NULL, 'CFG_VENDOR', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'EMPLOYEE', 'CONFIGURATOR_FEATURE', 'EMPLOYEE', '_employeeConfig', 'LINK', 140, '/configurator/employee/employeeConfig.xhtml', NULL, 1, NULL, 'CFG_EMPLOYEE', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'FILE_TRANSFER', 'CONFIGURATOR_FEATURE', 'FILE_TRANSFER', '_fileTransfer', 'LINK', 150, '/configurator/filetransfer/fileTransferConfig.xhtml', NULL, 1, NULL, 'CFG_FILE_TRANSFER', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'SYSTEM_CONFIG', 'CONFIGURATOR_FEATURE', 'SystemConfig', '_systemConfig', 'LINK', 980, '/configurator/system/systemConfig.xhtml', NULL, 0, NULL, 'CFG_SYSTEM_CONFIGURATION', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'MENU_CONFIG', 'CONFIGURATOR_FEATURE', 'MenuConfig', '_menuConfig', 'LINK', 990, '/configurator/menu/menuConfig.xhtml', NULL, 1, NULL, 'CFG_MENU_CONFIG', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'RECEIPT_CONFIG', 'CONFIGURATOR_FEATURE', 'RcptConfig', '_receiptConfig', 'LINK', 1000, '/configurator/receipt/receiptConfig.xhtml', NULL, 1, NULL, 'CFG_RECEIPT_CONFIG', NULL, NULL, NULL, 0);
GO

DELETE FROM cfg_menu_config WHERE category = 'MAIN_MENU' and parent_menu_name = 'ITEM_CONFIG';
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'ITEM', 'ITEM_CONFIG', 'ITEM', '_itemConfigFeatureTitle', 'LINK', 10, '/configurator/item/itemConfig.xhtml', NULL, 1, NULL, 'CFG_ITEM', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'ITEM_PRICES', 'ITEM_CONFIG', 'ITEM_PRICES', '_itemPricesConfigFeatureTitle', 'LINK', 20, '/configurator/itemprices/itemPricesConfig.xhtml', NULL, 1, NULL, 'CFG_ITEM_PRICING', NULL, NULL, NULL, 0);
GO

DELETE FROM cfg_menu_config WHERE category = 'MAIN_MENU' and parent_menu_name = 'SECURITY_CONFIG';
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'SECURITY_GROUP', 'SECURITY_CONFIG', 'SECURITY_GROUP', '_securityGroup', 'LINK', 10, '/configurator/security/securityGroupConfig.xhtml', NULL, 1, NULL, 'CFG_SECURITY_GROUP', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'SECURITY_PRIVILEGE', 'SECURITY_CONFIG', 'SECURITY_PRIVILEGE', '_securityPrivilege', 'LINK', 20, '/configurator/security/securityPrivilegeConfig.xhtml', NULL, 1, NULL, 'CFG_SECURITY_PRIVILEGE', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'SECURITY_PERMISSION', 'SECURITY_CONFIG', 'SECURITY_PERMISSION', '_securityPermission', 'LINK', 30, '/configurator/security/securityPermissionConfig.xhtml', NULL, 1, NULL, 'CFG_SECURITY_PERMISSION', NULL, NULL, NULL, 0);
GO

DELETE FROM cfg_menu_config WHERE category = 'MAIN_MENU' and parent_menu_name = 'TAX_CONFIG';
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'TAX_AUTHORITY', 'TAX_CONFIG', 'TAX_AUTHORITY', '_taxAuthority', 'LINK', 10, '/configurator/tax/taxAuthorityConfig.xhtml', NULL, 1, NULL, 'CFG_TAX_AUTHORITY', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'TAX_LOCATION', 'TAX_CONFIG', 'TAX_LOCATION', '_taxLocation', 'LINK', 20, '/configurator/tax/taxLocationConfig.xhtml', NULL, 1, NULL, 'CFG_TAX_LOCATION', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'TAX_GROUP', 'TAX_CONFIG', 'TAX_GROUP', '_taxGroup', 'LINK', 30, '/configurator/tax/taxGroupConfig.xhtml', NULL, 1, NULL, 'CFG_TAX_GROUP', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'TAX_GROUP_RULE', 'TAX_CONFIG', 'TAX_GROUP_RULE;TAX_RATE_RULE;TAX_RATE_RULE_OVERRIDE', '_taxGroupRule', 'LINK', 40, '/configurator/tax/taxGroupRuleConfig.xhtml', NULL, 1, NULL, 'CFG_TAX_GROUP_RULE', NULL, NULL, NULL, 0);
GO

DELETE FROM cfg_menu_config WHERE category = 'MAIN_MENU' and parent_menu_name = 'TENDER_CONFIG';
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'TENDER', 'TENDER_CONFIG', 'TENDER', '_tender', 'LINK', 10, '/configurator/tender/tenderConfig.xhtml', NULL, 1, NULL, 'CFG_TENDER', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'TENDER_USER_SETTINGS', 'TENDER_CONFIG', 'TENDER_USER_SETTINGS', '_tenderUserSettings', 'LINK', 20, '/configurator/tender/tenderUserSettingsConfig.xhtml', NULL, 1, NULL, 'CFG_TENDER_USER_SETTINGS', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'TENDER_AVAILABILITY', 'TENDER_CONFIG', 'TENDER_AVAILABILITY', '_tenderAvailability', 'LINK', 30, '/configurator/tender/tenderAvailabilityConfig.xhtml', NULL, 1, NULL, 'CFG_TENDER_AVAILABILITY', NULL, NULL, NULL, 0);
GO

DELETE FROM cfg_menu_config WHERE category = 'MAIN_MENU' and parent_menu_name = 'SUPPORT_MENU';
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'SUPPORT_DASHBOARD', 'SUPPORT_MENU', NULL, '_supportDashboard', 'LINK', 10, '/support/dashboard.xhtml', NULL, 1, NULL, 'SPT_VIEW_SUPPORT_DASHBOARD', NULL, NULL, NULL, 0);

DELETE FROM cfg_menu_config WHERE category = 'MAIN_MENU' and parent_menu_name = 'PROFILE_CONFIG';
--INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
--  VALUES ('MAIN_MENU', 'CONFIG_PROFILE', 'PROFILE_CONFIG', NULL, '_configProfileFeatureTitle', 'LINK', 10, '/admin/profile/configProfileConfig.xhtml', NULL, 1, NULL, 'ADMN_CONFIG_PROFILE', NULL, NULL, NULL, 0);
--INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
--  VALUES ('MAIN_MENU', 'ORG_HIERARCHY_LEVEL', 'PROFILE_CONFIG', NULL, '_orgHierarchyLevelFeatureTitle', 'LINK', 20, '/admin/orghierarchylevel/orgHierarchyLevelConfig.xhtml', NULL, 1, NULL, 'ADMN_ORG_HIERARCHY', NULL, NULL, NULL, 0);
--INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
--  VALUES ('MAIN_MENU', 'STORE_COLLECTION', 'PROFILE_CONFIG', NULL, '_storeCollectionTitle', 'LINK', 30, '/admin/storecollections/editStoreCollections.xhtml', NULL, 1, NULL, 'ADMN_STORE_COLLECTIONS', NULL, NULL, NULL, 0);
--INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
--  VALUES ('MAIN_MENU', 'CONFIG_PATH_GROUP', 'PROFILE_CONFIG', NULL, '_configPathGroupFeatureTitle', 'LINK', 40, '/admin/configpath/configPathGroupConfig.xhtml', NULL, 1, NULL, 'ADMN_CONFIG_PATH_GROUP', NULL, NULL, NULL, 0);
--INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
--  VALUES ('MAIN_MENU', 'CONFIG_PATH', 'PROFILE_CONFIG', NULL, '_configPathFeatureTitle', 'LINK', 50, '/admin/configpath/configPathConfig.xhtml', NULL, 1, NULL, 'ADMN_CONFIG_PATH', NULL, NULL, NULL, 0);

GO

DELETE FROM cfg_menu_config WHERE category = 'MAIN_MENU' and parent_menu_name = 'XCENTER_ADMIN_MENU';
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'ADMIN_CONFIG', 'XCENTER_ADMIN_MENU', NULL, '_adminConfigFeatureShortTitle', 'LINK', 10, '/admin/admin/adminConfig.xhtml', NULL, 1, NULL, 'ADMN_XCENTER_ADMIN_CONFIG', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'ADMIN_USER', 'XCENTER_ADMIN_MENU', NULL, '_userPanelTitle', 'LINK', 20, '/admin/user/userConfig.xhtml', NULL, 1, NULL, 'ADMN_ADD_EDIT_USERS', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'ADMIN_USER_ROLES', 'XCENTER_ADMIN_MENU', NULL, '_userRolePanelTitle', 'LINK', 25, '/admin/userrole/userRoleConfig.xhtml', NULL, 1, NULL, 'ADMN_USER_ROLES', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'STORE_COLLECTION', 'XCENTER_ADMIN_MENU', NULL, '_storeCollectionTitle', 'LINK', 30, '/admin/storecollections/editStoreCollections.xhtml', NULL, 1, NULL, 'ADMN_STORE_COLLECTIONS', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'CONFIG_PATH', 'XCENTER_ADMIN_MENU', NULL, '_configPathFeatureTitle', 'LINK', 40, '/admin/configpath/configPathConfig.xhtml', NULL, 1, NULL, 'ADMN_CONFIG_PATH', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'ORGANIZATION_ID', 'XCENTER_ADMIN_MENU', NULL, '_organizationIdFeatureTitle', 'LINK', 50, '/admin/orgid/organizationIdConfig.xhtml', NULL, 1, NULL, 'ADMN_ORG_IDS', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'ORG_HIERARCHY_LEVEL', 'XCENTER_ADMIN_MENU', NULL, '_orgHierarchyLevelFeatureTitle', 'LINK', 60, '/admin/orghierarchylevel/orgHierarchyLevelConfig.xhtml', NULL, 1, NULL, 'ADMN_ORG_HIERARCHY', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'AVAILABLE_LOCALE', 'XCENTER_ADMIN_MENU', NULL, '_availableLocaleFeatureTitle', 'LINK', 70, '/admin/locale/availableLocaleConfig.xhtml', NULL, 1, NULL, 'ADMN_AVAILABLE_LOCALES', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'VERSION_DISPLAY', 'XCENTER_ADMIN_MENU', NULL, '_versionDisplay', 'LINK', 80, '/admin/version/versionDisplay.xhtml', NULL, 1, NULL, 'ADMN_VIEW_ADMIN_VERSION', NULL, NULL, NULL, 0);
--INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
--  VALUES ('MAIN_MENU', 'PROFILE_CONFIG', 'XCENTER_ADMIN_MENU', NULL, '_profileDefiniionTitle', 'LINK_GROUP', 90, NULL, NULL, 0, NULL, 'ADMN_PROFILE_DEFINITION', NULL, NULL, NULL, 0);
GO

DELETE FROM cfg_menu_config WHERE category = 'MAIN_MENU' and parent_menu_name = 'REPORTS_MENU';
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'FLASH_SALES_GROUP', 'REPORTS_MENU', NULL, '_menutextFlashSalesReports', 'ACTION', 10, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'SALES_REPORTS', 'REPORTS_MENU', NULL, '_menuTextSalesReport', 'ACTION', 20, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'INVENTORY_EXCEPTION', 'REPORTS_MENU', NULL, '_menuTextInventoryException', 'ACTION', 30, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'SCHEDULE_REPORTS', 'REPORTS_MENU', NULL, '_menutextEmpSchedRep', 'ACTION', 40, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'LAYAWAY_REPORTS', 'REPORTS_MENU', NULL, '_menutextLayawayReports', 'ACTION', 50, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'MISC_REPORTS', 'REPORTS_MENU', NULL, '_menutextMiscReports', 'ACTION', 60, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0);
GO

DELETE FROM cfg_menu_config WHERE category = 'MAIN_MENU' and parent_menu_name = 'FLASH_SALES_GROUP';
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'FLASH_SALES', 'FLASH_SALES_GROUP', NULL, '_rptFlashSalesReportTitle', 'ACTION', 10, NULL, '@{reportLoadAction.execute(''FLASH_SALES'')}', 1, NULL, 'RPT_FLASH_SALES_SUMMARY', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'FLASH_SALES_BY_DEPT_REPORT', 'FLASH_SALES_GROUP', NULL, '_rptMenuTxtFlashSalesByDeptReports', 'ACTION', 20, NULL, '@{reportLoadAction.execute(''FLASH_SALES_BY_DEPT_REPORT'')}', 1, NULL, 'RPT_FLASH_SALES_DEPARTMENT', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'FLASH_SALES_BY_HOUR_REPORT', 'FLASH_SALES_GROUP', NULL, '_rptFlReportByHourTitleMenu', 'ACTION', 30, NULL, '@{reportLoadAction.execute(''FLASH_SALES_BY_HOUR_REPORT'')}', 1, NULL, 'RPT_FLASH_SALES_HOUR', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'FLASH_SALES_BY_EMP_REPORT', 'FLASH_SALES_GROUP', NULL, '_rptMenuTxtFlashSalesByEmpReports', 'ACTION', 40, NULL, '@{reportLoadAction.execute(''FLASH_SALES_BY_EMP_REPORT'')}', 1, NULL, 'RPT_FLASH_SALES_EMPLOYEE', NULL, NULL, NULL, 0);
GO

DELETE FROM cfg_menu_config WHERE category = 'MAIN_MENU' and parent_menu_name = 'SALES_REPORTS';
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'DAILY_SALES_CASH_REPORT', 'SALES_REPORTS', NULL, '_rptDailySalesSummaryReportTitleMenu', 'ACTION', 10, NULL, '@{reportLoadAction.execute(''DAILY_SALES_CASH_REPORT'')}', 1, NULL, 'RPT_DAILY_SALES_CASH', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'SALES_BY_HOUR_REPORT', 'SALES_REPORTS', NULL, '_rptSalesByHourReportSalesByHour', 'ACTION', 20, NULL, '@{reportLoadAction.execute(''SALES_BY_HOUR_REPORT'')}', 1, NULL, 'RPT_SALES_HOUR', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'SALE_BY_HOUR_ANALYSIS_REPORT', 'SALES_REPORTS', NULL, '_rptSaleByHourAnalysisTitleMenu', 'ACTION', 30, NULL, '@{reportLoadAction.execute(''SALE_BY_HOUR_ANALYSIS_REPORT'')}', 1, NULL, 'RPT_SALES_HOUR_ANALYSIS', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'SALE_BY_DEPT_REPORT', 'SALES_REPORTS', NULL, '_rptSaleByDeptTitleMenu', 'ACTION', 40, NULL, '@{reportLoadAction.execute(''SALE_BY_DEPT_REPORT'')}', 1, NULL, 'RPT_SALES_DEPARTMENT', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'SALE_BY_DEPT_BY_EMP_REPORT', 'SALES_REPORTS', NULL, '_rptSaleByDeptByEmpTitleMenu', 'ACTION', 50, NULL, '@{reportLoadAction.execute(''SALE_BY_DEPT_BY_EMP_REPORT'')}', 1, NULL, 'RPT_SALES_DEPT_EMPLOYEE', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'RETURN_MERCHANDISE_LOG_REPORT', 'SALES_REPORTS', NULL, '_rptReturnMerchandiseReportTitleMenu', 'ACTION', 60, NULL, '@{reportLoadAction.execute(''RETURN_MERCHANDISE_LOG_REPORT'')}', 1, NULL, 'RPT_RETURNED_MERCHANDISE', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'SALES_BY_EMPLOYEE', 'SALES_REPORTS', NULL, '_rptSalesByEmployeeReportTitleMenu', 'ACTION', 70, NULL, '@{reportLoadAction.execute(''SALES_BY_EMPLOYEE'')}', 1, NULL, 'RPT_DAILY_SALES', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'BEST_SELLERS_BY_STYLE_REPORT', 'SALES_REPORTS', NULL, '_rptBestSellersByStyleTitle', 'ACTION', 80, NULL, '@{reportLoadAction.execute(''BEST_SELLERS_BY_STYLE_REPORT'')}', 1, NULL, 'RPT_BEST_SELLERS_STYLE', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'BEST_SELLERS_BY_ITEM_REPORT', 'SALES_REPORTS', NULL, '_rptBestSellersByItemTitle', 'ACTION', 90, NULL, '@{reportLoadAction.execute(''BEST_SELLERS_BY_ITEM_REPORT'')}', 1, NULL, 'RPT_BEST_SELLERS_ITEM', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'WORST_SELLERS_BY_STYLE_REPORT', 'SALES_REPORTS', NULL, '_rptWorstSellersByStyleTitle', 'ACTION', 100, NULL, '@{reportLoadAction.execute(''WORST_SELLERS_BY_STYLE_REPORT'')}', 1, NULL, 'RPT_WORST_SELLERS_STYLE', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'WORST_SELLERS_BY_ITEM_REPORT', 'SALES_REPORTS', NULL, '_rptWorstSellersByItemTitle', 'ACTION', 110, NULL, '@{reportLoadAction.execute(''WORST_SELLERS_BY_ITEM_REPORT'')}', 1, NULL, 'RPT_WORST_SELLERS_ITEM', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'TAX_EXEMPTION_REPORT', 'SALES_REPORTS', NULL, '_rptTaxExemptionReportTitleMenu', 'ACTION', 120, NULL, '@{reportLoadAction.execute(''TAX_EXEMPTION_REPORT'')}', 1, NULL, 'RPT_TAX_EXEMPTION', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'GIFT_CERTIFICATE_REPORT', 'SALES_REPORTS', NULL, '_rptGiftCertificateTitleMenu', 'ACTION', 130, NULL, '@{reportLoadAction.execute(''GIFT_CERTIFICATE_REPORT'')}', 1, NULL, 'RPT_GIFT_CERTIFICATE', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'NO_SALE_REPORT', 'SALES_REPORTS', NULL, '_rptNoSaleTitleMenu', 'ACTION', 140, NULL, '@{reportLoadAction.execute(''NO_SALE_REPORT'')}', 1, NULL, 'RPT_NO_SALE', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'PRICE_OVERRIDE_REPORT', 'SALES_REPORTS', NULL, '_rptPriceOverrideTitleMenu', 'ACTION', 150, NULL, '@{reportLoadAction.execute(''PRICE_OVERRIDE_REPORT'')}', 1, NULL, 'RPT_PRICE_OVERRIDE', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'POST_VOID_REPORT', 'SALES_REPORTS', NULL, '_rptPostVoidTitleMenu', 'ACTION', 160, NULL, '@{reportLoadAction.execute(''POST_VOID_REPORT'')}', 1, NULL, 'RPT_POST_VOID_SUMMARY', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'POST_VOID_DETAIL_REPORT', 'SALES_REPORTS', NULL, '_rptPostVoidDetailTitleMenu', 'ACTION', 170, NULL, '@{reportLoadAction.execute(''POST_VOID_DETAIL_REPORT'')}', 1, NULL, 'RPT_POST_VOID_DETAIL', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'TRANSACTION_VOID_REPORT', 'SALES_REPORTS', NULL, '_rptTransactionVoidDetailTitleMenu', 'ACTION', 180, NULL, '@{reportLoadAction.execute(''TRANSACTION_VOID_REPORT'')}', 1, NULL, 'RPT_TRANS_CANCEL_SUMMARY', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'TRANSACTION_VOID_SUMMARY_REPORT', 'SALES_REPORTS', NULL, '_rptTransactionVoidSummaryTitleMenu', 'ACTION', 190, NULL, '@{reportLoadAction.execute(''TRANSACTION_VOID_SUMMARY_REPORT'')}', 1, NULL, 'RPT_TRANS_CANCEL_DETAIL', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'LINE_VOID_REPORT', 'SALES_REPORTS', NULL, '_rptLineVoidTitleMenu', 'ACTION', 200, NULL, '@{reportLoadAction.execute(''LINE_VOID_REPORT'')}', 1, NULL, 'RPT_LINE_VOID', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'SUSPENDED_TRANSACTION_REPORT', 'SALES_REPORTS', NULL, '_rptSuspdTrnTitleMenu', 'ACTION', 210, NULL, '@{reportLoadAction.execute(''SUSPENDED_TRANSACTION_REPORT'')}', 1, NULL, 'RPT_SUSPENDED_TRANS_DETAIL', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'SUSPENDED_TRANSACTION_SUMMARY_REPORT', 'SALES_REPORTS', NULL, '_rptSuspdTrnSummaryTitleMenu', 'ACTION', 220, NULL, '@{reportLoadAction.execute(''SUSPENDED_TRANSACTION_SUMMARY_REPORT'')}', 1, NULL, 'RPT_SUSPENDED_TRANS_SUMMARY', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'CREDIT_CARD_REPORT', 'SALES_REPORTS', NULL, '_rptCreditCardTitleMenu', 'ACTION', 230, NULL, '@{reportLoadAction.execute(''CREDIT_CARD_REPORT'')}', 1, NULL, 'RPT_CREDIT_CARD', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'DAILY_SALES_TOTAL_REPORT', 'SALES_REPORTS', NULL, '_rptDailySalesReportTitleMenu', 'ACTION', 240, NULL, '@{reportLoadAction.execute(''DAILY_SALES_TOTAL_REPORT'')}', 1, NULL, 'RPT_DAILY_SALES_TOTAL', NULL, NULL, NULL, 0);
GO

DELETE FROM cfg_menu_config WHERE category = 'MAIN_MENU' and parent_menu_name = 'INVENTORY_EXCEPTION';
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'INVENTORY_SHIPPING_EXCEPTION_REPORT', 'INVENTORY_EXCEPTION', NULL, '_rptInventoryExceptionShipTitle', 'ACTION', 10, NULL, '@{reportLoadAction.execute(''INVENTORY_SHIPPING_EXCEPTION_REPORT'')}', 1, NULL, 'RPT_SHIPPING_EXCEPTION', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'INVENTORY_RECEIVING_EXCEPTION_REPORT', 'INVENTORY_EXCEPTION', NULL, '_rptInventoryExceptionRecTitle', 'ACTION', 20, NULL, '@{reportLoadAction.execute(''INVENTORY_RECEIVING_EXCEPTION_REPORT'')}', 1, NULL, 'RPT_RECEIVING_EXCEPTION', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'RESTOCK_REPORT', 'INVENTORY_EXCEPTION', NULL, '_rpReStockReportTitleMenu', 'ACTION', 30, NULL, '@{reportLoadAction.execute("RESTOCK_REPORT")}', 0, NULL, NULL, NULL, NULL, NULL, 0);
GO

DELETE FROM cfg_menu_config WHERE category = 'MAIN_MENU' and parent_menu_name = 'SCHEDULE_REPORTS';
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'EMPLOYEE_PERFORMANCE_REPORT', 'SCHEDULE_REPORTS', NULL, '_menutextRepEmpPerf', 'ACTION', 10, NULL, '@{reportLoadAction.execute(''EMPLOYEE_PERFORMANCE_REPORT'')}', 1, NULL, 'RPT_EMPLOYEE_PERFORMANCE', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'SCHEDULE_DETAIL_REPORT', 'SCHEDULE_REPORTS', NULL, '_menutextRepSchedDet', 'ACTION', 20, NULL, '@{reportLoadAction.execute("SCHEDULE_DETAIL_REPORT")}', 0, NULL, NULL, NULL, NULL, NULL, 0);
GO

DELETE FROM cfg_menu_config WHERE category = 'MAIN_MENU' and parent_menu_name = 'LAYAWAY_REPORTS';
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'LAYAWAY_AGING_SUMMARY_REPORT', 'LAYAWAY_REPORTS', NULL, '_menutextLayawayAgingSummaryReport', 'ACTION', 10, NULL, '@{reportLoadAction.execute(''LAYAWAY_AGING_SUMMARY_REPORT'')}', 1, NULL, 'RPT_LAYAWAY_AGING_SUMMARY', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'LAYAWAY_AGING_DETAIL_REPORT', 'LAYAWAY_REPORTS', NULL, '_menutextLayawayAgingDetailReport', 'ACTION', 20, NULL, '@{reportLoadAction.execute(''LAYAWAY_AGING_DETAIL_REPORT'')}', 1, NULL, 'RPT_LAYAWAY_AGING_DETAIL', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'LAYAWAY_ACCOUNT_ACTIVITY_SUMMARY_REPORT', 'LAYAWAY_REPORTS', NULL, '_menutextLayawayAccountActivtySummaryReport', 'ACTION', 30, NULL, '@{reportLoadAction.execute(''LAYAWAY_ACCOUNT_ACTIVITY_SUMMARY_REPORT'')}', 1, NULL, 'RPT_LAYAWAY_ACCT_ACTIVITY_SUM', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'LAYAWAY_ACCOUNT_ACTIVITY_DETAIL_REPORT', 'LAYAWAY_REPORTS', NULL, '_menutextLayawayAccountActivtyDetailReport', 'ACTION', 40, NULL, '@{reportLoadAction.execute(''LAYAWAY_ACCOUNT_ACTIVITY_DETAIL_REPORT'')}', 1, NULL, 'RPT_LAYAWAY_ACCT_ACTIVITY_DTL', NULL, NULL, NULL, 0);
GO

DELETE FROM cfg_menu_config WHERE category = 'MAIN_MENU' and parent_menu_name = 'MISC_REPORTS';
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'JOURNAL_ROLL_REPORT', 'MISC_REPORTS', NULL, '_rptJournalRollReport', 'ACTION', 10, NULL, '@{reportLoadAction.execute(''JOURNAL_ROLL_REPORT'')}', 1, NULL, 'RPT_JOURNAL', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'STORE_LOCATIONS_REPORT', 'MISC_REPORTS', NULL, '_rptStoreLocationsReport', 'ACTION', 20, NULL, '@{reportLoadAction.execute(''STORE_LOCATIONS_REPORT'')}', 1, NULL, 'RPT_STORE_LOCATIONS', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'CUSTOMER_LIST_REPORT', 'MISC_REPORTS', NULL, '_menutextCustList', 'ACTION', 30, NULL, '@{reportLoadAction.execute(''CUSTOMER_LIST_REPORT'')}', 1, NULL, 'RPT_CUSTOMER_LIST', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'SPECIAL_ORDERS_REPORT', 'MISC_REPORTS', NULL, '_menutextSpecialOrdersReports', 'ACTION', 40, NULL, '@{reportLoadAction.execute(''SPECIAL_ORDERS_REPORT'')}', 1, NULL, 'RPT_SPECIAL_ORDERS', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'PRICE_CHANGE_REPORT', 'MISC_REPORTS', NULL, '_menutextPriceChangeReport', 'ACTION', 50, NULL, '@{reportLoadAction.execute(''PRICE_CHANGE_REPORT'')}', 1, NULL, 'RPT_PRICE_CHANGE', NULL, NULL, NULL, 0);
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'ITEM_LIST_REPORT', 'MISC_REPORTS', NULL, '_rptItemListTitle', 'ACTION', 70, NULL, '@{reportLoadAction.execute(''ITEM_LIST_REPORT'')}', 1, NULL, 'RPT_ITEM_LIST', NULL, NULL, NULL, 0);

INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, config_type, title, menu_type, sort_order, view_id, action_expression, active_flag, propagation, security_privilege, custom_datasource_class, custom_datasource_method, menu_small_icon, menu_separator)
  VALUES ('MAIN_MENU', 'EMPLOYEE_TASK_REPORT', 'MISC_REPORTS', NULL, '_rptEmployeeTasksReport', 'ACTION', 80, NULL, '@{reportLoadAction.execute("EMPLOYEE_TASKS_REPORT")}', 1, NULL, 'RPT_EMP_TASKS', NULL, NULL, NULL, 0);
GO

-- CFG_CONFIG_PROPERTIES
DELETE FROM cfg_config_properties WHERE category = 'DATA';
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('DATA', 'Tender---authMethodCode', 'TENDER', 'Tender authorization method code', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('DATA', 'Tender---custIdReqCode', 'TENDER', 'Default customer identification type', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('DATA', 'Tender---reportingGroup', 'TENDER', 'Reporting group this tender belongs to on the sales reports.', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('DATA', 'Tender---unitCountCode', 'TENDER', 'Till count type code.', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('DATA', 'Tender---currencyId', 'TENDER', 'Default current code for current store', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('DATA', 'TenderUserSettings---usageCode', 'TENDER', 'Tender user settings usage code', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('DATA', 'TenderUserSettings---entryMethodCode', 'TENDER', 'Tender user settings hardware entry method code', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('DATA', 'TenderAvailability---availabilityCode', 'TENDER', 'Tender availability code', NULL, 60);
GO

DELETE FROM cfg_config_properties WHERE category = 'VISIBILITY_RULE';
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AddCustomerToTranVisibilityRule', 'DEFAULT', 'Determines if "Add Customer To Tran" button in customer maintenance should be enabled.', '"Add Customer To Tran" button in customer maintenance', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AddTenderExchangeRule', 'AvailableForExchangeType', 'Determines if multiple incoming or outgoing tender is allowed in tender exchange.', 'Allow multiple tenders in tender exchange', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AllowCancelCustomerAccountAccCheck', 'DEFAULT', 'Determines if cancel customer account is allowed based on the customer account status.', 'Allow cancel customer account based on status', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AllowEditWorkOrderAccountVisibilityRule', 'DEFAULT', 'Determines if the current work order account is allowed to be edited based on the account status.', 'Allow edit work order based on status', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AllowMixedInTransWithSaleItemTypeRule', 'type', 'Determines if different sale type options are available based on existing line item sale type.', 'Allow different sale type options', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnyARAcctForCurrentCustCheck', 'NotOnHoldOnly', 'Determines if accounts receivable options are available based on if the current customer has an open AR account.', 'Enable AR option based on current customer', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnyCustAccountNotesCheck', 'DEFAULT', 'Determines if the current customer account contains any comment.', 'Check customer account contains any comment', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnyEstimateWorkOrderTaskCheck', 'DEFAULT', 'Determines if the current work order contains any estimate task to be converted to actual task.', 'Any estimate task in work order account', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnyItemWithoutSaleItemTypeVisibilityRule', 'type', 'Enable the option if any of the existing line items does not contain the specific sale type.', 'No line item belongs to sale type check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnyItemWithSaleItemTypeVisibilityRule', 'type', 'Enable the option if any of the existing line items contains the specific sale type.', 'Any line item belongs to sale type check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnyLayawayItemVisibilityRule', 'DEFAULT', 'Determines if the current layaway/special order account is editable based on the customer account detail status.', 'Allow edit on current layaway/special order account', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnyMinimalSpecOrderItemsRule', 'DEFAULT', 'Determines if there is any special order line item and if it is in minimal special order mode.', 'Check minimal special order mode', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnyNonSaleItemsInTransCheck', 'DEFAULT', 'Disable the option if the current transaction contains any line item sale type that is not allowed to be mixed with SALE sale type.', 'Check line item sale type not allow mixing', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnyPartOnCurrentWorkOrderCheck', 'DEFAULT', 'Determine if there is any editable work order parts line item based on the work order line item status.', 'Allow edit work order parts', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnyPickableItemsVisibilityRule', 'DEFAULT', 'Determine if there is any items on the current layaway account are ready to be picked up or have open status.', 'Any ready to pickup or open layaway item check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnyReadyToPickupLayawayLineItemCheck', 'DEFAULT', 'Determine if there is any items on the current layaway account are available to be picked up.', 'Any ready to pickup layaway item check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnyReadyToPickupSpecialOrderLineItemCheck', 'DEFAULT', 'Determine if there is any items on the current special order account are available to be picked up.', 'Any ready to pickup special order item check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnyReadyToPickupWorkOrderLineItemCheck', 'DEFAULT', 'Determine if there is any items on the current work order account are available to be picked up.', 'Any ready to pickup work order item check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnySaleReturnLineItemVisibilityRule', 'DEFAULT', 'Determine if there is any non-voided line item in the current retail transaction.', 'Any non-voided line item check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnyTaskOnCurrentWorkOrderCheck', 'DEFAULT', 'Determine if there is any editable work order task line item based on the work order line item status.', 'Allow edit work order task', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnyTenderAccCheck', 'return', 'Determine if there is any tender line item exists in the current retail transaction.', 'Any tender line item check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnyUserAccCheck', 'DEFAULT', 'Access check that is met if any user other than the system default user is logged in.', 'Not default user log in check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnyWorkOrderItemCheck', 'DEFAULT', 'Determine if there is any work order item on the current work order account.', 'Any work order item check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnyWorkOrderNotesCheck', 'DEFAULT', 'Determine if the current work order account contains any comment.', 'Work order account contains comments check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AvailableLayawayTenderAccCheck', 'tender', 'Determine if the current tender would be available for layaway transaction.', 'Tender available for layaway check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.BasicSaleTypeAvailabilityAccCheck', 'DEFAULT', 'Determine if the current tender would be available for sale transaction.', 'Tender available for sales check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.ChangeExpectedDateVisibilityRule', 'DEFAULT', 'Determine if expected date option is enabled for special order.', 'Expected date option for special order check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.CheckPayDeductTenderAvailCheck', 'DEFAULT', 'Determine if payroll deduction tender option would be enabled based on the employee status of the current customer.', 'Enable payroll deduction tender option', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.ClosedLayawayAccCheck', 'DEFAULT', 'Disable the option if there is any closed layaway account on the current retail transaction.', 'Any closed layaway account check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.ContainsNextDenominationCountVisibilityRule', 'DEFAULT', 'Disable the option if there is no more next till count group (in till count screen).', 'Any next till count group check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.TillAccountabilityAccessCheck', 'DEFAULT', 'Enable the option if till accountability functionality is used.', 'Use Till Accountability', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.ContainsPriorDenominationCountVisibilityRule', 'DEFAULT', 'Disable the option if there is no more prior till count group (in till count screen).', 'Any prior till count group check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.CreditDebitTenderOnReturnRule', 'DEFAULT', 'Determine if credit tender would be available in return transaction based on the return type and tender configuration.', 'Enable credit tender based on return type', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.CurrentEjournalTranIsStatusCode', 'statusCode', 'Validate whether the transaction currently selected in the Ejournal contains the status code pass in.', 'Ejournal transaction status check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.CurrentUserAccCheck', 'privilege', 'Enable the option if the current login user has enough security level for the given security privilege.', 'Current login user security check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.CustomerAccountLengthCheck', 'limit', 'Validate the length of the customer account number to determine edit customer button should be enabled.', 'Enable edit customer option', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.DelinquentLayawayAccCheck', 'DEFAULT', 'Disable the option if the current layaway account has delinquent status.', 'Delinquent layaway account status check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.DisableRemoteSendRefundCheck', 'DEFAULT', 'Disables a menu option if we are performing a Remote Send Refund.', 'Remote send refund mode check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.EditTaxExemptVisibilityRule', 'DEFAULT', 'Determines visibility of the "Edit Tax Exemption" button from the customer maintenance form.', 'Enable "Edit Tax Exemption" button in customer maintenance', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.EffectiveTenderAccCheck', 'tender', 'Checks if a tender option is available based on effective date configured in tender table.', 'Enable tender option based on effective date', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.EffectiveTenderCheck', 'tender', 'Checks if a tender option is available based on its tender availability configuration.', 'Enable tender option based on tender availability', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.EffectiveTenderExchangeAccCheck', 'ExchangeType', 'Checks if a tender option is available in tender exchange based on its tender availability configuration.', 'Enable tender exchange tender option based on tender availability', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.ExistingLayawayAccCheck', 'DEFAULT', 'Enable the option if the current layaway account is a new or existing account.', 'New or existing layaway account check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.FormNavigationVisibilityRule', 'formName', 'Disable the option if a form with the given named is active.', 'Form with a given name is active check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.GiftReceiptEligibilityAccessCheck', 'DEFAULT', 'Enable the option if there is at least one non-voided and non-return line item is in the current transaction.', 'Non-voided and non-return line item check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.HasTenderAccessCheck', 'tenderId', 'Enable the option if there is any tender line item with the given tender id in the current transaction.', 'Any tender line match given tender id check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.IsSaleCompleteAccCheck', 'DEFAULT', 'Enable the option if the current retail tranasction has complete status.', 'Transaction with complete status check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.IsStoreCreatedInvCountDocRule', 'DEFAULT', 'Enable the option if the current inventory control document is store created.', 'Inventory document is store created check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.IssueLayawayRefundAccCheck', 'DEFAULT', 'Enable the option if the current layaway account requires refund.', 'Layaway account requires refund check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.IsTillAttachedAccCheck', 'DEFAULT', 'Enable the option if a till is attached to the current workstation.', 'Till attached to register check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.LastReceiptNotNullVisibilityRule', 'privilege', 'Enable the option if the last retail transaction id can be retrieved.', 'Any last transaction found check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.LayawayZeroBalanceAccCheck', 'DEFAULT', 'Enable the option if the current layaway account balance is greater than zero.', 'Positive layaway account balance check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.MoreThanOnceAllowTenderVisibilityRule', 'tenderId', 'Enable the option if there is any tender line item with the given tender id in the current retail transaction.', 'Any tender with given tender id check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.MoreThanOneLayawayItemVisibilityRule', 'DEFAULT', 'Enable the option if the current transaction contains more than one non-voided layaway item.', 'Any non-voided layaway item check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.MoreThanOneSaleItemVisibilityRule', 'DEFAULT', 'Enable the option if the current transaction contains more than one non-voided sale item.', 'Any non-voided sale item check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.MoreThanOneSpecOrderItemVisibilityRule', 'DEFAULT', 'Enable the option if the current transaction contains more than one non-voided special order item.', 'Any non-voided special order item check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.MoreThanOneWorkOrderItemCheck', 'DEFAULT', 'Enable the option if the current transaction contains more than one non-voided work order item.', 'Any non-voided work order item check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.NoAuthorizedLineItemAccCheck', 'DEFAULT', 'Disable the option if there is any authorized line item in the current transaction.', 'Any authorized line item check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.NoAvailableInTrainingModeRule', 'DEFAULT', 'Disable the option if the system is currently in training mode.', 'Training mode check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.NoItemIsNotReturnItemVisibilityRule', 'DEFAULT', 'Disable the option if there is any return line item or restocking fee item in current retail transaction.', 'Any return line item check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.NoItemWithoutSaleItemTypeVisibilityRule', 'type', 'Disable the option if any of the existing line items does not contain the specific sale type.', 'Any line item contains the given sale type check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.NoItemWithSaleItemTypeVisibilityRule', 'type', 'Disable the option if any of the existing line items does not contain the specific sale type.', 'Any line item not contain the given sale type check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.NotExceedTotalMaxReturnAmtThresholdCheck', 'DEFAULT', 'Disable the option if the total return value does exceed the total configured maximum return amount.', 'Total return amount exceed max check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.NoTillAttachedAccCheck', 'DEFAULT', 'Disable the option if a till is attached to current work station.', 'No till attached to register check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.NoTranAccCheck', 'DEFAULT', 'Disable the option if there is a current transaction.', 'No transaction check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.NotUnavailableAccessCheck', 'type', 'Enable the option if the given persistence manager type is available.', 'Persistence manager type is available check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.OpenSupportTicketVisibilityRule', 'DEFAULT', 'Enable the option if system configuration for GenerateHelpTickets is set to true. (It is for "Open Support Ticket" option.)', 'Enable "Open Support Ticket" option', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.RefundBalanceTenderAccessCheck', 'tender', 'Determine if the refund tender option should be enabled or not based on the minimum and maximum refund tender amount configuration.', 'Enable refund tender based on refund amount', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.RefundTenderCheck', 'tender', 'Determine if the refund tender option should be enabled or not based on the return transaction and refund tender configuration.', 'Enable refund tender based on refund configuration', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.RequirePartyObjForARUserCheck', 'DEFAULT', 'Enable the option if the system requires customer party records for all account receivable eligible users.', 'Require party record for AR user check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.SpecialOrderModeAccessCheck', 'MINIMAL', 'Enable the option if the system is configured to use the special order mode that matches the given mode.', 'Match the given special order mode check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.TenderExchangeAvailableAccCheck', 'DEFAULT', 'Enable the option if there is a customer association in the current transaction.', 'Customer association check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.TenderExchangeIsEvenTenderedAccCheck', 'DEFAULT', 'Enable the option if the current tender exchange transaction is evenly tendered and has at least one incoming tender.', 'Tender exchange is evenly tendered check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.TillRegisterModeAccessCheck', 'mode', 'Enable the option if the current system is running in the same till accounting mode as the given mode.', 'Match the given till accounting mode check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.ValidSuspendableTransVisibilityRule', 'DEFAULT', 'Disable the option if there is any customer account line item, verified return line item, or any tender line item in current transaction.', 'Allow suspend transaction option', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.ViewLogVisibilityRule', 'DEFAULT', 'Enable the option if the ViewErrorLog system configuration is set to "on".', 'Allow viewing log', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.ZeroTransAmountDueVisibilityRule', 'DEFAULT', 'Enable the option if the current transaction amount due is not zero.', 'Zero transaction amount due check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.assistance.access.AssistanceModeStatusAccessCheck', 'state', 'Enable the option if the current system assistance state matches the given value.', 'System assistance state check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.nosale.CashDrawerPresentVisibilityRule', 'DEFAULT', 'Enable the option if a cashdrawer hardware is attached to current register.', 'Cashdrawer available check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shipsale.common.AnyOpenSendSalesRule', 'INVERTED', 'Enable the option if there is any open send sale in the current transaction.', 'Any open send sale check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.layaway.validation.LayawayStatusVisibilityRule', 'STATUS', 'Enable the option if the status of the current layaway account matches one of the set of configurable values.', 'Layaway account status check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.specorder.AnyNewlyPickedItemsVisibilityRule', 'accountType', 'Enable the option if the current customer account of the given type does not have any newly picked up items.', 'Any newly picked up customer account check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.specorder.validation.SpecOrderStatusVisibilityRule', 'STATUS', 'Enable the option if the status of the current layaway account matches one of the set of configurable values.', 'Special Order account status check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.specorder.account.SpecOrderAddItemVisibilityRule', 'DEFAULT', 'Enable the option if the current special order account status is NEW or OPEN.', 'Allow adding special order item based on status', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.accountsreceivable.CreditPaymentOnTransVisibilityRule', 'DEFAULT', 'Enable the option if there is a credit payment item on the current transaction.', 'Any credit payment tender check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.assistance.access.TrainingModeEnabledAccessCheck', 'DEFAULT', 'Enable the option if training mode is enabled in system configuration.', 'Training mode enabled check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.browser.BrowserBackVisibilityRule', 'DEFAULT', 'Determine if the Esc(BACK) option should be enabled for browser.', 'Allow BACK option for browser', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.browser.BrowserForwardVisibilityRule', 'DEFAULT', 'Determine if the FORWARD button should be enabled for browser.', 'Allow FORWARD option for browser', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.ejournal.giftrcpt.EjournalTranAccessCheck', 'tranType', 'Enable the option if the current transaction type is the same as the given value.', 'Transaction type check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.ejournal.postvoid.visibility.TenderVisibilityRule', 'EXCLUDE', 'Enable the option if the current EJournal transaction does not contain a tender in the list specified in configuration.', 'No specific tender in EJournal transaction chekc', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.framework.visibilityrules.FingerprintDeviceRule', 'DEFAULT', 'Enable the option if a fingerprint device is available.', 'Fingerprint device available', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.itemrepeat.LastLineItemRepeatableVisibilityRule', 'DEFAULT', 'Disable the option if the last line item added to the transaction is voided or a non physical item.', 'Last line item repeatable check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.spa.DisableMenuButtonsForSPARule', 'DEFAULT', 'Disable the option if a SPA (Sale Price Adjustment) transaction has started.', 'SPA transaction has started check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.spa.SPATenderVisibilityRule', 'DEFAULT', 'Disable the option if there is any SPA (Sale Price Adjustment) line item.', 'Any SPA line item check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.spa.TempMarkdownVisibilityRule', 'state', 'Toggles "Temporary Markdown" menu option for SPA (Sale Price Adjustment) transaction.', 'Enable temporary markdown option', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.remotesend.common.AnyOpenRemoteSendsRule', 'DEFAULT', 'Enable the option if there is any remote send line item in the current transaction.', 'Any remote send line item check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.systemcycle.access.ChangeBDateVisibilityRule', 'DEFAULT', 'Enable the option if AllowUserChangeBusinessDate system configuration is set to true.', 'Allow change business date check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.systemcycle.access.ClosingVisibilityRule', 'DEFAULT', 'Enable the option if the store is not in the closing processes.', 'Store closing check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.systemcycle.access.CompleteRemoteCloseVisibilityRule', 'DEFAULT', 'Enable the option if the AllowRemoteWSClose system configuration is set to true and there are open work stations.', 'Allow complete remote register close check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.systemcycle.access.RePollVisibilityRule', 'DEFAULT', 'Enable the option if the last environment message sent to Xstore was a polling error.', 'Enable RePoll option check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.systemcycle.access.RetailLocationStateAccessCheck', 'state', 'Enable the option if the current store status is the same as the given value.', 'Store status check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.systemcycle.access.StartRemoteCloseVisibilityRule', 'DEFAULT', 'Enable the option if the AllowRemoteWSClose system configuration is set to true.', 'Allow start remote register close check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.systemcycle.access.WorkstationStateAccessCheck', 'state', 'Enable the option if the current work station state is the same as the given value.', 'Register status check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.tender.exchange.AnyTendersVisibilityRule', 'tenderStatus', 'Enable the option if there is any non-voided tender with any of the given tender types in the current transaction.', 'Any tender type check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.tender.exchange.MultipleOutgoingTendersVisibilityRule', 'DEFAULT', 'Enable the option if AllowMultipleOutgoingTenders system configuration is set to true.', 'Multiple outgoing tender check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.employee.ChangePasswordAccessCheck', 'privilege', 'Enable the option if the current user has enough security level to change password.', 'Change password security check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.ejournal.postvoid.visibility.BusinessDateVisibilityRule', 'DEFAULT', 'Enable the option if the selected EJournal transaction is from the same Business Date as is current.', 'EJournal transaction business date check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.inventory.locationmaint.SystemInventoryLocationVisibilityRule', 'DEFAULT', 'Enable the option if an inventory location is not flagged as a system location.', 'System inventory location check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.inventory.lookup.style.PrintStyleLookupReceiptRule', 'DEFAULT', 'Enable the "printing of Style Lookup Price History" option only if similar style items are found.', 'Enable print style lookup receipt check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.inventory.lookup.AddItemVisibilityRule', 'DEFAULT', 'Determines visiblity of add item key for item lookup screen.', 'Add item option check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.inventory.lookup.GridPageNextPriorRule', 'DEFAULT', 'Visibilty rule for the grid next and previous options in item lookup screen.', 'Enable item lookup next previous options', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.inventory.lookup.LayawayVisibilityRule', 'DEFAULT', 'Determines visibility for layaway option in item lookup screen.', 'Enable item lookup layaway option', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.inventory.lookup.SimilarItemsRule', 'DEFAULT', 'Visibility rule for similar items selection in item lookup screen.', 'Enable item lookup similar item option', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.inventory.lookup.SpecialOrderVisibilityRule', 'DEFAULT', 'Determines visibility for special order option in item lookup screen.', 'Enable item lookup special order option', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.inventory.lookup.SubstituteItemsRule', 'DEFAULT', 'Visibility rule for substitute items selection in item lookup screen.', 'Enable item lookup substitute items option', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.inventory.lookup.VendorItemsRule', 'DEFAULT', 'Determines visiblity of the vendor items list for item lookups screen.', 'Enable item lookup vendor items option', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.inventory.movement.AnyMovementPendingItemsVisibilityRule', 'DEFAULT', 'Enable the option if there are any pending inventory movement items to be reconciled.', 'Any pending inventory movement item check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.inventory.receive.visibilityrules.CartonSpecificLineItemRule', 'DEFAULT', 'Enable the option if there is at least an item in the current receiving carton.', 'Any item in receiving carton check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.inventory.visibilityrules.AnyInvDocLineItemRule', 'DEFAULT', 'Enable the option if there is any line items on the inventory document.', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.inventory.visibilityrules.AnyInventoryDocumentRule', 'DEFAULT', 'Enable the option if there is any inventory control document found and displayed on current shipping or receiving maintenance screen.', 'Any inventory document check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.inventory.visibilityrules.AnyShipmentItemsRule', 'DEFAULT', 'Enable the option if there is any shipment items in the current shipping document.', 'Any shipment items check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.inventory.visibilityrules.CartonShippingVisibilityRule', 'DEFAULT', 'Disable the option if the carton shipping configuration is enabled.', 'Carton shipping configuration check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.inventory.visibilityrules.ClosedInvDocRule', 'DEFAULT', 'Disable the option if the current inventory document status is CLOSED.', 'Closed inventory document check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.inventory.visibilityrules.DocumentContainsSerialNumbersVisibilityRule', 'DEFAULT', 'Enable the option if the inventory document contains all of the serial numbers for serialized items.', 'Document contains serial number check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.inventory.visibilityrules.RecordCreationTypeNotVisibileRule', 'type', 'Visibility rule that controls access based on the record creation type of the current inventory control document.', 'Inventory document record creation type check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.inventory.visibilityrules.StoreCreatedInvDocRule', 'type', 'Enable the option if the inventory document is store created.', 'Store created inventory document check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.reporting.ui.SaveReportAvailableRule', 'type', 'Visibility rule that prevents the user from selecting "Save" when viewing a saved report.', 'Enable save report option', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.workorder.account.WorkOrderStatusVisibilityRule', 'type', 'Enable the option if the status of the current work order account matches one of the set of configurable values.', 'Work order account status check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.sale.RefundDueVisibilityRule', 'DEFAULT', 'A visibility rule that is satisfied if there is a current transaction, and that transaction has a refund owed.', 'Current transaction contains refund owe check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.sale.AmountDueVisibilityRule', 'DEFAULT', 'A visibility rule that is satisfied if there is a current transaction, and that transaction has an amount owed.', 'Current transaction contains amount owe check', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.systemcycle.access.ChangeBusinessDateVisibilityRule', 'DEFAULT', 'Enable the option if AllowUserChangeBusinessDate system configuration is set to true.', 'AllowUserChangeBusinessDate system configuration check', 10);
GO

DELETE FROM cfg_config_properties WHERE category = 'SYSTEM_CONFIG' AND config_name LIKE 'Store---RegisterConfig%';
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---CommissionedAssociates---PromptPerItem', 'System', '_CommissionedAssociatePromptPerItem', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---CommissionedAssociates---DisplayPerItemOnSale', 'System', '_CommissionedAssociateDisplayPerItemOnSale', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---RestrictSaleType', 'Register', '_RestrictSaleType', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---SaleEndingChain', 'Register', '_SaleEndingChain', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---OpenClose---StartupApplication', 'Open Close', '_OpenCloseStartupApplication', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---OpenClose---AutoClockOutOnRtlLocClose', 'Open Close', '_AutoClockOutOnRtlLocClose', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---OpenClose---ValidateSuspendedTransactionsAtClose', 'Open Close', '_ValidateSuspendedTransactionsAtClose', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---OpenClose---AlwaysAuthenticateOnAppChange', 'Open Close', '_AlwaysAuthenticateOnAppChange', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---OpenClose---AllowUserChangeBusinessDate', 'Open Close', '_AllowUserChangeBusinessDate', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---OpenClose---AllowSameDayStoreReopen', 'Open Close', '_AllowSameDayStoreReopen', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---OpenClose---AllowUnscheduledBusinessDateOpen', 'Open Close', '_AllowUnscheduledBusinessDateOpen', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---OpenClose---CountTillsInClose', 'Open Close', '_CountTillsInClose', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---OpenClose---BlindWSOpenOnRtlLocOpen', 'Open Close', '_BlindWSOpenOnRtlLocOpen', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---OpenClose---ForceCloseWSOnRtlLocClose', 'Open Close', '_ForceCloseWSOnRtlLocClose', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---OpenClose---RunWSOpenOnRtlLocOpen', 'Open Close', '_RunWSOpenOnRtlLocOpen', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---OpenClose---RequireWSClosed', 'Open Close', '_RequireWSClosed', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---OpenClose---RtlLocCycleFromPrimaryWSOnly', 'Open Close', '_RtlLocCloseFromPrimaryWSOnly', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---OpenClose---HaltApplicationOnRtlLocClose', 'Open Close', '_HaltApplicationOnRtlLocClose', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---OpenClose---AllowRemoteWSClose', 'Open Close', '_AllowRemoteWSClose', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---OpenClose---CreateRtlLocOpenTransaction', 'Open Close', '_CreateRtlLocOpenTransaction', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---OpenClose---CreateRtlLocCloseTransaction', 'Open Close', '_CreateRtlLocCloseTransaction', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---OpenClose---CreateWSOpenTransaction', 'Open Close', '_CreateWSOpenTransaction', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---OpenClose---CreateWSCloseTransaction', 'Open Close', '_CreateWSCloseTransaction', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---OpenClose---PromptWishToOpenQuestion', 'Open Close', '_PromptWishToOpenQuestion', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---OpenClose---PromptWishToCloseQuestion', 'Open Close', '_PromptWishToCloseQuestion', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---OpenClose---CaptureUserMsg', 'Open Close', '_OpenCloseCaptureUserMsg', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---OpenClose---DisplayUserCapturedMsg', 'Open Close', '_OpenCloseDisplayUserCapturedMsg', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---OpenClose---ListOpenWS', 'Open Close', '_OpenCloseListOpenWS', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---OpenClose---ProcessPayroll', 'Open Close', '_OpenCloseProcessPayroll', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---OpenClose---ValidateAnyEmployeeNotClockOutYet', 'Open Close', '_ValidateAnyEmployeeNotClockOutYet', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---OpenClose---QuickTransferNoTillCount', 'Open Close', '_QuickTransferNoTillCount', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---Location---DepositAcct', 'Store', '_DepositAcct', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---Location---OvertimeRuleType', 'Payroll', '_OvertimeRuleType', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---Browser---Type', 'System', '_BrowserType', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---Browser---Debug', 'System', '_BrowserDebug', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---CommissionedAssociates---PromptForCommissionedAssociates', 'System', '_PromptForCommissionedAssociates', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---CommissionedAssociates---DefaultCashierAsFirstCommissionedAssociate', 'System', '_DefaultCashierAsFirstCommissionedAssociate', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---CommissionedAssociates---AllowCashierAsCommissionedAssociate', 'System', '_AllowCashierAsCommissionedAssociate', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---CommissionedAssociates---DefaultCommissionMethod', 'System', '_DefaultCommissionMethod', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---CommissionedAssociates---MinCommissionedAssociatesAllowed', 'System', '_MinCommissionedAssociatesAllowed', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---CommissionedAssociates---MaxCommissionedAssociatesAllowed', 'System', '_MaxCommissionedAssociatesAllowed', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---CommissionedAssociates---PromptWithList', 'System', '_CommissionedAssociatesPromptWithList', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---CommissionedAssociates---RcptIncludeFirstName', 'System', '_CommissionedAssociatesRcptIncludeFirstName', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---CommissionedAssociates---RcptIncludeLastName', 'System', '_CommissionedAssociatesRcptIncludeLastName', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---CommissionedAssociates---RcptNewLineBetween', 'System', '_CommissionedAssociatesRcptNewLineBetween', NULL, 10);
GO

DELETE FROM cfg_config_properties WHERE category = 'SYSTEM_CONFIG' AND config_name LIKE 'Store---SystemConfig%';
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---AccountsReceivable---PromptUserInfoMethod', 'Accounts Receivable', '_PromptUserInfoMethod', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SendSale---DefaultExternalOrignatorCode', 'Send Sale', '_SendSaleDefaultExternalOrignatorCode', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Tender---DefaultChangeTenderIdIfNoneFound', 'Tender', '_DefaultChangeTenderIdIfNoneFound', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---NonMerchItemTypes---IncludeItemCounts', 'Non Physical Item', '_NonMerchItemTypesIncludeItemCounts', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---PrintStoreCopyWithSigCaptured', 'System', '_PrintStoreCopyWithSigCaptured', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ProcessInventoryJournal', 'System', '_ProcessInventoryJournal', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---IncludeSalesTaxInGrossSales', 'System', '_IncludeSalesTaxInGrossSales', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ExcludeVatFromNetSales', 'System', '_ExcludeVatFromNetSales', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---GrossSalesOptions---EnforceExcludeFromNetSalesFlag', 'System', '_EnforceExcludeFromNetSalesFlag', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---GrossSalesOptions---ExcludeReturns', 'System', '_GrossSaleExcludeReturns', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---OneSequenceForAllCustAccounts', 'System', '_OneSequenceForAllCustAccounts', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TrainingMode---TrainingModeEnabled', 'Training', '_TrainingModeEnabled', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TrainingMode---TrainingModeRestrictOfflineAccess', 'Training', '_TrainingModeRestrictOfflineAccess', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TrainingMode---TrainingModeRestrictEmployeeAccess', 'Training', '_TrainingModeRestrictEmployeeAccess', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TrainingMode---TrainingModeRestrictReceiptPrinting', 'Training', '_TrainingModeRestrictReceiptPrinting', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TrainingMode---TrainingModeRestrictEnterExitPrinting', 'Training', '_TrainingModeRestrictEnterExitPrinting', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TrainingMode---TrainingModeProcessPosLog', 'Training', '_TrainingModeProcessPosLog', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TrainingMode---TrainingModeRtlLocCloseFromPrimaryWSOnly', 'Training', '_TrainingModeRtlLocCloseFromPrimaryWSOnly', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---AutoLogout---Enabled', 'System', '_AutoLogoutEnabled', 'AutoLogout:Enabled', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---AutoLogout---WaitSeconds', 'System', '_AutoLogoutWaitSeconds', 'AutoLogout:WaitSeconds', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---DisplayVoidedLineItems', 'System', '_DisplayVoidedLineItems', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---UseOneStepReprompt', 'System', '_UseOneStepReprompt', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LocalCurrencyId', 'Tender', '_LocalCurrencyId', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LoginSecurity---IdType', 'Security', '_SecurityIdType', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LoginSecurity---DetailedFailureMessage', 'Security', '_SecurityDetailedFailureMessage', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LoginSecurity---PasswordHistoryLength', 'Security', '_PasswordHistoryLength', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LoginSecurity---AccountLockout---Enabled', 'Security', '_AccountLockoutEnabled', 'AutoLogout:Enabled', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LoginSecurity---AccountLockout---Retries', 'Security', '_AccountLockoutRetries', 'AccountLockout:Retries', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LoginSecurity---PasswordExpiration---Enabled', 'Security', '_PasswordExpirationEnabled', 'PasswordExpiration:Enabled', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LoginSecurity---PasswordExpiration---Days', 'Security', '_PasswordExpirationDays', 'PasswordExpiration:Days', 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Transaction---PromptForCancelReasonCode', 'Sale', '_SalePromptForCancelReasonCode', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---NoSale---ManualOpen---Notes', 'No Sale', '_NoSaleNotes', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemSale---MinItemIdLength', 'Sale', '_SaleMinItemIdLength', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemSale---MaxItemIdLength', 'Sale', '_SaleMaxItemIdLength', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemSale---MinItemSerialLength', 'Sale', '_SaleMinItemSerialLength', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemSale---MaxItemSerialLength', 'Sale', '_MaxItemSerialLength', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemSale---MaxItemPrice', 'Sale', '_SaleMaxItemPrice', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemSale---UseItemTableAsPrimary', 'Sale', '_UseItemTableAsPrimary', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemSale---AllowItemNotOnFile', 'Sale', '_SaleAllowItemNotOnFile', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemSale---NotifyForItemNotOnFile', 'Sale', '_SaleNotifyForItemNotOnFile', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemSale---PromptUserForSaleCompletion', 'Sale', '_PromptUserForSaleCompletion', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemSale---RequireItemVoidReasonCode', 'Sale', '_RequireItemVoidReasonCode', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemSale---PromptForBirthDate', 'Sale', '_SalePromptForBirthDate', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemSale---VerifyItemOnHand', 'Sale', '_SaleVerifyItemOnHand', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TimeClock---ClockInNotRequiredOnCreateEmpFlag', 'Timecard', '_ClockInNotRequiredOnCreateEmpFlag', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TimeClock---ClockInRequiredForAuthorizationFlag', 'Timecard', '_ClockInRequiredForAuthorizationFlag', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---EmployeeSale---AllowSelfSale', 'Sale', '_AttachedItemsAllowSelfSale', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---DefaultIDType', 'Tender', '_DefaultIDType', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---CheckTender---PromptForBirthday', 'Tender', '_CheckPromptForBirthday', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---CheckTender---MinimumCheckNumber', 'Tender', '_MinimumCheckNumber', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---CreditDebitTender---PinpadRequiredForDebit', 'Tender', '_PinpadRequiredForDebit', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---PayrollDeduction---OverMaxPayDeductThreshold', 'Tender', '_OverMaxPayDeductThreshold', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---AccountsReceivable---PromptUserInfo', 'Accounts Receivable', '_PromptUserInfo', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---AccountsReceivable---ARTenderAmtOverAvailCreditLimitThreshold', 'Accounts Receivable', '_ARTenderAmtOverAvailCreditLimitThreshold', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---AccountsReceivable---HomeOfficeManualAuthPhoneNumber', 'Accounts Receivable', '_HomeOfficeManualAuthPhoneNumber', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---AccountsReceivable---RequirePartyObjectForAllUsers', 'Accounts Receivable', '_RequirePartyObjectForAllUsers', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---AccountsReceivable---RequireARAcctOwnerAsscWithTrans', 'Accounts Receivable', '_RequireARAcctOwnerAsscWithTrans', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---AccountsReceivable---ShowOutstandingInvoicesForPayment', 'Accounts Receivable', '_ShowOutstandingInvoicesForPayment', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---AccountsReceivable---PromptStatementDateAtPayment', 'Accounts Receivable', '_PromptStatementDateAtPayment', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---AccountsReceivable---PromptInvoicesAtPayment', 'Accounts Receivable', '_PromptInvoicesAtPayment', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---GiftReceipt---MaxPrintsBeforeManagerOverride', 'Sale', '_MaxPrintsBeforeManagerOverride', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---GiftReceipt---MultipleQuantityPrintsMultipleGiftReceipts', 'Sale', '_MultipleQuantityPrintsMultipleGiftReceipts', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---GiftReceipt---AllowNormalSaleItemsOnReceipt', 'Sale', '_AllowNormalSaleItemsOnReceipt', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---GiftReceipt---AllowShipSaleItemsOnReceipt', 'Sale', '_AllowShipSaleItemsOnReceipt', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---GiftReceipt---AllowSpecialOrderPickupItemsOnReceipt', 'Sale', '_AllowSpecialOrderPickupItemsOnReceipt', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---GiftReceipt---AllowLayawayPickupItemsOnReceipt', 'Sale', '_AllowLayawayPickupItemsOnReceipt', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---GiftReceipt---PromptForMultipleGiftReceipts', 'Sale', '_PromptForMultipleGiftReceipts', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---PromptForCustomer', 'Return', '_ReturnPromptForCustomer', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---CustomerRecordRequired', 'Return', '_ReturnCustomerRecordRequired', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---CreditCommissionedAsscMethod---CreditBlindReturnSaleCommissionMethod', 'Return', '_CreditBlindReturnSaleCommissionMethod', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---CreditCommissionedAsscMethod---CreditUnverifiedReturnSaleCommissionMethod', 'Return', '_CreditUnverifiedReturnSaleCommissionMethod', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---CreditCommissionedAsscMethod---CreditVerifiedReturnSaleCommissionMethod', 'Return', '_CreditVerifiedReturnSaleCommissionMethod', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---SearchBySerialOnBlindReturn', 'Return', '_SearchBySerialOnBlindReturn', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---AllowItemNotOnFileBlindReturn', 'Return', '_AllowItemNotOnFileBlindReturn', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---AlwaysPromptForItemPriceUponVerifiedReturn', 'Return', '_AlwaysPromptForItemPriceUponVerifiedReturn', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---AlwaysPromptForItemPriceUponUnverifiedReturn', 'Return', '_AlwaysPromptForItemPriceUponUnverifiedReturn', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---AlwaysPromptForItemPriceUponBlindReturn', 'Return', '_AlwaysPromptForItemPriceUponBlindReturn', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---PriceHistory---LookupPriceHistory', 'Return', '_LookupPriceHistory', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---PriceHistory---LookupVerifiedPriceHistory', 'Return', '_LookupVerifiedPriceHistory', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---PriceHistory---PriceHistoryLookupPreviousNumberOfDays', 'Return', '_PriceHistoryLookupPreviousNumberOfDays', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---RestockingFee---PromptApplyRestockingFeeMessage', 'Return', '_PromptApplyRestockingFeeMessage', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---ReturnVerification---ReturnVerificationRequired', 'Return', '_ReturnVerificationRequired', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---MaxTotalTransactionRefundThreshold', 'Return', '_MaxTotalTransactionRefundThreshold', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---MinimumReturnItemPrice', 'Return', '_MinimumReturnItemPrice', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---MaximumReturnItemPrice', 'Return', '_MaximumReturnItemPrice', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---MaxDaysAfterPurchase', 'Return', '_MaxDaysAfterPurchase', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---Layaway---AllowLayawayItemReturn', 'Return', '_AllowLayawayItemReturn', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---SpecialOrder---AllowSpecialOrderItemReturn', 'Return', '_AllowSpecialOrderItemReturn', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---SendSale---AllowSendSaleItemReturn', 'Return', '_AllowSendSaleItemReturn', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---ValidateReturnSerialNumberAgainstOriginalTrans', 'Return', '_ValidateReturnSerialNumberAgainstOriginalTrans', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---PromptTndrAmtForOriginalCreditCardTender', 'Return', '_PromptTndrAmtForOriginalCreditCardTender', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---Discounts---AllowDiscountsOnBlindReturn', 'Return', '_AllowDiscountsOnBlindReturn', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---Discounts---AllowDiscountsOnUnverifiedReturn', 'Return', '_AllowDiscountsOnUnverifiedReturn', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---Discounts---AllowDiscountsOnVerifiedReturn', 'Return', '_AllowDiscountsOnVerifiedReturn', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemMessaging---DisplayMessage', 'Item Messaging', '_ItemMessagingDisplayMessage', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemMessaging---ClearViewOnBlankMessage', 'Item Messaging', '_ClearViewOnBlankMessage', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemMessaging---StyleMessageSupersede', 'Item Messaging', '_ItemMessagingStyleMessageSupersede', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemMessaging---DisplayMessageAsNotify', 'Item Messaging', '_ItemMessagingDisplayMessageAsNotify', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemMessaging---DisplayOnSaleItem', 'Item Messaging', '_ItemMessagingDisplayOnSaleItem', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemMessaging---DisplayOnSendItem', 'Item Messaging', '_ItemMessagingDisplayOnSendItem', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemMessaging---DisplayOnReturnItem', 'Item Messaging', '_ItemMessagingDisplayOnReturnItem', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---AttachedItems---EnableAttachedItems', 'Sale', '_AttachedItemsEnableAttachedItems', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---AttachedItems---NotifyItemAdded', 'Sale', '_AttachedItemsNotifyItemAdded', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---AttachedItems---AddWithMessage', 'Sale', '_AttachedItemsAddWithMessage', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---PromptForCustomerOnLogin', 'Customer', '_PromptForCustomerOnLogin', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---AutoGenerateCustomerId', 'Customer', '_AutoGenerateCustomerId', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---AutoGenerateEmployeeId', 'Employee', '_AutoGenerateEmployeeId', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ShowListsIfOne', 'Employee', '_DefaultEmployeeShowListsIfOne', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ShowEmployeeListIfOne', 'Employee', '_ShowEmployeeListIfOne', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ShowCustomerListIfOne', 'Customer', '_ShowCustomerListIfOne', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ReturnToCustomerSearchAfterSave', 'Customer', '_ReturnToCustomerSearchAfterSave', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SuspendTransaction---PrintSuspendTransReceipt', 'Suspend Resume', '_PrintSuspendTransReceipt', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SendSale---OneStep', 'Send Sale', '_SendSaleOneStep', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SendSale---EnhancedSendSale', 'Send Sale', '_EnhancedSendSale', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SendSale---AutoGenerateShippingFee', 'Send Sale', '_SendSaleAutoGenerateShippingFee', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SendSale---GenerateWeightedShippingFee', 'Send Sale', '_SendSaleGenerateWeightedShippingFee', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SendSale---ShipperInfoRequired', 'Send Sale', '_SendSaleShipperInfoRequired', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SendSale---DefaultStoreOrignatorCode', 'Send Sale', '_SendSaleDefaultStoreOrignatorCode', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SendSale---RequireCustomer', 'Send Sale', '_SendSaleRequireCustomer', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SendSale---CustomerIsDefaultDestination', 'Send Sale', '_SendSaleCustomerIsDefaultDestination', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SendSale---SendTaxType', 'Send Sale', '_SendSaleSendTaxType', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SendSale---UseThisStoreAsFailOverTaxRate', 'Send Sale', '_SendSaleUseThisStoreAsFailOverTaxRate', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SendSale---DisplaySoldItemCount', 'Send Sale', '_SendSaleDisplaySoldItemCount', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SendSale---PrintSendSaleMerchandiseTicketPerItem', 'Send Sale', '_PrintSendSaleMerchandiseTicketPerItem', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SendSale---EmailStore', 'Send Sale', '_SendSaleEmailStore', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SendSale---UnverifiedReturnsPromptSendSale', 'Send Sale', '_UnverifiedReturnsPromptSendSale', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---RemoteSend---AutoGenerateShippingFee', 'Remote Send', '_RemoteSendAutoGenerateShippingFee', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---RemoteSend---SendTaxType', 'Remote Send', '_RemoteSendSendTaxType', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---RemoteSend---UseThisStoreAsFailOverTaxRate', 'Remote Send', '_RemoteSendUseThisStoreAsFailOverTaxRate', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---ApplyDealAtSetup', 'Layaway', '_LayawayApplyDealAtSetup', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---AllowAddItemAfterSetup', 'Layaway', '_LayawayAllowAddItemAfterSetup', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---AccountType', 'Layaway', '_LayawayAccountType', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---AutoGenerateLayawayId', 'Layaway', '_AutoGenerateLayawayId', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---AllowItemNotOnFileLayaway', 'Layaway', '_AllowItemNotOnFileLayaway', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---MaxLayawayItems', 'Layaway', '_MaxLayawayItems', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---MinTotalItemPrice', 'Layaway', '_LayawayMinTotalItemPrice', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---MinimumPaymentPctOfRecommended', 'Layaway', '_LayawayMinimumPaymentPctOfRecommended', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---MinimumPaymentDollarAmount', 'Layaway', '_LayawayMinimumPaymentDollarAmount', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---SkipSearchFormIfCustomer', 'Layaway', '_LayawaySkipSearchFormIfCustomer', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---AllowOverpayment', 'Layaway', '_LayawayAllowOverpayment', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---SetupFeeType', 'Layaway', '_LayawaySetupFeeType', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---SetupFeeAmount', 'Layaway', '_LayawaySetupFeeAmount', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---SetupFeePercentage', 'Layaway', '_LayawaySetupFeePercentage', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---RefundSetupFeeOnCancel', 'Layaway', '_LayawayRefundSetupFeeOnCancel', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---ChargeSetupFeeOnEdit', 'Layaway', '_LayawayChargeSetupFeeOnEdit', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---AddSetupFeeToLayawayAccount', 'Layaway', '_AddSetupFeeToLayawayAccount', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---DepositFeeType', 'Layaway', '_LayawayDepositFeeType', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---DepositFixedAmount', 'Layaway', '_LayawayDepositFixedAmount', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---DepositPercentage', 'Layaway', '_LayawayDepositPercentage', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---DelinquencyFeeType', 'Layaway', '_LayawayDelinquencyFeeType', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---DelinquencyFeeAmount', 'Layaway', '_LayawayDelinquencyFeeAmount', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---DelinquencyFeePercentage', 'Layaway', '_LayawayDelinquencyFeePercentage', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---RestockingFeeType', 'Layaway', '_LayawayRestockingFeeType', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---RestockingFeeFixedAmount', 'Layaway', '_LayawayRestockingFeeFixedAmount', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---RestockingFeePercentage', 'Layaway', '_LayawayRestockingFeePercentage', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---BookAsSaleOnSetup', 'Layaway', '_LayawayBookAsSaleOnSetup', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---AllowPartialPickups', 'Layaway', '_LayawayAllowPartialPickups', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---AutoAdjustItemPriceAtPickup', 'Layaway', '_LayawayAutoAdjustItemPriceAtPickup', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---PaymentPeriods', 'Layaway', '_LayawayPaymentPeriods', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---PaymentPeriodDaysLength', 'Layaway', '_LayawayPaymentPeriodDaysLength', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---PromptForPaymentPeriods', 'Layaway', '_LayawayPromptForPaymentPeriods', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---PromptForPaymentPeriodDaysLength', 'Layaway', '_LayawayPromptForPaymentPeriodDaysLength', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---PromptForPaymentScheduleChange', 'Layaway', '_LayawayPromptForPaymentScheduleChange', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---DelinquentDaysAfterDue', 'Layaway', '_LayawayDelinquentDaysAfterDue', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---AllowNewLayawayWithDelinquentExisting', 'Layaway', '_AllowNewLayawayWithDelinquentExisting', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---PrintLayawayOnlyReceipt', 'Layaway', '_PrintLayawayOnlyReceipt', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---PrintLayawayMerchandiseTicket', 'Layaway', '_PrintLayawayMerchandiseTicket', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---PrintLayawayMerchandiseTicketPerItem', 'Layaway', '_PrintLayawayMerchandiseTicketPerItem', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---DisplayLayawayItemOnTransactionList', 'Layaway', '_DisplayLayawayItemOnTransactionList', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---MinDaysBeforeEscrowAllowed', 'Layaway', '_LayawayMinDaysBeforeEscrowAllowed', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---AllowEscrowDayOfPayment', 'Layaway', '_LayawayAllowEscrowDayOfPayment', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SpecialOrder---ApplyDealAtSetup', 'Special Order', '_SPApplyDealAtSetup', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SpecialOrder---MinimalOrderMode', 'Special Order', '_SPMinimalOrderMode', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SpecialOrder---AutoGenerateReceivingDoc', 'Special Order', '_SPAutoGenerateReceivingDoc', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SpecialOrder---AutoGenerateSpecOrderId', 'Special Order', '_AutoGenerateSpecOrderId', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SpecialOrder---AllowItemNotOnFileSpecOrder', 'Special Order', '_SPAllowItemNotOnFileSpecOrder', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SpecialOrder---MaxItems', 'Special Order', '_SPMaxItems', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SpecialOrder---MinTotalItemPrice', 'Special Order', '_SPMinTotalItemPrice', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SpecialOrder---CheckInventoryOnItemAdd', 'Special Order', '_SPCheckInventoryOnItemAdd', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SpecialOrder---PromptForExpectedDate', 'Special Order', '_SPPromptForExpectedDate', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SpecialOrder---MarkReadyForPickupOnSetup', 'Special Order', '_SPMarkReadyForPickupOnSetup', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SpecialOrder---ForceStorePickup', 'Special Order', '_SPForceStorePickup', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SpecialOrder---AllowPartialPickups', 'Special Order', '_SPAllowPartialPickups', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SpecialOrder---AutoAdjustItemPriceAtPickup', 'Special Order', '_SPAutoAdjustItemPriceAtPickup', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SpecialOrder---ServiceFeeType', 'Special Order', '_SPServiceFeeType', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SpecialOrder---ServiceFeeAmount', 'Special Order', '_SPServiceFeeAmount', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SpecialOrder---ServiceFeePercentage', 'Special Order', '_SPServiceFeePercentage', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SpecialOrder---DepositFeeType', 'Special Order', '_SPDepositFeeType', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SpecialOrder---DepositFixedAmount', 'Special Order', '_SPDepositFixedAmount', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SpecialOrder---DepositPercentage', 'Special Order', '_SPDepositPercentage', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SpecialOrder---ShippingFeeType', 'Special Order', '_SPShippingFeeType', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SpecialOrder---ShippingFixedAmount', 'Special Order', '_SPShippingFixedAmount', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SpecialOrder---ShippingPercentage', 'Special Order', '_SPShippingPercentage', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SpecialOrder---PromptShippingFeeAdded', 'Special Order', '_SPPromptShippingFeeAdded', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SpecialOrder---RestockingFeeType', 'Special Order', '_SPRestockingFeeType', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SpecialOrder---RestockingFeeFixedAmount', 'Special Order', '_SPRestockingFeeFixedAmount', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SpecialOrder---RestockingFeePercentage', 'Special Order', '_SPRestockingFeePercentage', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SpecialOrder---BookAsSaleOnSetup', 'Special Order', '_SPBookAsSaleOnSetup', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SpecialOrder---MinDaysBeforeEscrowAllowed', 'Special Order', '_SPMinDaysBeforeEscrowAllowed', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SpecialOrder---AllowEscrowDayOfPayment', 'Special Order', '_SPAllowEscrowDayOfPayment', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---WorkOrder---BatchShipMode', 'Work Order', '_WorkOrderBatchShipMode', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---WorkOrder---UseCategoryServiceLocations', 'Work Order', '_WorkOrderUseCategoryServiceLocations', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---WorkOrder---DefaultLeadDays', 'Work Order', '_WorkOrderDefaultLeadDays', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---WorkOrder---AutoGenerateWorkOrderId', 'Work Order', '_AutoGenerateWorkOrderId', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---WorkOrder---AllowMultipleItemsPerWorkOrder', 'Work Order', '_AllowMultipleItemsPerWorkOrder', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---WorkOrder---RequireDeposit', 'Work Order', '_WorkOrderRequireDeposit', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---WorkOrder---DepositPercentage', 'Work Order', '_WorkOrderDepositPercentage', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---WorkOrder---WorkItemSerialPromptMethod', 'Work Order', '_WorkItemSerialPromptMethod', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Warranty---AutoGenerateWarrantyId', 'Warranty', '_AutoGenerateWarrantyId', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Warranty---CustomerRecordRequired', 'Warranty', '_WarrantyCustomerRecordRequired', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Warranty---CustomerAcceptancePrompted', 'Warranty', '_CustomerAcceptancePrompted', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Warranty---NotifyWhenAnyWarranty', 'Warranty', '_NotifyWhenAnyWarranty', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Warranty---NotifyWhenNoWarranty', 'Warranty', '_NotifyWhenNoWarranty', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Warranty---NotifyWhenOneWarranty', 'Warranty', '_NotifyWhenOneWarranty', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Shipping---DocumentAttentionDays', 'Shipping', '_ShippingDocumentAttentionDays', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Shipping---AllowForcedSku', 'Shipping', '_ShippingAllowForcedSku', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Shipping---AutogenerateShippingDocumentId', 'Shipping', '_AutogenerateShippingDocumentId', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Receiving---DocumentAttentionDays', 'Receiving', '_DocumentAttentionDays', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Receiving---AllowForcedSku', 'Receiving', '_ReceivingAllowForcedSku', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Receiving---ReceivingDocumentIdRule', 'Receiving', '_ReceivingDocumentIdRule', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Receiving---Blind', 'Receiving', '_ReceivingBlind', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Receiving---CartonListOnDocument', 'Receiving', '_CartonListOnDocument', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Receiving---AllowReceiveDocInTotal', 'Receiving', '_AllowReceiveDocInTotal', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Receiving---ValidateControlNumber', 'Receiving', '_ValidateControlNumber', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ShelfLabelsItemTags---AllowNonMerchTickets', 'Shelf Label', '_AllowNonMerchTickets', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ShelfLabelsItemTags---MaxQuantityPerLine', 'Shelf Label', '_ShelfLabelsItemTagsMaxQuantityPerLine', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Tax---PromptForTaxChangeReason', 'Tax', '_PromptForTaxChangeReason', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Tax---PromptForTaxExemptReason', 'Tax', '_PromptForTaxExemptReason', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Tax---AllowExemptAllTaxLines', 'Tax', '_AllowExemptAllTaxLines', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Tax---ChangeIndividualTaxLines', 'Tax', '_ChangeIndividualTaxLines', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Tax---TaxGroupLines', 'Tax', '_TaxGroupLines', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Tax---CompoundTaxIncludeOtherCompTaxAmt', 'Tax', '_CompoundTaxIncludeOtherCompTaxAmt', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---UseTillAccountability', 'Till', '_UseTillAccountability', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---PromptToOpenStoreBankOnStoreOpen', 'Till', '_PromptToOpenStoreBankOnStoreOpen', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---StoreBankOpeningCountMethod', 'Till', '_StoreBankOpeningCountMethod', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---StartTillCountMethod', 'Till', '_StartTillCountMethod', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---TillPickupMethod', 'Till', '_TillPickupMethod', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---CashTransferCountMethod', 'Till', '_CashTransferCountMethod', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---StoreBankCashDepositCountMethod', 'Till', '_StoreBankCashDepositCountMethod', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---SuggestedAfterCashPickUpAmtUponOverMax', 'Till', '_SuggestedAfterCashPickUpAmtUponOverMax', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---PrintPickupTotalOnTillCountReceipt', 'Till', '_PrintPickupTotalOnTillCountReceipt', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---DisplayMsgTillCountStatus', 'Till', '_DisplayMsgTillCountStatus', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---InitialCountSummarySkipped', 'Till', '_InitialCountSummarySkipped', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---MinimumCashInTill', 'Till', '_MinimumCashInTill', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---CountSummaryViewTypeMidDay', 'Till', '_CountSummaryViewTypeMidDay', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---CountSummaryViewTypeTillCount', 'Till', '_CountSummaryViewTypeTillCount', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---CountSummaryViewTypeReconcile', 'Till', '_CountSummaryViewTypeReconcile', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---StoreBankEndCountAtStoreCloseRequired', 'Till', '_StoreBankEndCountAtStoreCloseRequired', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---StoreBankEndCountAtStoreCloseMethod', 'Till', '_StoreBankEndCountAtStoreCloseMethod', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---StoreBankDepositValidated', 'Till', '_StoreBankDepositValidated', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---StoreBankDepositDiscrepancyAutoAdjusted', 'Till', '_StoreBankDepositDiscrepancyAutoAdjusted', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---RecountCloseCountDiscrepancyOverThreshold', 'Till', '_RecountCloseCountDiscrepancyOverThreshold', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---TillCountOverThreshold---RequireReentry', 'Till', '_TillRequireReentry', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---TillCountOverThreshold---AcceptSameCount', 'Till', '_TillAcceptSameCount', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Deals---DisplayProportionDealAmt', 'Deal', '_DisplayPorportionDealAmt', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Deals---DisplayNegative', 'Deal', '_DisplayNegative', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---PostVoid---PromptForReason', 'Post Void', '_PostVoidPromptForReason', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Scheduling---FutureSchedulingWeeks', 'Scheduling', '_FutureSchedulingWeeks', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Scheduling---ScheduleDailyViewStartHour', 'Scheduling', '_ScheduleDailyViewStartHour', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Scheduling---ScheduleDailyViewEndHour', 'Scheduling', '_ScheduleDailyViewEndHour', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Scheduling---TimeOff---FutureTimeOffWeeks', 'Scheduling', '_FutureTimeOffWeeks', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Scheduling---TimeOff---PastTimeOffWeeks', 'Scheduling', '_PastTimeOffWeeks', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Timecard---DefaultActiveEmployeeListSortType', 'Timecard', '_DefaultActiveEmployeeListSortType', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Timecard---Exception---TimecardHourGreaterThanHours', 'Timecard', '_TimecardHourGreaterThanHours', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Timecard---Exception---TimecardHourLessThanHours', 'Timecard', '_TimecardHourLessThanHours', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Timecard---PrintAcceptanceForm', 'Timecard', '_TimecardPrintAcceptanceForm', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Timecard---DisplayDeletedTimecardEntry', 'Timecard', '_DisplayDeletedTimecardEntry', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Timecard---AllowFutureTimecardEntry', 'Timecard', '_AllowFutureTimecardEntry', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Timecard---TimecardEntryRoundingMinutes', 'Timecard', '_TimecardEntryRoundingMinutes', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemLookup---PriceHistoryDays', 'Item Lookup', '_PriceHistoryDays', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemLookup---DepartmentChild', 'Item Lookup', '_ItemLookupDepartmentChild', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemLookup---LoadUpcs', 'Item Lookup', '_ItemLookupLoadUpcs', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemLookup---LoadSimilar', 'Item Lookup', '_ItemLookupLoadSimilar', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemLookup---LoadSubstitute', 'Item Lookup', '_ItemLookupLoadSubstitute', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemLookup---LoadPriceHistory', 'Item Lookup', '_ItemLookupLoadPriceHistory', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemLookup---LoadGrid', 'Item Lookup', '_ItemLookupLoadGrid', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemLookup---LoadVendorItems', 'Item Lookup', '_ItemLookupLoadVendorItems', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Payroll---PayrollHoursRoundingMinutes', 'Payroll', '_PayrollHoursRoundingMinutes', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Payroll---PostPayroll---EnforcePostPayrollDateTime', 'Payroll', '_EnforcePostPayrollDateTime', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Payroll---PostPayroll---PayrollPostDay', 'Payroll', '_PayrollPostDay', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Payroll---PostPayroll---PayrollPostTime', 'Payroll', '_PayrollPostTime', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Payroll---PostPayroll---DisplayPayrollNotYetPostMsgAtLoginIfPassPostDay', 'Payroll', '_DisplayPayrollNotYetPostMsgAtLoginIfPassPostDay', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Payroll---PostPayroll---OnlyWriteHourlyPayStatusEmpInPayrollLog', 'Payroll', '_OnlyWriteHourlyPayStatusEmpInPayrollLog', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Payroll---PostPayroll---AllowAutoPostWithError', 'Payroll', '_PayrollAllowAutoPostWithError', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Payroll---PayrollCalendar---DisplayNumberOfPreviousWeek', 'Payroll', '_PayrollDisplayNumberOfPreviousWeek', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Payroll---PayrollCalendar---DisplayNumberOfPreviousWeekinReport', 'Payroll', '_PayrollDisplayNumberOfPreviousWeekinReport', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Payroll---AllowRepostingPayroll', 'Payroll', '_AllowRepostingPayroll', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Payroll---MaxDailyTotalPayrollHours', 'Payroll', '_MaxDailyTotalPayrollHours', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Payroll---ReviewPayrollRequired', 'Payroll', '_ReviewPayrollRequired', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Payroll---AllowPostPayrollPerEmployee', 'Payroll', '_AllowPostPayrollPerEmployee', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Payroll---BasicCalendarPayrollWeekendingDay', 'Payroll', '_BasicCalendarPayrollWeekendingDay', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Payroll---MaxAssociateAdvanceAmount', 'Associate Advance', '_MaxAssociateAdvanceAmount', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Payroll---IsRegularPayrollHoursFromTimeCard', 'Payroll', '_IsRegularPayrollHoursFromTimeCard', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Payroll---AssocAdvanceAuthNumber', 'Associate Advance', '_AssocAdvanceAuthNumber', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Payroll---AllowFuturePayrollEntry', 'Payroll', '_AllowFuturePayrollEntry', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Payroll---AssocAdvanceRepaymentAmount', 'Associate Advance', '_AssocAdvanceRepaymentAmount', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---AllowSaleItemTypeInMixedTransaction---RETURN', 'Sale', '_AllowSaleItemTypeInMixedTransactionRETURN', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---AllowSaleItemTypeInMixedTransaction---LAYAWAY', 'Sale', '_AllowSaleItemTypeInMixedTransactionLAYAWAY', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---AllowSaleItemTypeInMixedTransaction---SPECIALORDER', 'Sale', '_AllowSaleItemTypeInMixedTransactionSPECIALORDER', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---AllowSaleItemTypeInMixedTransaction---SENDSALE', 'Sale', '_AllowSaleItemTypeInMixedTransactionSENDSALE', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---AllowSaleItemTypeInMixedTransaction---WORKORDER', 'Sale', '_AllowSaleItemTypeInMixedTransactionWORKORDER', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---AllowSaleItemTypeInMixedTransaction---REMOTESEND', 'Sale', '_AllowSaleItemTypeInMixedTransactionREMOTESEND', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Tlog---GenerateTLog', 'Tlog', '_GenerateTLog', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Tlog---MarkAsPostedImmediately', 'Tlog', '_TLogMarkAsPostedImmediately', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Tender---ManualEnteredCreditCardImprint', 'Tender', '_ManualEnteredCreditCardImprint', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---CurrencyRounding---RoundingMode', 'Tender', '_RoundingMode', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---IncludeEmployeeInDefaultGroup', 'Employee', '_IncludeEmployeeInDefaultGroup', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---InventoryAdjustment---EnterAdjustmentQtyOnly', 'Inventory Adjustment', '_EnterAdjustmentQtyOnly', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Discounts---RoundingMode', 'Discount', '_DiscountRoundingMode', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LineItemDiscount---LineDiscountUseConfiguredScale', 'Discount', '_LineDiscountUseConfiguredScale', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LineItemDiscount---LineDiscountPrecision', 'Discount', '_LineDiscountPrecision', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LineItemDiscount---DiscountThreshold---Enabled', 'Discount', '_LineItemDiscountDiscountThresholdEnabled', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LineItemDiscount---DiscountThreshold---Amount', 'Discount', '_LineItemDiscountDiscountThresholdAmount', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LineItemDiscount---DiscountThreshold---Percent', 'Discount', '_LineItemDiscountDiscountThresholdPercent', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---GroupDiscount---ConfirmItemVoid', 'Discount', '_ConfirmItemVoid', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---GroupDiscount---MinItemsRequired', 'Discount', '_MinItemsRequired', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---YouSavedMessage---ShowYouSavedMessage', 'Receipt', '_ShowYouSavedMessage', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---YouSavedMessage---MinYouSavedMessageThresholdAmount', 'Receipt', '_MinYouSavedMessageThresholdAmount', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---IrsIdentificationNumber', 'Tender', '_IrsIdentificationNumber', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---CashPaymentRequiringIrsReport', 'Tender', '_CashPaymentRequiringIrsReport', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Document---SeriesIdLength', 'Document', '_DocumentSeriesIdLength', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TenderExchange---AllowMultipleIncomingTenders', 'Tender Exchange', '_AllowMultipleIncomingTenders', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TenderExchange---AllowMultipleOutgoingTenders', 'Tender Exchange', '_AllowMultipleOutgoingTenders', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---EmployeeSearchCurrentLocationOnly', 'Employee', '_EmployeeSearchCurrentLocationOnly', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---CriteriaRequiredForEmployeeSearch', 'Employee', '_CriteriaRequiredForEmployeeSearch', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---MinimumEmployeeIdLength', 'Employee', '_MinimumEmployeeIdLength', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---MaximumEmployeeIdLength', 'Employee', '_MaximumEmployeeIdLength', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---DatabaseTranslationsEnabled', 'System', '_DatabaseTranslationsEnabled', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---MaximumKeyedQuantityThreshold', 'Inventory', '_MaximumKeyedQuantityThreshold', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---VoucherNumberMask', 'Voucher', '_VoucherNumberMask', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TaxWare---CompanyId', 'Taxware', '_TaxwareCompanyId', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TaxWare---UseSTEP', 'Taxware', '_TaxwareUseSTEP', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ErrorScreen---EmailAddressFrom', 'System', '_EmailAddressFrom', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ErrorScreen---EmailAddressTo', 'System', '_EmailAddressTo', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ErrorScreen---EmailServerAddress', 'System', '_MailServer', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ErrorScreen---EmailServerPort', 'System', '_MailServerPort', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LocationBasedInventory---LocationDisablingAllowed', 'Inventory', '_LocationDisablingAllowed', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LocationBasedInventory---NotifySummaryCountDiscrepancy', 'Inventory', '_NotifySummaryCountDiscrepancy', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LocationBasedInventory---ItemNotInSourceTransferAllowed', 'Inventory', '_ItemNotInSourceTransferAllowed', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LocationBasedInventory---PromptItemTransferReason', 'Inventory', '_PromptItemTransferReason', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LocationBasedInventory---InventoryValueTrackingMethod', 'Inventory', '_InventoryValueTrackingMethod', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LocationBasedInventory---PromptForReceivingLocation', 'Inventory', '_LocationBasedInventoryPromptForReceivingLocation', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LocationBasedInventory---AutoReceiveLocationBasedOnDocConfig', 'Inventory', '_AutoReceiveLocationBasedOnDocConfig', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LocationBasedInventory---InventoryLocationPromptMethod', 'Inventory', '_InventoryLocationPromptMethod', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LocationBasedInventory---DisplayOnlyLocationsWithNonZeroQuantity', 'Inventory', '_DisplayOnlyLocationsWithNonZeroQuantity', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LocationBasedInventory---TrackMovementPending', 'Inventory', '_TrackMovementPending', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LocationBasedInventory---ValidateSerialNumber', 'Inventory', '_LocationBasedInventoryValidateSerialNumber', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LocationBasedInventory---PrintSoldFromInventoryLocationOnReceipt', 'Inventory', '_PrintSoldFromInventoryLocationOnReceipt', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LocationBasedInventory---PrintMovementPendingReconciliationReport', 'Inventory', '_PrintMovementPendingReconciliationReport', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LocationBasedInventory---PrintItemTransferReport', 'Inventory', '_PrintItemTransferReport', NULL, 10);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LocationBasedInventory---QuantityUnavailableAction', 'Inventory', '_QuantityUnavailableAction', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---PriceHistory---AutoCalculateHistoryPrice', 'Return', '_autoCalculateHistoryPrice', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SuspendTransaction---AllowSuspendTransWithVerifiedReturn', 'Suspend Resume', '_AllowSuspendTransWithVerifiedReturn', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ErrorScreen---AutoGenerateErrorNotification', 'System', '_AutoGenerateErrorNotification', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Help---HelpKey', 'System', '_DisableHelpFunction', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Help---HelpFilePath', 'System', '_HelpFilePath', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Help---HelpMenu', 'System', '_HelpMenu', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---CurrencyRounding---CashTotalDisplay', 'Tender', '_CashTotalDisplay', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---CountSummaryViewTypeCashPickup', 'Till', '_CountSummaryViewTypeCashPickup', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---CountSummaryViewTypeStartCount', 'Till', '_CountSummaryViewTypeStartCount', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---InventoryCount---PhysicalCount---AllowCreate', 'Inventory Count', '_PhysicalCountAllowCreate', 'PhysicalCount:AllowCreate', 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---InventoryCount---PhysicalCount---AutoRecountUnmatchedItems', 'Inventory Count', '_PhysicalCountAutoRecountUnmatchedItems', 'PhysicalCount:AutoRecountUnmatchedItems', 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---InventoryCount---PhysicalCount---MaximumCountCycles', 'Inventory Count', '_PhysicalCountMaximumCountCycles', 'PhysicalCount:MaximumCountCycles', 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---InventoryCount---CycleCount---AllowCreate', 'Inventory Count', '_CycleCountAllowCreate', 'CycleCount:AllowCreate', 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---InventoryCount---CycleCount---AutoRecountUnmatchedItems', 'Inventory Count', '_CycleCountAutoRecountUnmatchedItems', 'CycleCount:AutoRecountUnmatchedItems', 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---InventoryCount---CycleCount---MaximumCountCycles', 'Inventory Count', '_CycleCountMaximumCountCycles', 'CycleCount:MaximumCountCycles', 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---InventoryCount---CycleCount---InventoryBucket', 'Inventory Count', '_CycleCountInventoryBucket', 'CycleCount:InventoryBucket', 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---InventoryCount---SupplyCount---AllowCreate', 'Inventory Count', '_SupplyCountAllowCreate', 'SupplyCount:AllowCreate', 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---InventoryCount---SupplyCount---AutoRecountUnmatchedItems', 'Inventory Count', '_SupplyCountAutoRecountUnmatchedItems', 'SupplyCount:AutoRecountUnmatchedItems', 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---InventoryCount---SupplyCount---MaximumCountCycles', 'Inventory Count', '_SupplyCountMaximumCountCycles', 'SupplyCount:MaximumCountCycles', 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---InventoryCount---SupplyCount---InventoryBucket', 'Inventory Count', '_SupplyCountInventoryBucket', 'SupplyCount:InventoryBucket', 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---InventoryCount---SupplyCount---DepartmentForCount', 'Inventory Count', '_SupplyCountDepartmentForCount', 'SupplyCount:DepartmentForCount', 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---InventoryCount---UtilizeHandheldForCounting', 'Inventory Count', '_UtilizeHandheldForCounting', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---InventoryCount---ValidateSectionExistsForBucket', 'Inventory Count', '_ValidateSectionExistsForBucket', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---InventoryCount---ItemsPerCountSheetPagePrimary', 'Inventory Count', '_ItemsPerCountSheetPagePrimary', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---InventoryCount---ItemsPerCountSheetPageSecondary', 'Inventory Count', '_ItemsPerCountSheetPageSecondary', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---InventoryCount---RequirePrintedCountSheet', 'Inventory Count', '_RequirePrintedCountSheet', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---InventoryCount---AllowEnterCountsBeforeInitiating', 'Inventory Count', '_AllowEnterCountsBeforeInitiating', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---InventoryCount---DaysBeforeBeginDateInitiateAllowed', 'Inventory Count', '_DaysBeforeBeginDateInitiateAllowed', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---InventoryCount---DaysAfterBeginDateInitiatedAllowed', 'Inventory Count', '_DaysAfterBeginDateInitiatedAllowed', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---InventoryCount---EnforceInitiateCountDate', 'Inventory Count', '_EnforceInitiateCountDate', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---InventoryCount---BlankLinesPerPrintedCountSheet', 'Inventory Count', '_BlankLinesPerPrintedCountSheet', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---InventoryCount---PrintBlankLinesAfterPrimaryCycle', 'Inventory Count', '_PrintBlankLinesAfterPrimaryCycle', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Email---DefaultMailHost', 'System', '_DefaultMailHost', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Email---DefaultMailPort', 'System', '_DefaultMailPort', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Email---DefaultSender', 'System', '_DefaultSender', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Email---DefaultRecipient', 'System', '_DefaultRecipient', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemMessaging---CompositeMessageImagePosition', 'Item Messaging', '_CompositeMessageImagePosition', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---ValidateCashTenderGoNegative', 'Till', '_ValiateCashTenderGoNegative', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---AllowSaleItemTypeInMixedTransaction---SALE', 'Sale', '_AllowSaleItemTypeInMixedTransactionSALE', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---XPocket---ExportDirectory', 'XPocket', '_XPocketExportDirectory', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Email---UseSmtpAuth', 'System', '_UseSmtpAuth', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Email---DefaultMailUser', 'System', '_DefaultMailUser', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Email---DefaultMailPassword', 'System', '_DefaultMailPassword', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Email---SmtpDebug', 'System', '_SmtpDebug', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Email---Receipt---SendEmailReceipts', 'System', '_SendEmailReceipts', 'EmailReceipt:SendEmailReceipts', 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Email---Receipt---Subject', 'System', '_SendEmailReceiptsSubject', 'EmailReceipt:Subject', 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Email---Receipt---Body', 'System', '_SendEmailReceiptsBody', 'EmailReceipt:Body', 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Email---Receipt---From', 'System', '_SendEmailReceiptsFrom', 'EmailReceipt:From', 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Email---Receipt---Watermark', 'System', '_SendEmailReceiptsWatermark', 'EmailReceipt:Watermark', 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---CreditDebitTender---BINSmartLookup---Enabled', 'Tender', '_BINSmartLookupEnabled', 'BINSmartLookup:Enabled', 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---CreditDebitTender---BINSmartLookup---AuthMethodCode', 'Tender', '_BINSmartLookupAuthMethodCode', 'BINSmartLookup:AuthMethodCode', 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---CreditDebitTender---BINSmartLookup---PreferredDefaultOnError', 'Tender', '_BINSmartLookupPreferredDefaultOnError', 'BINSmartLookup:PreferredDefaultOnError', 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---ProrationTimeUnit', 'Return', '_ProrationTimeUnit', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---InventoryCount---MaxItemsPerCountSection', 'Inventory Count', '_MaxItemsPerCountSection', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---XPocket---ImportDirectory', 'XPocket', '_XPocketImportDirectory', NULL, 60);
INSERT INTO cfg_config_properties (category, config_name, sub_category, description, short_description, sort_order)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---XPocket---ArchiveDirectory', 'XPocket', '_XPocketArchiveDirectory', NULL, 60);
GO

-- CFG_CODE_VALUE
IF NOT EXISTS (SELECT 1 FROM cfg_code_value WHERE category = 'AVAILABLE_LOCALE' AND sub_category = 'DEFAULT')
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('AVAILABLE_LOCALE', 'DEFAULT', 'en_US', 'DEFAULT', 'US English', 10, NULL, NULL, NULL);
GO

IF NOT EXISTS (SELECT 1 FROM cfg_code_value WHERE category = 'CONFIG_PATH' AND sub_category = 'DEFAULT')
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('CONFIG_PATH', 'DEFAULT', '/version1', 'DEFAULT', 'cust', 10, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'CONFIG_PATH_GROUP' AND sub_category = 'DEFAULT';
--INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
--  VALUES ('CONFIG_PATH_GROUP', 'DEFAULT', 'version1', 'DEFAULT', 'All Customer Configs', 0, NULL, NULL, NULL);
GO

IF NOT EXISTS (SELECT 1 FROM cfg_code_value where config_name ='DeployDeliveryWindow' and category = 'ConfiguratorConfig' and sub_category='DEFAULT')
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('ConfiguratorConfig', 'DeployDeliveryWindow', '14', 'DEFAULT', '_DeployDeliveryWindow', 10, 'true', 'com.micros_retail.configurator.deployment.deploymentConfigChanged', 'number');
GO

IF NOT EXISTS (SELECT 1 FROM cfg_code_value where config_name ='DeploymentFailureTime' and category = 'ConfiguratorConfig' and sub_category='DEFAULT')
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('ConfiguratorConfig', 'DeploymentFailureTime', '8', 'DEFAULT', '_deploymentFailureTime', 10, 'true', 'com.micros_retail.configurator.deployment.deploymentConfigChanged', 'hourOfDay');
GO

IF NOT EXISTS (SELECT 1 FROM cfg_code_value where config_name ='DeploymentPollingTime' and category = 'ConfiguratorConfig' and sub_category='DEFAULT')
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('ConfiguratorConfig', 'DeploymentPollingTime', '1', 'DEFAULT', '_deploymentPollingTime', 10, 'true', 'com.micros_retail.configurator.deployment.deploymentConfigChanged', 'hourOfDay');
GO

IF NOT EXISTS (SELECT 1 FROM cfg_code_value where config_name ='DeploymentSchedulerInterval' and category = 'ConfiguratorConfig' and sub_category='DEFAULT')
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('ConfiguratorConfig', 'DeploymentSchedulerInterval', '3600000', 'DEFAULT', '_DeploymentSchedulerInterval', 10, 'true', 'com.micros_retail.configurator.deployment.schedulerIntervalChanged', 'MILISECONDS');
GO

IF NOT EXISTS (SELECT 1 FROM cfg_code_value where config_name ='DeploymentDownloadId' and category = 'ConfiguratorConfig' and sub_category='DEFAULT')
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('ConfiguratorConfig', 'DeploymentDownloadId', 'configurator::${sessionId}::${deploymentPlanId}::${retailLocationId}', 'DEFAULT', '_DeploymentDownloadId', 10, 'false', 'com.micros_retail.configurator.deployment.deploymentConfigChanged', 'string');
GO

IF NOT EXISTS (SELECT 1 FROM cfg_code_value where config_name ='EnableAutoFileDeploymentService' and category = 'ConfiguratorConfig' and sub_category='DEFAULT')
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('ConfiguratorConfig', 'EnableAutoFileDeploymentService', 'true', 'DEFAULT', '_enabledAutoFileTransfer', 10, 'true', 'com.micros_retail.configurator.filetransfer.autoSchedulerIntervalChanged', 'boolean');
GO

IF NOT EXISTS (SELECT 1 FROM cfg_code_value where config_name ='FileDeploymentServiceDirectory' and category = 'ConfiguratorConfig' and sub_category='DEFAULT')
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('ConfiguratorConfig', 'FileDeploymentServiceDirectory', '/polling/org${organizationId}/store${retailLocationId}/', 'DEFAULT', '_FileDeploymentServiceDirectory', 10, 'true', 'com.micros_retail.configurator.deployment.deploymentConfigChanged', 'directory');
GO

IF NOT EXISTS (SELECT 1 FROM cfg_code_value where config_name ='DataDeploymentServiceFileName' and category = 'ConfiguratorConfig' and sub_category='DEFAULT')
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('ConfiguratorConfig', 'DataDeploymentServiceFileName', 'cfgSession${sessionId}-deployment${deploymentPlanId}-store${retailLocationId}.dat', 'DEFAULT', '_DataDeploymentServiceFileName', 10, 'true', 'com.micros_retail.configurator.deployment.deploymentConfigChanged', 'string');
GO

IF NOT EXISTS (SELECT 1 FROM cfg_code_value where config_name ='DataDeploymentDateFileNamePattern' and category = 'ConfiguratorConfig' and sub_category='DEFAULT')
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('ConfiguratorConfig', 'DataDeploymentDateFileNamePattern', 'yyyy-MM-dd', 'DEFAULT', '_DataDeploymentDateTimeFileNamePattern', 10, 'false', 'com.micros_retail.configurator.deployment.deploymentConfigChanged', 'string');
GO

IF NOT EXISTS (SELECT 1 FROM cfg_code_value where config_name ='XMLDeploymentServiceFileName' and category = 'ConfiguratorConfig' and sub_category='DEFAULT')
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('ConfiguratorConfig', 'XMLDeploymentServiceFileName', 'configurator.jar', 'DEFAULT', '_XMLDeploymentServiceFileName', 10, 'true', 'com.micros_retail.configurator.deployment.deploymentConfigChanged', 'string');
GO

IF NOT EXISTS (SELECT 1 FROM cfg_code_value where config_name ='DataDeploymentServiceFileHeader' and category = 'ConfiguratorConfig' and sub_category='DEFAULT')
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('ConfiguratorConfig', 'DataDeploymentServiceFileHeader', '<Header line_count="${sessionJournalCount}" download_id="${DeploymentDownloadId}" application_date="${deploymentDate}"/>', 'DEFAULT', '_DataDeploymentServiceFileHeader', 10, 'false', 'com.micros_retail.configurator.deployment.deploymentConfigChanged', 'string');
GO

IF NOT EXISTS (SELECT 1 FROM cfg_code_value where config_name ='DataDeploymentServiceFileLine' and category = 'ConfiguratorConfig' and sub_category='DEFAULT')
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('ConfiguratorConfig', 'DataDeploymentServiceFileLine', 'INSERT~XML_PERSISTABLES~${sessionJournalConfigData}', 'DEFAULT', '_DataDeploymentServiceFileLine', 10, 'false', 'com.micros_retail.configurator.deployment.deploymentConfigChanged', 'string');
GO

IF NOT EXISTS (SELECT 1 FROM cfg_code_value where config_name ='FileTransferDeploymentServiceFileHeader' and category = 'ConfiguratorConfig' and sub_category='DEFAULT')
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('ConfiguratorConfig', 'FileTransferDeploymentServiceFileHeader', '<Header download_id="${DeploymentDownloadId}" application_date="${deploymentDate}"/>', 'DEFAULT', '_FileTransferDeploymentServiceFileHeader', 10, 'false', 'com.micros_retail.configurator.filetransfer.autoSchedulerIntervalChanged', 'string');
GO

IF NOT EXISTS (SELECT 1 FROM cfg_code_value where config_name ='AutoFileTransferDirectory' and category = 'ConfiguratorConfig' and sub_category='DEFAULT')
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('ConfiguratorConfig', 'AutoFileTransferDirectory', '/filetransfer/auto/org${organizationId}/', 'DEFAULT', '_AutoFileTransferDirectory', 10, 'true', 'com.micros_retail.configurator.filetransfer.autoSchedulerIntervalChanged', 'directory');
GO

IF NOT EXISTS (SELECT 1 FROM cfg_code_value where config_name ='AutoFileTransferSchedulerInterval' and category = 'ConfiguratorConfig' and sub_category='DEFAULT')
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('ConfiguratorConfig', 'AutoFileTransferSchedulerInterval', '900000', 'DEFAULT', '_AutoFileTransferSchedulerInterval', 10, 'true', 'com.micros_retail.configurator.filetransfer.autoSchedulerIntervalChanged', 'MILISECONDS');
GO

IF NOT EXISTS (SELECT 1 FROM cfg_code_value where config_name ='ManualFileTransferDirectory' and category = 'ConfiguratorConfig' and sub_category='DEFAULT')
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('ConfiguratorConfig', 'ManualFileTransferDirectory', '/filetransfer/manual/org${organizationId}/', 'DEFAULT', '_ManualFileTransferDirectory', 10, 'true', 'com.micros_retail.configurator.filetransfer.autoSchedulerIntervalChanged', 'directory');
GO

IF NOT EXISTS (SELECT 1 FROM cfg_code_value where config_name ='FileTransferArchiveDirectory' and category = 'ConfiguratorConfig' and sub_category='DEFAULT')
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('ConfiguratorConfig', 'FileTransferArchiveDirectory', '/filetransfer/archive/org${organizationId}/', 'DEFAULT', '_FileTransferArchiveDirectory', 10, 'true', 'com.micros_retail.configurator.filetransfer.autoSchedulerIntervalChanged', 'directory');
GO

IF NOT EXISTS (SELECT 1 FROM cfg_code_value where config_name ='DeploymentFailureWindow' and category = 'ConfiguratorConfig' and sub_category='DEFAULT')
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('ConfiguratorConfig', 'DeploymentFailureWindow', '24', 'DEFAULT', '_DeploymentFailureWindow', 10, 'false', 'com.micros_retail.configurator.deployment.deploymentConfigChanged', 'number');
GO

IF NOT EXISTS (SELECT 1 FROM cfg_code_value where config_name ='DeployedSessionTableRowCount' and category = 'ConfiguratorConfig' and sub_category='DEFAULT')
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('ConfiguratorConfig', 'DeployedSessionTableRowCount', '10', 'DEFAULT', '_DeployedSessionTableRowCount', 10, 'true', 'com.micros_retail.configurator.deployedSessionRowsChanged', 'number');
GO

IF NOT EXISTS (SELECT 1 FROM cfg_code_value where config_name ='DeviceConsideredMissingInXMinutes' and category = 'DeviceRegistrationMonitor' and sub_category='DEFAULT')
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DeviceRegistrationMonitor', 'DeviceConsideredMissingInXMinutes', '15', 'DEFAULT', null, 0, null, null, null);
GO

IF NOT EXISTS (SELECT 1 FROM cfg_code_value where config_name ='IgnoreMissingDeviceAfterXHours' and category = 'DeviceRegistrationMonitor' and sub_category='DEFAULT')
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DeviceRegistrationMonitor', 'IgnoreMissingDeviceAfterXHours', '72', 'DEFAULT', null, 0, null, null, null);
GO

IF NOT EXISTS (SELECT 1 FROM cfg_code_value where config_name ='ScanForMissingDevicesEveryXMinutes' and category = 'DeviceRegistrationMonitor' and sub_category='DEFAULT')
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DeviceRegistrationMonitor', 'ScanForMissingDevicesEveryXMinutes', '15', 'DEFAULT', null, 0, null, null, null);
GO

DELETE FROM cfg_code_value WHERE category = 'DATA' AND config_name = 'CODE_CATEGORY';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'CODE_CATEGORY', 'ADDRESS_TYPE', 'DEFAULT', '_codeCategory_ADDRESS_TYPE', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'CODE_CATEGORY', 'CUST_ACCOUNT_STATE', 'DEFAULT', '_CUST_ACCOUNT_STATE', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'CODE_CATEGORY', 'CUSTOMER_CONTACT_PREF', 'DEFAULT', '_CUSTOMER_CONTACT_PREF', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'CODE_CATEGORY', 'CUSTOMER_GROUPS', 'DEFAULT', '_CUSTOMER_GROUPS', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'CODE_CATEGORY', 'CUSTOMER_LEGAL_STATUS', 'DEFAULT', '_CUSTOMER_LEGAL_STATUS', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'CODE_CATEGORY', 'CUSTOMER_LEVEL', 'DEFAULT', '_CUSTOMER_LEVEL', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'CODE_CATEGORY', 'DISCOUNT_ELIGIBILITY_TYPES', 'DEFAULT', '_DISCOUNT_ELIGIBILITY_TYPES', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'CODE_CATEGORY', 'EMPLOYEE_GROUP', 'DEFAULT', '_EMPLOYEE_GROUP', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'CODE_CATEGORY', 'EMPLOYEE_ROLE', 'DEFAULT', '_EMPLOYEE_ROLE', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'CODE_CATEGORY', 'EMPLOYEE_STATUS', 'DEFAULT', '_EMPLOYEE_STATUS', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'CODE_CATEGORY', 'EMPLOYEE_TYPE', 'DEFAULT', '_EMPLOYEE_TYPE', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'CODE_CATEGORY', 'GENDER', 'DEFAULT', '_GENDER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'CODE_CATEGORY', 'INV_BUCKET_TRACKING_METHOD', 'DEFAULT', '_INV_BUCKET_TRACKING_METHOD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'CODE_CATEGORY', 'INV_CTL_DOC_COUNT_SUBTYPE', 'DEFAULT', '_INV_CTL_DOC_COUNT_SUBTYPE', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'CODE_CATEGORY', 'INVENTORY_ACTION_CODES', 'DEFAULT', '_INVENTORY_ACTION_CODES', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'CODE_CATEGORY', 'INVENTORY_LOCATOR_DISTANCES', 'DEFAULT', '_INVENTORY_LOCATOR_DISTANCES', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'CODE_CATEGORY', 'ITEM_GROUP', 'DEFAULT', '_ITEM_GROUP', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'CODE_CATEGORY', 'MARITAL_STATUS', 'DEFAULT', '_MARITAL_STATUS', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'CODE_CATEGORY', 'ORGANIZATION_TYPE', 'DEFAULT', '_ORGANIZATION_TYPE', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'CODE_CATEGORY', 'PARTY_TYPE_CODE', 'DEFAULT', '_PARTY_TYPE_CODE', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'CODE_CATEGORY', 'PAY_STATUS', 'DEFAULT', '_PAY_STATUS', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'CODE_CATEGORY', 'SALUTATION', 'DEFAULT', '_SALUTATION', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'CODE_CATEGORY', 'SHIPPING_METHODS', 'DEFAULT', '_SHIPPING_METHODS', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'CODE_CATEGORY', 'EMPLOYEE_TASK_TYPE', 'DEFAULT', '_EMPLOYEE_TASK_TYPE', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'CODE_CATEGORY', 'INSURANCE_PLAN', 'DEFAULT', '_INSURANCE_PLAN', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'CODE_CATEGORY', 'MODULE_NAME', 'DEFAULT', '_MODULE_NAME', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'CODE_CATEGORY', 'PRIVATE_CREDIT_ACCOUNT_TYPE', 'DEFAULT', '_PRIVATE_CREDIT_ACCOUNT_TYPE', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'CODE_CATEGORY', 'PRIVATE_CREDIT_PRIMARY_ID_TYPE', 'DEFAULT', '_PRIVATE_CREDIT_PRIMARY_ID_TYPE', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'CODE_CATEGORY', 'PRIVATE_CREDIT_SECOND_ID_TYPE', 'DEFAULT', '_PRIVATE_CREDIT_SECOND_ID_TYPE', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'CODE_CATEGORY', 'SHOPPER_STATUS', 'DEFAULT', '_SHOPPER_STATUS', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'CODE_CATEGORY', 'TELEPHONE_TYPE', 'DEFAULT', '_TELEPHONE_TYPE', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'CODE_CATEGORY', 'WORK_ORDER_CONTACT_METHODS', 'DEFAULT', '_WORK_ORDER_CONTACT_METHODS', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'CODE_CATEGORY', 'WORK_ORDER_PRIORITIES', 'DEFAULT', '_WORK_ORDER_PRIORITIES', 10, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'DATA' AND config_name = 'Discount---applicationMethod';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Discount---applicationMethod', 'LINE_ITEM', 'DISCOUNT', '_LINE_ITEM_DISCOUNT', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Discount---applicationMethod', 'GROUP', 'DISCOUNT', '_GROUP_DISCOUNT', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Discount---applicationMethod', 'TRANSACTION', 'DISCOUNT', '_TRANSACTION_DISCOUNT', 30, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'DATA' AND config_name = 'Discount---calculationMethod';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Discount---calculationMethod', 'PERCENT', 'DISCOUNT', '_PERCENT_DISCOUNT', 10, 'PERCENT', NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Discount---calculationMethod', 'AMOUNT', 'DISCOUNT', '_AMOUNT_DISCOUNT', 20, 'AMOUNT', NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Discount---calculationMethod', 'OUT_THE_DOOR', 'DISCOUNT', '_OUT_THE_DOOR_DISCOUNT', 30, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Discount---calculationMethod', 'PROMPT_AMOUNT', 'DISCOUNT', '_PROMPT_AMOUNT_DISCOUNT', 40, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Discount---calculationMethod', 'PROMPT_PERCENT', 'DISCOUNT', '_PROMPT_PERCENT_DISCOUNT', 50, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Discount---calculationMethod', 'PROMPT_NEW_PRICE', 'DISCOUNT', '_PROMPT_NEW_PRICE_DISCOUNT', 60, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Discount---calculationMethod', 'PROMPT_QTY_NEW_PRICE', 'DISCOUNT', '_PROMPT_QTY_NEW_PRICE_DISCOUNT', 70, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Discount---calculationMethod', 'PROMPT_COMP_PRICE_PERCENT', 'DISCOUNT', '_PROMPT_COMP_PRICE_PERCENT_DISCOUNT', 80, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Discount---calculationMethod', 'PROMPT_COMP_PRICE_AMOUNT', 'DISCOUNT', '_PROMPT_COMP_PRICE_AMOUNT_DISCOUNT', 90, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'DATA' AND config_name = 'Discount---eligibilityType';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Discount---eligibilityType', 'SALE', 'DISCOUNT', '_sale', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Discount---eligibilityType', 'RETURN', 'DISCOUNT', '_RETURN', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Discount---eligibilityType', 'LAYAWAY', 'DISCOUNT', '_layaway', 30, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Discount---eligibilityType', 'SHIP_SALE', 'DISCOUNT', '_sendSale', 40, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Discount---eligibilityType', 'SPECIAL_ORDER', 'DISCOUNT', '_specialOrder', 50, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Discount---eligibilityType', 'ORDER', 'DISCOUNT', '_order', 60, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'DATA' AND config_name = 'Discount---taxibilityCode';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Discount---taxibilityCode', 'POST_TAX', 'DISCOUNT', '_POST_TAX_DISCOUNT', 10, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'DATA' AND config_name = 'Employee---employeeStatusCode';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Employee---employeeStatusCode', 'A', 'EMPLOYEE', '_A', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Employee---employeeStatusCode', 'T', 'EMPLOYEE', '_T', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'DATA' AND config_name = 'Employee---employeeTypeCode';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Employee---employeeTypeCode', 'CASHIER', 'EMPLOYEE', '_CASHIER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Employee---employeeTypeCode', 'ASST_MGR', 'EMPLOYEE', '_ASST_MGR', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Employee---employeeTypeCode', 'MANAGER', 'EMPLOYEE', '_MANAGER', 30, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'DATA' AND config_name = 'Item---itemLevelCode';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Item---itemLevelCode', 'STYLE', 'ITEM', '_STYLEITEM', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Item---itemLevelCode', 'ITEM', 'ITEM', '_ITEMITEM', 10, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'DATA' AND config_name = 'Item---itemTypeCode';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Item---itemTypeCode', 'STANDARD', 'ITEM', '_STANDARDITEM', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Item---itemTypeCode', 'DUMMY', 'ITEM', '_DUMMYITEM', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Item---itemTypeCode', 'TRADEIN', 'ITEM', '_TRADEINITEM', 30, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Item---itemTypeCode', 'NOT_ON_FILE', 'ITEM', '_NOT_ON_FILEITEM', 40, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Item---itemTypeCode', 'KIT', 'ITEM', '_KITITEM', 50, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'DATA' AND config_name = 'PRICE_TYPES';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'PRICE_TYPES', 'REGULAR_PRICE', 'DEFAULT', '_REGULAR_PRICE', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'PRICE_TYPES', 'PROMO_PRICE', 'DEFAULT', '_PROMO_PRICE', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'DATA' AND config_name = 'REASON_CODE_TYPE';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'REASON_CODE_TYPE', 'CHANGE_FLOAT', 'DEFAULT', '_CHANGE_FLOAT', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'REASON_CODE_TYPE', 'INVENTORY_ADJUSTMENT', 'DEFAULT', '_INVENTORY_ADJUSTMENT', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'REASON_CODE_TYPE', 'ITEM_TRANSFER', 'DEFAULT', '_ITEM_TRANSFER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'REASON_CODE_TYPE', 'NO_SALE', 'DEFAULT', '_NO_SALE', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'REASON_CODE_TYPE', 'PAID_IN', 'DEFAULT', '_PAID_IN', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'REASON_CODE_TYPE', 'PAID_OUT', 'DEFAULT', '_PAID_OUT', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'REASON_CODE_TYPE', 'POST_VOID', 'DEFAULT', '_POST_VOID', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'REASON_CODE_TYPE', 'PRICE_CHANGE', 'DEFAULT', '_PRICE_CHANGE', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'REASON_CODE_TYPE', 'RETURN', 'DEFAULT', '_RETURN', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'REASON_CODE_TYPE', 'TAX_CHANGE', 'DEFAULT', '_TAX_CHANGE', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'REASON_CODE_TYPE', 'TAX_EXEMPT', 'DEFAULT', '_TAX_EXEMPT', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'REASON_CODE_TYPE', 'TIME_OFF_REAS_CODE', 'DEFAULT', '_TIME_OFF_REAS_CODE', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'REASON_CODE_TYPE', 'TRANSACTION_CANCEL', 'DEFAULT', '_TRANSACTION_CANCEL', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'REASON_CODE_TYPE', 'VOID_LINE_ITEM', 'DEFAULT', '_VOID_LINE_ITEM', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'REASON_CODE_TYPE', 'DISCOUNT', 'DEFAULT', '_DISCOUNT', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'REASON_CODE_TYPE', 'TILL_COUNT_DISCREPANCY', 'DEFAULT', '_TILL_COUNT_DISCREPANCY', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order)
  VALUES ('DATA', 'REASON_CODE_TYPE', 'REPLENISHMENT_HEADER', 'DEFAULT', '_reasonCodeType_REPLENISHMENT_HEADER', 10);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order)
  VALUES ('DATA', 'REASON_CODE_TYPE', 'REPLENISHMENT_ITEM', 'DEFAULT', '_reasonCodeType_REPLENISHMENT_ITEM', 10);
GO

DELETE FROM cfg_code_value WHERE category = 'DATA' AND config_name = 'RECEIPT_TEXT';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'STORE_COPY_HEADER', 'RECEIPT', '_STORE_COPY_HEADER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'CUSTOMER_COPY_HEADER', 'RECEIPT', '_CUSTOMER_COPY_HEADER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'CUSTOMER_COPY_FOOTER', 'RECEIPT', '_CUSTOMER_COPY_FOOTER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'STORE_COPY_FOOTER', 'RECEIPT', '_STORE_COPY_FOOTER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'LAYAWAY_CANCEL_HEADER', 'RECEIPT', '_LAYAWAY_CANCEL_HEADER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'LAYAWAY_CANCEL_FOOTER', 'RECEIPT', '_LAYAWAY_CANCEL_FOOTER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'LAYAWAY_HEADER', 'RECEIPT', '_LAYAWAY_HEADER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'LAYAWAY_FOOTER', 'RECEIPT', '_LAYAWAY_FOOTER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'LAYAWAY_TERMS', 'RECEIPT', '_LAYAWAY_TERMS', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'LAYAWAY_MERCH_TICKET_HEADER', 'RECEIPT', '_LAYAWAY_MERCH_TICKET_HEADER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'LAYAWAY_MERCH_TICKET_FOOTER', 'RECEIPT', '_LAYAWAY_MERCH_TICKET_FOOTER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'SPECIAL_ORDER_CANCEL_HEADER', 'RECEIPT', '_SPECIAL_ORDER_CANCEL_HEADER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'SPECIAL_ORDER_CANCEL_FOOTER', 'RECEIPT', '_SPECIAL_ORDER_CANCEL_FOOTER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'SPECIAL_ORDER_HEADER', 'RECEIPT', '_SPECIAL_ORDER_HEADER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'SPECIAL_ORDER_FOOTER', 'RECEIPT', '_SPECIAL_ORDER_FOOTER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'SPECIAL_ORDER_TERMS', 'RECEIPT', '_SPECIAL_ORDER_TERMS', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'WORK_ORDER_CANCEL_HEADER', 'RECEIPT', '_WORK_ORDER_CANCEL_HEADER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'WORK_ORDER_CANCEL_FOOTER', 'RECEIPT', '_WORK_ORDER_CANCEL_FOOTER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'WORK_ORDER_HEADER', 'RECEIPT', '_WORK_ORDER_HEADER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'WORK_ORDER_FOOTER', 'RECEIPT', '_WORK_ORDER_FOOTER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'WORK_ORDER_TERMS', 'RECEIPT', '_WORK_ORDER_TERMS', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'WORK_ORDER_MERCH_TICKET_HEADER', 'RECEIPT', '_WORK_ORDER_MERCH_TICKET_HEADER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'WORK_ORDER_MERCH_TICKET_FOOTER', 'RECEIPT', '_WORK_ORDER_MERCH_TICKET_FOOTER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'SEND_SALE_HEADER', 'RECEIPT', '_SEND_SALE_HEADER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'SEND_SALE_FOOTER', 'RECEIPT', '_SEND_SALE_FOOTER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'SEND_SALE_TERMS', 'RECEIPT', '_SEND_SALE_TERMS', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'SEND_SALE_MERCH_TICKET_HEADER', 'RECEIPT', '_SEND_SALE_MERCH_TICKET_HEADER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'SEND_SALE_MERCH_TICKET_FOOTER', 'RECEIPT', '_SEND_SALE_MERCH_TICKET_FOOTER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'REFUND_CREDIT_TERMS', 'RECEIPT', '_REFUND_CREDIT_TERMS', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'TENDER_CREDIT_TERMS', 'RECEIPT', '_TENDER_CREDIT_TERMS', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'TIMECARD_HEADER', 'RECEIPT', '_TIMECARD_HEADER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'TIMECARD_FOOTER', 'RECEIPT', '_TIMECARD_FOOTER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'REBATE_FOOTER', 'RECEIPT', '_REBATE_FOOTER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'REBATE_HEADER', 'RECEIPT', '_REBATE_HEADER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'BOUNCE_BACK_COUPON_HEADER', 'RECEIPT', '_BOUNCE_BACK_COUPON_HEADER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'BOUNCE_BACK_COUPON_FOOTER', 'RECEIPT', '_BOUNCE_BACK_COUPON_FOOTER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'GIFT_CERTIFICATE_FOOTER', 'RECEIPT', '_GIFT_CERTIFICATE_FOOTER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'GIFT_CERTIFICATE_HEADER', 'RECEIPT', '_GIFT_CERTIFICATE_HEADER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'BALANCE_INQUIRY_HEADER', 'RECEIPT', '_BALANCE_INQUIRY_HEADER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'BALANCE_INQUIRY_FOOTER', 'RECEIPT', '_BALANCE_INQUIRY_FOOTER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'NO_SALE_HEADER', 'RECEIPT', '_NO_SALE_HEADER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'NO_SALE_FOOTER', 'RECEIPT', '_NO_SALE_FOOTER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'TILL_BEGIN_COUNT_HEADER', 'RECEIPT', '_TILL_BEGIN_COUNT_HEADER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'TILL_BEGIN_COUNT_FOOTER', 'RECEIPT', '_TILL_BEGIN_COUNT_FOOTER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'TILL_MIDDAY_COUNT_HEADER', 'RECEIPT', '_TILL_MIDDAY_COUNT_HEADER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'TILL_MIDDAY_COUNT_FOOTER', 'RECEIPT', '_TILL_MIDDAY_COUNT_FOOTER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'TILL_CLOSING_COUNT_HEADER', 'RECEIPT', '_TILL_CLOSING_COUNT_HEADER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'TILL_CLOSING_COUNT_FOOTER', 'RECEIPT', '_TILL_CLOSING_COUNT_FOOTER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'CASH_PICKUP_HEADER', 'RECEIPT', '_CASH_PICKUP_HEADER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'CASH_PICKUP_FOOTER', 'RECEIPT', '_CASH_PICKUP_FOOTER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'PAID_IN_HEADER', 'RECEIPT', '_PAID_IN_HEADER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'PAID_IN_FOOTER', 'RECEIPT', '_PAID_IN_FOOTER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'PAID_OUT_HEADER', 'RECEIPT', '_PAID_OUT_HEADER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'PAID_OUT_FOOTER', 'RECEIPT', '_PAID_OUT_FOOTER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'DEPOSIT_SLIP_HEADER', 'RECEIPT', '_DEPOSIT_SLIP_HEADER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'DEPOSIT_SLIP_FOOTER', 'RECEIPT', '_DEPOSIT_SLIP_FOOTER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'RAIN_CHECK_HEADER', 'RECEIPT', '_RAIN_CHECK_HEADER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'RAIN_CHECK_FOOTER', 'RECEIPT', '_RAIN_CHECK_FOOTER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'INVENTORY_LOCATION_HEADER', 'RECEIPT', '_INVENTORY_LOCATION_HEADER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'INVENTORY_LOCATION_FOOTER', 'RECEIPT', '_INVENTORY_LOCATION_FOOTER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'DISPOSITION_HEADER', 'RECEIPT', '_DISPOSITION_HEADER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'DISPOSITION_FOOTER', 'RECEIPT', '_DISPOSITION_FOOTER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'DISPOSITION_MESSAGE', 'RECEIPT', '_DISPOSITION_MESSAGE', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'SPA_HEADER', 'RECEIPT', '_SPA_HEADER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'SPA_FOOTER', 'RECEIPT', '_SPA_FOOTER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'FLASH_SALE_HEADER', 'RECEIPT', '_FLASH_SALE_HEADER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'FLASH_SALE_FOOTER', 'RECEIPT', '_FLASH_SALE_FOOTER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'CBO_HEADER', 'RECEIPT', '_CBO_HEADER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'CBO_FOOTER', 'RECEIPT', '_CBO_FOOTER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'CBO_TERMS', 'RECEIPT', '_CBO_TERMS', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'RECEIVING_HEADER', 'RECEIPT', '_RECEIVING_HEADER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'RECEIVING_FOOTER', 'RECEIPT', '_RECEIVING_FOOTER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'SHIPPING_HEADER', 'RECEIPT', '_SHIPPING_HEADER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'SHIPPING_FOOTER', 'RECEIPT', '_SHIPPING_FOOTER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'REMOTE_SEND_HEADER', 'RECEIPT', '_REMOTE_SEND_HEADER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'REMOTE_SEND_FOOTER', 'RECEIPT', '_REMOTE_SEND_FOOTER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'REMOTE_SEND_TERMS', 'RECEIPT', '_REMOTE_SEND_TERMS', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'REMOTE_SEND_MERCHTICKET_HEADER', 'RECEIPT', 'Remote Send merchandise ticket header', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'RECEIPT_TEXT', 'REMOTE_SEND_MERCHTICKET_FOOTER', 'RECEIPT', 'Remote Send merchandise ticket footer', 10, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'DATA' AND config_name = 'Security---noAccessSettingsCode';
--INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
--  VALUES ('DATA', 'Security---noAccessSettingsCode', 'OVERRIDABLE', 'SECURITY_PRIVILEGE', '_OVERRIDABLE_SECURITY', 10, NULL, NULL, NULL);
--INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
--  VALUES ('DATA', 'Security---noAccessSettingsCode', 'HIDDEN', 'SECURITY_PRIVILEGE', '_HIDDEN_SECURITY', 20, NULL, NULL, NULL);
--INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
--  VALUES ('DATA', 'Security---noAccessSettingsCode', 'NOT_OVERRIDABLE', 'SECURITY_PRIVILEGE', '_NOT_OVERRIDABLE_SECURITY', 30, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'DATA' AND config_name = 'Security---secondPromptSettingsCode';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Security---secondPromptSettingsCode', 'NO_PROMPT', 'SECURITY_PRIVILEGE', '_NO_PROMPT_SECURITY', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Security---secondPromptSettingsCode', 'ANY_EMP_OK', 'SECURITY_PRIVILEGE', '_ANY_EMP_OK_SECURITY', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Security---secondPromptSettingsCode', 'REQUIRE_GRP_MEMBERSHIP', 'SECURITY_PRIVILEGE', '_REQUIRE_GRP_MEMBERSHIP_SECURITY', 30, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'DATA' AND config_name = 'Tax---roundingRequireCode';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tax---roundingRequireCode', 'UP', 'TAX_AUTHORITY', '_ROUND_UP', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tax---roundingRequireCode', 'DOWN', 'TAX_AUTHORITY', '_ROUND_DOWN', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tax---roundingRequireCode', 'CEILING', 'TAX_AUTHORITY', '_ROUND_CEILING', 30, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tax---roundingRequireCode', 'FLOOR', 'TAX_AUTHORITY', '_ROUND_FLOOR', 40, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tax---roundingRequireCode', 'HALF_UP', 'TAX_AUTHORITY', '_ROUND_HALF_UP', 50, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tax---roundingRequireCode', 'HALF_DOWN', 'TAX_AUTHORITY', '_ROUND_HALF_DOWN', 60, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tax---roundingRequireCode', 'HALF_EVEN', 'TAX_AUTHORITY', '_ROUND_HALF_EVEN', 70, NULL, NULL, NULL);
-- NOTE: do NOT include _ROUND_UNNECESSARY; PTS FB 268883
GO

DELETE FROM cfg_code_value WHERE category = 'DATA' AND config_name = 'Tax---taxApplicationTypes';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tax---taxApplicationTypes', 'Amount', 'TAX_RATE_RULE', '_Amount_TAX_APPLICATIONTYPE', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tax---taxApplicationTypes', 'Percentage', 'TAX_RATE_RULE', '_Percentage_TAX_APPLICATIONTYPE', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'DATA' AND config_name = 'Tax---taxBreakPointTypes';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tax---taxBreakPointTypes', 'FULL', 'TAX_RATE_RULE', '_FULL_TAX_BREAKPOINT', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tax---taxBreakPointTypes', 'PART', 'TAX_RATE_RULE', '_PART_TAX_BREAKPOINT', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'DATA' AND config_name = 'Tender---authMethodCode';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'XPAY_AMEX_FDMS', 'TENDER', '_XPAY_AMEX_FDMS', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'XPAY_CHECK_CERTEGY', 'TENDER', '_XPAY_CHECK_CERTEGY', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'XPAY_VISA_FDMS', 'TENDER', '_XPAY_VISA_FDMS', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'XPAY_DINERS_CLUB_FDMS', 'TENDER', '_XPAY_DINERS_CLUB_FDMS', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'XPAY_DISCOVER_FDMS', 'TENDER', '_XPAY_DISCOVER_FDMS', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'XPAY_GIFT_CARD', 'TENDER', '_XPAY_GIFT_CARD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'XPAY_POINTS_CARD', 'TENDER', '_XPAY_POINTS_CARD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'DACS_GIFT_CARD', 'TENDER', '_DACS_GIFT_CARD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'XPAY_AMEX_AMEX', 'TENDER', '_XPAY_AMEX_AMEX', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'XPAY_MASTERCARD_FDMS', 'TENDER', '_XPAY_MASTERCARD_FDMS', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'XPAY_JCB_FDMS', 'TENDER', '_XPAY_JCB_FDMS', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'XPAY_PRIVATE_LABEL_FDMS', 'TENDER', '_XPAY_PRIVATE_LABEL_FDMS', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'XPAY_DEBIT_FDMS', 'TENDER', '_XPAY_DEBIT_FDMS', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'TENDER_RETAIL_CREDIT', 'TENDER', '_TENDER_RETAIL_CREDIT', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'TENDER_RETAIL_DEBIT', 'TENDER', '_TENDER_RETAIL_DEBIT', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'AJB_VISA', 'TENDER', '_AJB_VISA', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'AJB_MASTERCARD', 'TENDER', '_AJB_MASTERCARD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'AJB_DINERS_CLUB', 'TENDER', '_AJB_DINERS_CLUB', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'AJB_JCB', 'TENDER', '_AJB_JCB', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'AJB_DISCOVER', 'TENDER', '_AJB_DISCOVER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'AJB_AMERICAN_EXPRESS', 'TENDER', '_AJB_AMERICAN_EXPRESS', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'MANUAL', 'TENDER', '_MANUAL', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'AJB_CHECK', 'TENDER', '_AJB_CHECK', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'AJB_DEBIT', 'TENDER', '_AJB_DEBIT', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'AJB_GIFT_CARD', 'TENDER', '_AJB_GIFT_CARD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'AJB_PRIVATE_CREDIT', 'TENDER', '_AJB_PRIVATE_CREDIT', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'AJB_PEOPLE_PRIVATE', 'TENDER', '_AJB_PEOPLE_PRIVATE', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'XPAY_VISA_SDC', 'TENDER', '_XPAY_VISA_SDC', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'XPAY_MASTERCARD_SDC', 'TENDER', '_XPAY_MASTERCARD_SDC', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'XPAY_AMEX_SDC', 'TENDER', '_XPAY_AMEX_SDC', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'XPAY_DISCOVER_SDC', 'TENDER', '_XPAY_DISCOVER_SDC', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'XPAY_JCB_SDC', 'TENDER', '_XPAY_JCB_SDC', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'XPAY_DEBIT_SDC', 'TENDER', '_XPAY_DEBIT_SDC', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'XPAY_CHECK_SDC', 'TENDER', '_XPAY_CHECK_SDC', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'XPAY_AMEX_FDMS_NORTH', 'TENDER', '_XPAY_AMEX_FDMS_NORTH', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'XPAY_DINERS_CLUB_FDMS_NORTH', 'TENDER', '_XPAY_DINERS_CLUB_FDMS_NORTH', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'XPAY_VISA_FDMS_NORTH', 'TENDER', '_XPAY_VISA_FDMS_NORTH', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'XPAY_MASTERCARD_FDMS_NORTH', 'TENDER', '_XPAY_MASTERCARD_FDMS_NORTH', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'XPAY_DISCOVER_FDMS_NORTH', 'TENDER', '_XPAY_DISCOVER_FDMS_NORTH', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'XPAY_AMEX_FDMS_SOUTH', 'TENDER', '_XPAY_AMEX_FDMS_SOUTH', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'XPAY_DINERS_CLUB_FDMS_SOUTH', 'TENDER', '_XPAY_DINERS_CLUB_FDMS_SOUTH', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'XPAY_VISA_FDMS_SOUTH', 'TENDER', '_XPAY_VISA_FDMS_SOUTH', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'XPAY_MASTERCARD_FDMS_SOUTH', 'TENDER', '_XPAY_MASTERCARD_FDMS_SOUTH', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---authMethodCode', 'XPAY_DISCOVER_FDMS_SOUTH', 'TENDER', '_XPAY_DISCOVER_FDMS_SOUTH', 10, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'DATA' AND config_name = 'Tender---currencyId';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'AED', 'TENDER', '_AED', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'AFN', 'TENDER', '_AFN', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'ALL', 'TENDER', '_ALL', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'AMD', 'TENDER', '_AMD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'ANG', 'TENDER', '_ANG', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'AOA', 'TENDER', '_AOA', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'ARS', 'TENDER', '_ARS', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'AUD', 'TENDER', '_AUD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'AWG', 'TENDER', '_AWG', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'AZN', 'TENDER', '_AZN', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'BAM', 'TENDER', '_BAM', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'BBD', 'TENDER', '_BBD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'BDT', 'TENDER', '_BDT', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'BGN', 'TENDER', '_BGN', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'BHD', 'TENDER', '_BHD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'BIF', 'TENDER', '_BIF', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'BMD', 'TENDER', '_BMD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'BND', 'TENDER', '_BND', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'BOB', 'TENDER', '_BOB', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'BOV', 'TENDER', '_BOV', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'BRL', 'TENDER', '_BRL', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'BSD', 'TENDER', '_BSD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'BTN', 'TENDER', '_BTN', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'BWP', 'TENDER', '_BWP', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'BYR', 'TENDER', '_BYR', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'CAD', 'TENDER', '_CAD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'CDF', 'TENDER', '_CDF', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'CHE', 'TENDER', '_CHE', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'CHF', 'TENDER', '_CHF', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'CHW', 'TENDER', '_CHW', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'CLP', 'TENDER', '_CLP', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'CNY', 'TENDER', '_CNY', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'COP', 'TENDER', '_COP', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'CRC', 'TENDER', '_CRC', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'CUP', 'TENDER', '_CUP', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'CVE', 'TENDER', '_CVE', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'CZK', 'TENDER', '_CZK', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'DJF', 'TENDER', '_DJF', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'DKK', 'TENDER', '_DKK', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'DOP', 'TENDER', '_DOP', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'DZD', 'TENDER', '_DZD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'EEK', 'TENDER', '_EEK', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'EGP', 'TENDER', '_EGP', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'ERN', 'TENDER', '_ERN', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'ETB', 'TENDER', '_ETB', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'EUR', 'TENDER', '_EUR', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'FJD', 'TENDER', '_FJD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'FKP', 'TENDER', '_FKP', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'GBP', 'TENDER', '_GBP', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'GEL', 'TENDER', '_GEL', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'GHS', 'TENDER', '_GHS', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'GIP', 'TENDER', '_GIP', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'GMD', 'TENDER', '_GMD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'GNF', 'TENDER', '_GNF', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'GTQ', 'TENDER', '_GTQ', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'GYD', 'TENDER', '_GYD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'HKD', 'TENDER', '_HKD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'HNL', 'TENDER', '_HNL', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'HRK', 'TENDER', '_HRK', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'HTG', 'TENDER', '_HTG', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'HUF', 'TENDER', '_HUF', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'IDR', 'TENDER', '_IDR', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'ILS', 'TENDER', '_ILS', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'INR', 'TENDER', '_INR', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'IQD', 'TENDER', '_IQD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'IRR', 'TENDER', '_IRR', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'ISK', 'TENDER', '_ISK', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'JMD', 'TENDER', '_JMD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'JOD', 'TENDER', '_JOD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'JPY', 'TENDER', '_JPY', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'KES', 'TENDER', '_KES', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'KGS', 'TENDER', '_KGS', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'KHR', 'TENDER', '_KHR', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'KMF', 'TENDER', '_KMF', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'KPW', 'TENDER', '_KPW', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'KRW', 'TENDER', '_KRW', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'KWD', 'TENDER', '_KWD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'KYD', 'TENDER', '_KYD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'KZT', 'TENDER', '_KZT', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'LAK', 'TENDER', '_LAK', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'LBP', 'TENDER', '_LBP', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'LKR', 'TENDER', '_LKR', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'LRD', 'TENDER', '_LRD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'LSL', 'TENDER', '_LSL', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'LTL', 'TENDER', '_LTL', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'LVL', 'TENDER', '_LVL', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'LYD', 'TENDER', '_LYD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'MAD', 'TENDER', '_MAD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'MDL', 'TENDER', '_MDL', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'MGA', 'TENDER', '_MGA', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'MKD', 'TENDER', '_MKD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'MMK', 'TENDER', '_MMK', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'MNT', 'TENDER', '_MNT', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'MOP', 'TENDER', '_MOP', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'MRO', 'TENDER', '_MRO', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'MUR', 'TENDER', '_MUR', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'MVR', 'TENDER', '_MVR', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'MWK', 'TENDER', '_MWK', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'MXN', 'TENDER', '_MXN', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'MYR', 'TENDER', '_MYR', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'MZN', 'TENDER', '_MZN', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'NAD', 'TENDER', '_NAD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'NGN', 'TENDER', '_NGN', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'NIO', 'TENDER', '_NIO', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'NOK', 'TENDER', '_NOK', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'NPR', 'TENDER', '_NPR', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'NZD', 'TENDER', '_NZD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'OMR', 'TENDER', '_OMR', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'PAB', 'TENDER', '_PAB', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'PEN', 'TENDER', '_PEN', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'PGK', 'TENDER', '_PGK', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'PHP', 'TENDER', '_PHP', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'PKR', 'TENDER', '_PKR', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'PLN', 'TENDER', '_PLN', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'PYG', 'TENDER', '_PYG', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'QAR', 'TENDER', '_QAR', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'RON', 'TENDER', '_RON', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'RSD', 'TENDER', '_RSD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'RUB', 'TENDER', '_RUB', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'RWF', 'TENDER', '_RWF', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'SAR', 'TENDER', '_SAR', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'SBD', 'TENDER', '_SBD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'SCR', 'TENDER', '_SCR', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'SDG', 'TENDER', '_SDG', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'SEK', 'TENDER', '_SEK', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'SGD', 'TENDER', '_SGD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'SHP', 'TENDER', '_SHP', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'SKK', 'TENDER', '_SKK', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'SLL', 'TENDER', '_SLL', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'SOS', 'TENDER', '_SOS', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'SRD', 'TENDER', '_SRD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'STD', 'TENDER', '_STD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'SYP', 'TENDER', '_SYP', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'SZL', 'TENDER', '_SZL', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'THB', 'TENDER', '_THB', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'TJS', 'TENDER', '_TJS', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'TMM', 'TENDER', '_TMM', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'TND', 'TENDER', '_TND', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'TOP', 'TENDER', '_TOP', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'TRY', 'TENDER', '_TRY', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'TTD', 'TENDER', '_TTD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'TWD', 'TENDER', '_TWD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'TZS', 'TENDER', '_TZS', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'UAH', 'TENDER', '_UAH', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'UGX', 'TENDER', '_UGX', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'USD', 'TENDER', '_USD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'UYU', 'TENDER', '_UYU', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'UZS', 'TENDER', '_UZS', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'VEF', 'TENDER', '_VEF', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'VND', 'TENDER', '_VND', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'VUV', 'TENDER', '_VUV', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'WST', 'TENDER', '_WST', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'XAF', 'TENDER', '_XAF', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'XCD', 'TENDER', '_XCD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'YER', 'TENDER', '_YER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'ZAR', 'TENDER', '_ZAR', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'ZMK', 'TENDER', '_ZMK', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---currencyId', 'ZWD', 'TENDER', '_ZWD', 10, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'DATA' AND config_name = 'Tender---custIdReqCode';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---custIdReqCode', 'DRIVER_LICENSE', 'TENDER', '_DRIVER_LICENSE', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---custIdReqCode', 'SOCIAL_SECURITY', 'TENDER', '_SOCIAL_SECURITY', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---custIdReqCode', 'MILITARY_ID', 'TENDER', '_MILITARY_ID', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---custIdReqCode', 'CREDIT_CARD', 'SALTENDERE', '_CREDIT_CARD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---custIdReqCode', 'EMPLOYEE_ID', 'SALTENDERE', '_EMPLOYEE_ID', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---custIdReqCode', 'CUSTOMER_CARD', 'SALTENDERE', '_CUSTOMER_CARD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---custIdReqCode', 'ANY', 'SALTENDERE', '_ANY_ID', 10, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'DATA' AND config_name = 'Tender---reportingGroup';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---reportingGroup', 'TENDER SUMMARY', 'TENDER', '_TENDER_SUMMARYCOUNT', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---reportingGroup', 'FOREIGN CURRENCY', 'TENDER', '_FOREIGN_CURRENCYCOUNT', 10, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'DATA' AND config_name = 'Tender---unitCountCode';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---unitCountCode', 'TOTAL_NORMAL', 'TENDER', '_TOTAL_NORMAL_COUNT', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---unitCountCode', 'TOTAL_SHORT', 'TENDER', '_TOTAL_SHORT_COUNT', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---unitCountCode', 'DENOMINATION', 'TENDER', '_DENOMINATION_COUNT', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Tender---unitCountCode', 'UNIT_NORMAL', 'TENDER', '_UNIT_NORMAL_COUNT', 10, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'DATA' AND config_name = 'TenderAvailability---availabilityCode';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'TenderAvailability---availabilityCode', 'SALE', 'TENDER', '_SALE', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'TenderAvailability---availabilityCode', 'RETURN_WITH_RECEIPT', 'TENDER', '_RETURN_WITH_RECEIPT', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'TenderAvailability---availabilityCode', 'RETURN_WITHOUT_RECEIPT', 'TENDER', '_RETURN_WITHOUT_RECEIPT', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'TenderAvailability---availabilityCode', 'CHANGE', 'TENDER', '_CHANGE', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'TenderAvailability---availabilityCode', 'LAYAWAY', 'TENDER', '_LAYAWAY', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'TenderAvailability---availabilityCode', 'SPECIAL_ORDER', 'TENDER', '_SPECIAL_ORDER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'TenderAvailability---availabilityCode', 'TENDER_EXCHANGE_IN', 'TENDER', '_TENDER_EXCHANGE_IN', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'TenderAvailability---availabilityCode', 'TENDER_EXCHANGE_OUT', 'TENDER', '_TENDER_EXCHANGE_OUT', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'TenderAvailability---availabilityCode', 'TILL_COUNT', 'TENDER', '_TILL_COUNT', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'TenderAvailability---availabilityCode', 'DEPOSIT', 'TENDER', '_DEPOSIT', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'TenderAvailability---availabilityCode', 'RETURN_WITH_GIFT_RECEIPT', 'TENDER', '_RETURN_WITH_GIFT_RECEIPT', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'TenderAvailability---availabilityCode', 'REMOTE_SEND', 'TENDER', '_REMOTE_SEND', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'TenderAvailability---availabilityCode', 'REMOTE_SEND_REFUND', 'TENDER', '_REMOTE_SEND_REFUND', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'TenderAvailability---availabilityCode', 'ORDER', 'TENDER', '_DIST_ORDER', 10, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'DATA' AND config_name = 'TenderUserSettings---entryMethodCode';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'TenderUserSettings---entryMethodCode', 'DEFAULT', 'TENDER', '_DEFAULT', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'TenderUserSettings---entryMethodCode', 'RFID', 'TENDER', '_RFID', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'TenderUserSettings---entryMethodCode', 'CUST_MSR', 'TENDER', '_CUST_MSR', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'TenderUserSettings---entryMethodCode', 'MAIN_MSR', 'TENDER', '_MAIN_MSR', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'TenderUserSettings---entryMethodCode', 'KEYBOARD', 'TENDER', '_KEYBOARD', 10, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'DATA' AND config_name = 'TenderUserSettings---usageCode';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'TenderUserSettings---usageCode', 'SALE', 'TENDER', '_SALE', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'TenderUserSettings---usageCode', 'RETURN_WITHRECEIPT', 'TENDER', '_RETURN_WITHRECEIPT', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'TenderUserSettings---usageCode', 'CHANGE', 'TENDER', '_CHANGE', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'TenderUserSettings---usageCode', 'RETURN_WITHOUTRECEIPT', 'TENDER', '_RETURN_WITHOUTRECEIPT', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'TenderUserSettings---usageCode', 'EXCHANGE', 'TENDER', '_EXCHANGE', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'TenderUserSettings---usageCode', 'VOID', 'TENDER', '_VOID', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'TenderUserSettings---usageCode', 'SIGNATURE_REQ_FLOOR_LIMIT', 'TENDER', '_SIGNATURE_REQ_FLOOR_LIMIT', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'TenderUserSettings---usageCode', 'SIGNATURE_REQ_FLOOR_LIMIT_NEG', 'TENDER', '_SIGNATURE_REQ_FLOOR_LIMIT_NEG', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'TenderUserSettings---usageCode', 'REMOTE_SEND_REFUND', 'TENDER', '_REMOTE_SEND_REFUND', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'TenderUserSettings---usageCode', 'DEFAULT', 'TENDER', 'Default', 10, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'DATA' AND config_name = 'Vendor---vendorStatusCode';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Vendor---vendorStatusCode', 'AVAILABLE', 'VENDOR', '_AVAILABLE', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Vendor---vendorStatusCode', 'SUSPENDED', 'VENDOR', '_SUSPENDED', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'DATA' AND config_name = 'Vendor---vendorTypeCode';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Vendor---vendorTypeCode', 'DIST', 'VENDOR', '_DIST', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('DATA', 'Vendor---vendorTypeCode', 'MANU', 'VENDOR', '_MANU', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'MenuConfig' AND config_name = 'KEY_STROKES';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('MenuConfig', 'KEY_STROKES', 'F2', 'DEFAULT', 'F2', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('MenuConfig', 'KEY_STROKES', 'F3', 'DEFAULT', 'F3', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('MenuConfig', 'KEY_STROKES', 'F4', 'DEFAULT', 'F4', 30, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('MenuConfig', 'KEY_STROKES', 'F5', 'DEFAULT', 'F5', 40, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('MenuConfig', 'KEY_STROKES', 'F6', 'DEFAULT', 'F6', 50, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('MenuConfig', 'KEY_STROKES', 'F7', 'DEFAULT', 'F7', 60, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('MenuConfig', 'KEY_STROKES', 'F8', 'DEFAULT', 'F8', 70, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('MenuConfig', 'KEY_STROKES', 'F9', 'DEFAULT', 'F9', 80, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('MenuConfig', 'KEY_STROKES', 'F10', 'DEFAULT', 'F10', 90, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('MenuConfig', 'KEY_STROKES', 'F11', 'DEFAULT', 'F11', 100, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'ORG_HIERARCHY_LEVEL' AND config_name = 'DEFAULT';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('ORG_HIERARCHY_LEVEL', 'DEFAULT', '*', 'DEFAULT', 'Global', 0, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('ORG_HIERARCHY_LEVEL', 'DEFAULT', 'CORP', 'DEFAULT', 'Corporate Division', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('ORG_HIERARCHY_LEVEL', 'DEFAULT', 'FRANCHISE', 'DEFAULT', 'Franchise Division', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('ORG_HIERARCHY_LEVEL', 'DEFAULT', 'OWNER', 'DEFAULT', 'Franchise Owner', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('ORG_HIERARCHY_LEVEL', 'DEFAULT', 'REGION', 'DEFAULT', 'Corporate Region', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('ORG_HIERARCHY_LEVEL', 'DEFAULT', 'AREA', 'DEFAULT', 'Franchise Area', 30, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('ORG_HIERARCHY_LEVEL', 'DEFAULT', 'DISTRICT', 'DEFAULT', 'District', 40, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('ORG_HIERARCHY_LEVEL', 'DEFAULT', 'STORE', 'DEFAULT', 'Store', 1000, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'VISIBILITY_RULE' AND config_name = 'dtv.pos.shared.visibilityrules.AddTenderExchangeRule';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AddTenderExchangeRule', 'OUTGOING', 'AvailableForExchangeType', 'Outgoing tender', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AddTenderExchangeRule', 'INCOMING', 'AvailableForExchangeType', 'Incoming tender', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'VISIBILITY_RULE' AND config_name = 'dtv.pos.shared.visibilityrules.AllowMixedInTransWithSaleItemTypeRule';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AllowMixedInTransWithSaleItemTypeRule', 'SALE', 'type', 'Sale', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AllowMixedInTransWithSaleItemTypeRule', 'RETURN', 'type', 'Return', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AllowMixedInTransWithSaleItemTypeRule', 'LAYAWAY', 'type', 'Layway', 30, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AllowMixedInTransWithSaleItemTypeRule', 'SPECIAL_ORDER', 'type', 'Special Order', 40, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AllowMixedInTransWithSaleItemTypeRule', 'CUSTOMER_BACKORDER', 'type', 'Customer Backorder', 50, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AllowMixedInTransWithSaleItemTypeRule', 'WORK_ORDER', 'type', 'Work Order', 60, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AllowMixedInTransWithSaleItemTypeRule', 'SHIP_SALE', 'type', 'Ship Sale', 70, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AllowMixedInTransWithSaleItemTypeRule', 'REMOTE_SEND', 'type', 'Remote Send', 80, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'VISIBILITY_RULE' AND config_name = 'dtv.pos.shared.visibilityrules.AnyARAcctForCurrentCustCheck';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnyARAcctForCurrentCustCheck', 'true', 'NotOnHoldOnly', 'Not on hold account only', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnyARAcctForCurrentCustCheck', 'false', 'NotOnHoldOnly', 'All', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'VISIBILITY_RULE' AND config_name = 'dtv.pos.shared.visibilityrules.AnyItemWithoutSaleItemTypeVisibilityRule';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnyItemWithoutSaleItemTypeVisibilityRule', 'SALE', 'type', 'Sale', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnyItemWithoutSaleItemTypeVisibilityRule', 'RETURN', 'type', 'Return', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnyItemWithoutSaleItemTypeVisibilityRule', 'LAYAWAY', 'type', 'Layway', 30, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnyItemWithoutSaleItemTypeVisibilityRule', 'SPECIAL_ORDER', 'type', 'Special Order', 40, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnyItemWithoutSaleItemTypeVisibilityRule', 'CUSTOMER_BACKORDER', 'type', 'Customer Backorder', 50, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnyItemWithoutSaleItemTypeVisibilityRule', 'WORK_ORDER', 'type', 'Work Order', 60, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnyItemWithoutSaleItemTypeVisibilityRule', 'SHIP_SALE', 'type', 'Ship Sale', 70, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnyItemWithoutSaleItemTypeVisibilityRule', 'REMOTE_SEND', 'type', 'Remote Send', 80, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'VISIBILITY_RULE' AND config_name = 'dtv.pos.shared.visibilityrules.AnyItemWithSaleItemTypeVisibilityRule';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnyItemWithSaleItemTypeVisibilityRule', 'SALE', 'type', 'Sale', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnyItemWithSaleItemTypeVisibilityRule', 'RETURN', 'type', 'Return', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnyItemWithSaleItemTypeVisibilityRule', 'LAYAWAY', 'type', 'Layway', 30, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnyItemWithSaleItemTypeVisibilityRule', 'SPECIAL_ORDER', 'type', 'Special Order', 40, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnyItemWithSaleItemTypeVisibilityRule', 'CUSTOMER_BACKORDER', 'type', 'Customer Backorder', 50, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnyItemWithSaleItemTypeVisibilityRule', 'WORK_ORDER', 'type', 'Work Order', 60, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnyItemWithSaleItemTypeVisibilityRule', 'SHIP_SALE', 'type', 'Ship Sale', 70, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnyItemWithSaleItemTypeVisibilityRule', 'REMOTE_SEND', 'type', 'Remote Send', 80, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'VISIBILITY_RULE' AND config_name = 'dtv.pos.shared.visibilityrules.AnyTenderAccCheck';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnyTenderAccCheck', 'true', 'return', 'Enable if condition is true.', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.AnyTenderAccCheck', 'false', 'return', 'Disable if condition is true.', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'VISIBILITY_RULE' AND config_name = 'dtv.pos.shared.visibilityrules.CurrentEjournalTranIsStatusCode';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.CurrentEjournalTranIsStatusCode', 'COMPLETE', 'statusCode', 'Complete', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.CurrentEjournalTranIsStatusCode', 'CANCEL', 'statusCode', 'Cancel', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.CurrentEjournalTranIsStatusCode', 'CANCEL_ORPHANED', 'statusCode', 'Cancel Orphaned', 30, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.CurrentEjournalTranIsStatusCode', 'NEW', 'statusCode', 'New', 40, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.CurrentEjournalTranIsStatusCode', 'RESUME', 'statusCode', 'Resume', 50, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.CurrentEjournalTranIsStatusCode', 'SUSPEND', 'statusCode', 'Suspend', 60, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'VISIBILITY_RULE' AND config_name = 'dtv.pos.shared.visibilityrules.EffectiveTenderExchangeAccCheck';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.EffectiveTenderExchangeAccCheck', 'INCOMING', 'ExchangeType', 'Incoming Tender', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.EffectiveTenderExchangeAccCheck', 'OUTGOING', 'ExchangeType', 'Outgoing Tender', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'VISIBILITY_RULE' AND config_name = 'dtv.pos.shared.visibilityrules.NoItemWithoutSaleItemTypeVisibilityRule';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.NoItemWithoutSaleItemTypeVisibilityRule', 'SALE', 'type', 'Sale', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.NoItemWithoutSaleItemTypeVisibilityRule', 'RETURN', 'type', 'Return', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.NoItemWithoutSaleItemTypeVisibilityRule', 'LAYAWAY', 'type', 'Layway', 30, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.NoItemWithoutSaleItemTypeVisibilityRule', 'SPECIAL_ORDER', 'type', 'Special Order', 40, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.NoItemWithoutSaleItemTypeVisibilityRule', 'CUSTOMER_BACKORDER', 'type', 'Customer Backorder', 50, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.NoItemWithoutSaleItemTypeVisibilityRule', 'WORK_ORDER', 'type', 'Work Order', 60, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.NoItemWithoutSaleItemTypeVisibilityRule', 'SHIP_SALE', 'type', 'Ship Sale', 70, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.NoItemWithoutSaleItemTypeVisibilityRule', 'REMOTE_SEND', 'type', 'Remote Send', 80, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'VISIBILITY_RULE' AND config_name = 'dtv.pos.shared.visibilityrules.NoItemWithSaleItemTypeVisibilityRule';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.NoItemWithSaleItemTypeVisibilityRule', 'SALE', 'type', 'Sale', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.NoItemWithSaleItemTypeVisibilityRule', 'RETURN', 'type', 'Return', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.NoItemWithSaleItemTypeVisibilityRule', 'LAYAWAY', 'type', 'Layway', 30, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.NoItemWithSaleItemTypeVisibilityRule', 'SPECIAL_ORDER', 'type', 'Special Order', 40, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.NoItemWithSaleItemTypeVisibilityRule', 'CUSTOMER_BACKORDER', 'type', 'Customer Backorder', 50, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.NoItemWithSaleItemTypeVisibilityRule', 'WORK_ORDER', 'type', 'Work Order', 60, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.NoItemWithSaleItemTypeVisibilityRule', 'SHIP_SALE', 'type', 'Ship Sale', 70, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.NoItemWithSaleItemTypeVisibilityRule', 'REMOTE_SEND', 'type', 'Remote Send', 80, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'VISIBILITY_RULE' AND config_name = 'dtv.pos.shared.visibilityrules.SpecialOrderModeAccessCheck';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.SpecialOrderModeAccessCheck', 'true', 'MINIMAL', 'Minimal Special Order', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.SpecialOrderModeAccessCheck', 'false', 'MINIMAL', 'Full Special Order', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'VISIBILITY_RULE' AND config_name = 'dtv.pos.shared.visibilityrules.TillRegisterModeAccessCheck';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.TillRegisterModeAccessCheck', 'TILL', 'mode', 'Till Accountability', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shared.visibilityrules.TillRegisterModeAccessCheck', 'REGISTER', 'mode', 'Register Accountability', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'VISIBILITY_RULE' AND config_name = 'dtv.pos.assistance.access.AssistanceModeStatusAccessCheck';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.assistance.access.AssistanceModeStatusAccessCheck', 'ENABLED', 'state', 'Enabled', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.assistance.access.AssistanceModeStatusAccessCheck', 'DISABLED', 'state', 'Disabled', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'VISIBILITY_RULE' AND config_name = 'dtv.pos.shipsale.common.AnyOpenSendSalesRule';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shipsale.common.AnyOpenSendSalesRule', 'true', 'INVERTED', 'Enable if false', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.shipsale.common.AnyOpenSendSalesRule', 'false', 'INVERTED', 'Enable if true', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'VISIBILITY_RULE' AND config_name = 'dtv.pos.register.layaway.validation.LayawayStatusVisibilityRule';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.layaway.validation.LayawayStatusVisibilityRule', 'NEW', 'STATUS', 'New', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.layaway.validation.LayawayStatusVisibilityRule', 'OPEN', 'STATUS', 'Open', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.layaway.validation.LayawayStatusVisibilityRule', 'CLOSED', 'STATUS', 'Closed', 30, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.layaway.validation.LayawayStatusVisibilityRule', 'VOID', 'STATUS', 'Void', 40, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.layaway.validation.LayawayStatusVisibilityRule', 'INACTIVE', 'STATUS', 'Inactive', 50, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.layaway.validation.LayawayStatusVisibilityRule', 'OVERDUE', 'STATUS', 'Overdue', 60, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.layaway.validation.LayawayStatusVisibilityRule', 'DELINQUENT', 'STATUS', 'Delinquent', 70, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.layaway.validation.LayawayStatusVisibilityRule', 'ABANDONED', 'STATUS', 'Abandoned', 80, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.layaway.validation.LayawayStatusVisibilityRule', 'REFUNDABLE', 'STATUS', 'Refundable', 90, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.layaway.validation.LayawayStatusVisibilityRule', 'IN_PROGRESS', 'STATUS', 'In progress', 100, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.layaway.validation.LayawayStatusVisibilityRule', 'PENDING', 'STATUS', 'Pending', 110, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.layaway.validation.LayawayStatusVisibilityRule', 'READY_TO_PICKUP', 'STATUS', 'Ready to pickup', 120, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.layaway.validation.LayawayStatusVisibilityRule', 'CLOSED_ESCROW', 'STATUS', 'Closed escrow', 130, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'VISIBILITY_RULE' AND config_name = 'dtv.pos.register.specorder.AnyNewlyPickedItemsVisibilityRule';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.specorder.AnyNewlyPickedItemsVisibilityRule', 'LAYAWAY', 'accountType', 'Layaway', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.specorder.AnyNewlyPickedItemsVisibilityRule', 'SPECIAL_ORDER', 'accountType', 'Special Order', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.specorder.AnyNewlyPickedItemsVisibilityRule', 'CUSTOMER_BACKORDER', 'accountType', 'Customer Back order', 30, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.specorder.AnyNewlyPickedItemsVisibilityRule', 'WORK_ORDER', 'accountType', 'Work Order', 40, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.specorder.AnyNewlyPickedItemsVisibilityRule', 'SHIP_SALE', 'accountType', 'Ship Sale', 50, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.specorder.AnyNewlyPickedItemsVisibilityRule', 'REMOTE_SEND', 'accountType', 'Remote Send', 60, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.specorder.AnyNewlyPickedItemsVisibilityRule', 'LOYALTY', 'accountType', 'Loyalty', 70, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.specorder.AnyNewlyPickedItemsVisibilityRule', 'ACCOUNTS_RECEIVABLE', 'accountType', 'Accounts Receivable', 80, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.specorder.AnyNewlyPickedItemsVisibilityRule', 'CREDIT_PAYMENT', 'accountType', 'Credit Payment', 90, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'VISIBILITY_RULE' AND config_name = 'dtv.pos.register.specorder.validation.SpecOrderStatusVisibilityRule';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.specorder.validation.SpecOrderStatusVisibilityRule', 'NEW', 'STATUS', 'New', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.specorder.validation.SpecOrderStatusVisibilityRule', 'OPEN', 'STATUS', 'Open', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.specorder.validation.SpecOrderStatusVisibilityRule', 'CLOSED', 'STATUS', 'Closed', 30, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.specorder.validation.SpecOrderStatusVisibilityRule', 'VOID', 'STATUS', 'Void', 40, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.specorder.validation.SpecOrderStatusVisibilityRule', 'INACTIVE', 'STATUS', 'Inactive', 50, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.specorder.validation.SpecOrderStatusVisibilityRule', 'OVERDUE', 'STATUS', 'Overdue', 60, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.specorder.validation.SpecOrderStatusVisibilityRule', 'DELINQUENT', 'STATUS', 'Delinquent', 70, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.specorder.validation.SpecOrderStatusVisibilityRule', 'ABANDONED', 'STATUS', 'Abandoned', 80, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.specorder.validation.SpecOrderStatusVisibilityRule', 'REFUNDABLE', 'STATUS', 'Refundable', 90, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.specorder.validation.SpecOrderStatusVisibilityRule', 'IN_PROGRESS', 'STATUS', 'In progress', 100, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.specorder.validation.SpecOrderStatusVisibilityRule', 'PENDING', 'STATUS', 'Pending', 110, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.specorder.validation.SpecOrderStatusVisibilityRule', 'READY_TO_PICKUP', 'STATUS', 'Ready to pickup', 120, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.register.specorder.validation.SpecOrderStatusVisibilityRule', 'CLOSED_ESCROW', 'STATUS', 'Closed escrow', 130, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'VISIBILITY_RULE' AND config_name = 'dtv.pos.ejournal.giftrcpt.EjournalTranAccessCheck';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.ejournal.giftrcpt.EjournalTranAccessCheck', 'TIMECLOCK', 'tranType', 'Timeclock', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.ejournal.giftrcpt.EjournalTranAccessCheck', 'NO_SALE', 'tranType', 'No Sale', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.ejournal.giftrcpt.EjournalTranAccessCheck', 'RETAIL_SALE', 'tranType', 'Retail Sale', 30, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.ejournal.giftrcpt.EjournalTranAccessCheck', 'SESSION_CONTROL', 'tranType', 'Session Control', 40, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.ejournal.giftrcpt.EjournalTranAccessCheck', 'TENDER_CONTROL', 'tranType', 'Tender Control', 50, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.ejournal.giftrcpt.EjournalTranAccessCheck', 'TENDER_EXCHANGE', 'tranType', 'Tender Exchange', 60, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.ejournal.giftrcpt.EjournalTranAccessCheck', 'SYSTEM_OPEN', 'tranType', 'System Open', 70, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.ejournal.giftrcpt.EjournalTranAccessCheck', 'SYSTEM_CLOSE', 'tranType', 'System Close', 80, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.ejournal.giftrcpt.EjournalTranAccessCheck', 'WORKSTATION_OPEN', 'tranType', 'Workstation Open', 90, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.ejournal.giftrcpt.EjournalTranAccessCheck', 'WORKSTATION_CLOSE', 'tranType', 'Workstation Close', 100, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.ejournal.giftrcpt.EjournalTranAccessCheck', 'WORKSTATION_START_REMOTE_CLOSE', 'tranType', 'Workstation Start Remote Close', 110, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.ejournal.giftrcpt.EjournalTranAccessCheck', 'WORKSTATION_COMPLETE_REMOTE_CLOSE', 'tranType', 'Workstation Complete Remote Close', 120, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.ejournal.giftrcpt.EjournalTranAccessCheck', 'INVENTORY_CONTROL', 'tranType', 'Inventory Control', 130, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.ejournal.giftrcpt.EjournalTranAccessCheck', 'POST_VOID', 'tranType', 'Post Void', 140, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.ejournal.giftrcpt.EjournalTranAccessCheck', 'TRAINING_MODE_ENTRY', 'tranType', 'Training Mode Entry', 150, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.ejournal.giftrcpt.EjournalTranAccessCheck', 'TRAINING_MODE_EXIT', 'tranType', 'Training Mode Exit', 160, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.ejournal.giftrcpt.EjournalTranAccessCheck', 'GENERIC', 'tranType', 'Generic', 170, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.ejournal.giftrcpt.EjournalTranAccessCheck', 'EXCHANGE_RATE', 'tranType', 'Exchange Rate', 180, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.ejournal.giftrcpt.EjournalTranAccessCheck', 'BANKDEPOSIT', 'tranType', 'Bank Deposit', 190, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.ejournal.giftrcpt.EjournalTranAccessCheck', 'BALANCE_INQUIRY', 'tranType', 'Balance Inquiry', 200, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.ejournal.giftrcpt.EjournalTranAccessCheck', 'LOYALTY_BALANCE_INQUIRY', 'tranType', 'Loyalty Balance Inquiry', 210, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.ejournal.giftrcpt.EjournalTranAccessCheck', 'WARRANTY_SERVICE', 'tranType', 'Warranty service', 220, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.ejournal.giftrcpt.EjournalTranAccessCheck', 'PRICE_CHANGE', 'tranType', 'Price Change', 230, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.ejournal.giftrcpt.EjournalTranAccessCheck', 'CREDIT_APPLICATION', 'tranType', 'Credit Application', 240, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.ejournal.giftrcpt.EjournalTranAccessCheck', 'ACCOUNT_LOOKUP', 'tranType', 'Account Lookup', 250, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.ejournal.giftrcpt.EjournalTranAccessCheck', 'TILL_CONTROL', 'tranType', 'Till Control', 260, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.ejournal.giftrcpt.EjournalTranAccessCheck', 'INVENTORY_SUMMARY_COUNT', 'tranType', 'Inventory Summary Count', 270, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.ejournal.giftrcpt.EjournalTranAccessCheck', 'MOVEMENT_PENDING', 'tranType', 'Movement Pending', 280, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.ejournal.giftrcpt.EjournalTranAccessCheck', 'ESCROW', 'tranType', 'Escrow', 290, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.ejournal.giftrcpt.EjournalTranAccessCheck', 'SPA', 'tranType', 'Spa', 300, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'VISIBILITY_RULE' AND config_name = 'dtv.pos.systemcycle.access.RetailLocationStateAccessCheck';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.systemcycle.access.RetailLocationStateAccessCheck', 'OPEN', 'state', 'Open', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.systemcycle.access.RetailLocationStateAccessCheck', 'CLOSED', 'state', 'Closed', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'VISIBILITY_RULE' AND config_name = 'dtv.pos.systemcycle.access.WorkstationStateAccessCheck';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.systemcycle.access.WorkstationStateAccessCheck', 'OPEN', 'state', 'Open', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.systemcycle.access.WorkstationStateAccessCheck', 'CLOSED', 'state', 'Closed', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'VISIBILITY_RULE' AND config_name = 'dtv.pos.tender.exchange.AnyTendersVisibilityRule';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.tender.exchange.AnyTendersVisibilityRule', 'TENDER', 'tenderStatus', 'Tender', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.tender.exchange.AnyTendersVisibilityRule', 'CHANGE', 'tenderStatus', 'Change', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('VISIBILITY_RULE', 'dtv.pos.tender.exchange.AnyTendersVisibilityRule', 'REFUND', 'tenderStatus', 'Refund', 30, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'RcptConfig' AND sub_category = 'DEFAULT';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'AREA', 'headerTop', 'DEFAULT', 'Header', 10, 'true', NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'AREA', 'items', 'DEFAULT', 'Item', 20, 'false', NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'AREA', 'nonchange_tender', 'DEFAULT', 'Tender', 30, 'false', NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'AREA', 'footer', 'DEFAULT', 'Footer', 40, 'true', NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'RcptConfig' AND sub_category = 'headerTop';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'SUB_AREA', 'logo', 'headerTop', 'Logo', 10, 'false', NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'SUB_AREA', 'transaction_store_location', 'headerTop', 'Store Location', 20, 'true', NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'SUB_AREA', 'trans_barcode', 'headerTop', 'Transaction Barcode', 30, 'false', NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'SUB_AREA', 'transaction_header_info', 'headerTop', 'Transaction Header', 40, 'true', NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'SUB_AREA', 'SALES_PERSON_INFO', 'headerTop', 'Sales Associates', 50, 'false', NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'SUB_AREA', 'transaction_cashier', 'headerTop', 'Cashier', 60, 'false', NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'RcptConfig' AND sub_category = 'items';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'SUB_AREA', 'item_header', 'items', 'Item Header', 10, 'true', NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'SUB_AREA', 'nonvoided_item', 'items', 'Item Detail', 20, 'true', NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'RcptConfig' AND sub_category = 'nonchange_tender';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'SUB_AREA', 'check_tender', 'nonchange_tender', 'Check', 10, 'true', NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'SUB_AREA', 'voucher_tender', 'nonchange_tender', 'Voucher', 20, 'true', NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'SUB_AREA', 'account_tender', 'nonchange_tender', 'Accounts Receivable', 30, 'true', NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'SUB_AREA', 'homeofficecheck_tender', 'nonchange_tender', 'Home Office Check', 40, 'true', NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'SUB_AREA', 'coupon_tender', 'nonchange_tender', 'Coupon', 50, 'true', NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'SUB_AREA', 'foreigncurrency_tender', 'nonchange_tender', 'Foreign Currency', 60, 'true', NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'SUB_AREA', 'misc_tender', 'nonchange_tender', 'Misc Tender', 70, 'true', NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'SUB_AREA', 'paydeduct_tender', 'nonchange_tender', 'Payroll Deduction', 80, 'true', NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'RcptConfig' AND sub_category = 'footer';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'SUB_AREA', 'YouSavedMessage', 'footer', 'You Save Message', 10, 'false', NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'SUB_AREA', 'soldItemsCount', 'footer', 'Sold Item Count', 20, 'false', NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'SUB_AREA', 'returnedItemsCount', 'footer', 'Return Item Count', 30, 'false', NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'SUB_AREA', 'trans_barcode', 'footer', 'Transaction Barcode', 40, 'false', NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'SUB_AREA', 'transaction_store_location', 'footer', 'Store Location', 50, 'true', NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'SUB_AREA', 'logo', 'footer', 'Logo', 60, 'false', NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'RcptConfig' AND sub_category = 'logo';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'picture', 'filename', 'logo', 'File Name', 10, 'graphic', '/xcenter-admin/images/logo.gif', NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'RcptConfig' AND sub_category = 'transaction_store_location';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getStoreName', 'transaction_store_location', 'Store Name', 10, 'text', 'Store Name', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getAddress1', 'transaction_store_location', 'Address 1', 20, 'text', '12345 Store Stree', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getAddress2', 'transaction_store_location', 'Address 2', 30, 'text', NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getCity', 'transaction_store_location', 'City', 40, 'text', 'CityName', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getState', 'transaction_store_location', 'State', 60, 'text', 'OH', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getPostalCode', 'transaction_store_location', 'Zip Code', 70, 'text', '55555', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getTelephone1', 'transaction_store_location', 'Phone 1', 80, 'text', '(555)555-5555', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getRetailLocationId', 'transaction_store_location', 'Store Number', 90, 'text', '101', NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'RcptConfig' AND sub_category = 'trans_barcode';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'barcode', 'dtv.hardware.barcode.TranBarcodeDocBuilderField', 'trans_barcode', 'Barcode', 10, 'graphic', '/xcenter-admin/images/barcode.png', 'Code 93');
GO

DELETE FROM cfg_code_value WHERE category = 'RcptConfig' AND sub_category = 'transaction_header_info';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'translation', '_ticket', 'transaction_header_info', 'Trans Seq Label', 10, 'translation', '_ticket', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getTransactionSequence', 'transaction_header_info', 'Trans Seq', 20, 'text', '1111', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'translation', '_date', 'transaction_header_info', 'Business Date Label', 30, 'translation', '_date', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getBusinessDate', 'transaction_header_info', 'Business Date', 40, 'text', '9/15/08', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'translation', '_store', 'transaction_header_info', 'Store Number Label', 50, 'translation', '_store', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getRetailLocationId', 'transaction_header_info', 'Store Number', 60, 'text', '101', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'translation', '_register', 'transaction_header_info', 'Reg Number Label', 70, 'translation', '_register', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getWorkstationId', 'transaction_header_info', 'Reg Number', 80, 'text', '1', NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'RcptConfig' AND sub_category = 'SALES_PERSON_INFO';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'translation', '_salesperson', 'SALES_PERSON_INFO', 'Sales Person Label', 10, 'translation', '_salesperson', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'Class', 'dtv.pos.common.rcpt.TranSalespersonBuilderField', 'SALES_PERSON_INFO', 'Sales Person List', 20, 'text', '100 (John Smith)', NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'RcptConfig' AND sub_category = 'transaction_cashier';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'translation', '_cashier', 'transaction_cashier', 'Cashier Label', 10, 'translation', '_cashier', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getOperatorParty.getEmployeeId', 'transaction_cashier', 'Cashier', 20, 'text', '200', NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'RcptConfig' AND sub_category = 'item_header';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'translation', '_item', 'item_header', 'Item Id Label', 10, 'translation', '_item', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'translation', '_qty', 'item_header', 'Qty Label', 20, 'translation', '_qty', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'translation', '_price', 'item_header', 'Unit Price Label', 30, 'translation', '_price', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'translation', '_amount', 'item_header', 'Extended Amt Label', 40, 'translation', '_amount', NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'RcptConfig' AND sub_category = 'nonvoided_item';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getItemDescription', 'nonvoided_item', 'Item Description', 10, 'text', 'Item Description', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'text', ' ', 'nonvoided_item', 'Space', 20, 'text', ' ', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getItem.getItemId', 'nonvoided_item', 'Item Id', 30, 'text', '1234567', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getQuantity', 'nonvoided_item', 'Qty', 40, 'text', '2', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getBaseUnitPrice', 'nonvoided_item', 'Unit Price', 50, 'text', '10.10', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getExtendedAmount', 'nonvoided_item', 'Extended Amount', 60, 'text', '20.20', 'MONEY');
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'Class', 'dtv.pos.register.tax.TaxFlagDocBuilderField', 'nonvoided_item', 'Tax Flag', 70, 'text', '', NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'RcptConfig' AND sub_category = 'check_tender';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getTender.getDescription', 'check_tender', 'Tender Description', 10, 'text', 'Check', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getAmount', 'check_tender', 'Tender Amount', 20, 'text', '20.00', 'MONEY');
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getCheckSequenceNumber', 'check_tender', 'Check Seq Number', 30, 'text', '564', NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'RcptConfig' AND sub_category = 'voucher_tender';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getTender.getDescription', 'voucher_tender', 'Tender Description', 10, 'text', 'Gift Card', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getAmount.abs', 'voucher_tender', 'Tender Amount', 20, 'text', '20.00', 'MONEY');
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getSerialNumber', 'voucher_tender', 'Voucher Number', 30, 'text', '700312456665', 'VOUCHER_NUMBER');
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getAuthorizationCode', 'voucher_tender', 'Auth Code', 40, 'text', '5412', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getUnspentBalanceAmount', 'voucher_tender', 'Unspent Balance', 50, 'text', '65.12', 'MONEY');
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'translation', '_rcptVoucherBalance', 'voucher_tender', 'Unspent Balance Label', 60, 'translation', '_rcptVoucherBalance', NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'RcptConfig' AND sub_category = 'account_tender';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getTender.getDescription', 'account_tender', 'Tender Description', 10, 'text', 'Accounts Receivable', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getAmount', 'account_tender', 'Tender Amount', 20, 'text', '20.00', 'MONEY');
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getAccountNumber', 'account_tender', 'Account Number', 30, 'text', '554668845', 'CREDITCARD');
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'translation', '_accountReceivableRcptUserName', 'account_tender', 'Account User Name Label', 50, 'translation', '_accountReceivableRcptUserName', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getAccountUserName', 'account_tender', 'Account User Name', 60, 'text', 'Jane Smith', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'translation', '_accountReceivablePONumber', 'account_tender', 'Account PO Number Label', 70, 'translation', '_accountReceivablePONumber', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getPoNumber', 'account_tender', 'Account PO Number', 80, 'text', '123545', NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'RcptConfig' AND sub_category = 'homeofficecheck_tender';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getTender.getDescription', 'homeofficecheck_tender', 'Tender Description', 10, 'text', 'Home Office Check', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getAmount', 'homeofficecheck_tender', 'Tender Amount', 20, 'text', '20.00', 'MONEY');
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getSendCheckReasonCode', 'homeofficecheck_tender', 'Send Check Reason Code', 30, 'text', 'Refund', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'translation', '_sendCheckToRcptText', 'homeofficecheck_tender', 'Send To Label', 40, 'translation', '_sendCheckToRcptText', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getPayableToName', 'homeofficecheck_tender', 'Payable To Name', 50, 'text', 'Joe Jones', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getPayableToAddress', 'homeofficecheck_tender', 'Payable To Address 1', 60, 'text', '555 Customer Drive', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getPayableToAddress2', 'homeofficecheck_tender', 'Payable To Address 2', 70, 'text', NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getPayableToCity', 'homeofficecheck_tender', 'Payable To City', 80, 'text', 'City', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getPayableToState', 'homeofficecheck_tender', 'Payable To State', 90, 'text', 'State', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getPayableToPostalCode', 'homeofficecheck_tender', 'Payable To Zip', 100, 'text', '66445', NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'RcptConfig' AND sub_category = 'coupon_tender';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getTender.getDescription', 'coupon_tender', 'Tender Description', 10, 'text', 'Coupon', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getAmount', 'coupon_tender', 'Tender Amount', 20, 'text', '20.00', 'MONEY');
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getSerialNumber', 'coupon_tender', 'Coupon Number', 30, 'text', '65541158', NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'RcptConfig' AND sub_category = 'misc_tender';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getTender.getDescription', 'misc_tender', 'Tender Description', 10, 'text', 'Misc Tender', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getAmount', 'misc_tender', 'Tender Amount', 20, 'text', '20.00', 'MONEY');
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getSerialNumber', 'misc_tender', 'Serial Number', 30, 'text', '6541125', NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'RcptConfig' AND sub_category = 'foreigncurrency_tender';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getTender.getDescription', 'foreigncurrency_tender', 'Tender Description', 10, 'text', 'Canadian Currency', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getAmount', 'foreigncurrency_tender', 'Tender Amount', 20, 'text', '20.00', 'MONEY');
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getForeignAmount', 'foreigncurrency_tender', 'Foreign Amount', 30, 'text', '21.52', 'MONEY');
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getExchangeRate', 'foreigncurrency_tender', 'Exchange Rate', 40, 'text', '0.95', 'MONEY');
GO

DELETE FROM cfg_code_value WHERE category = 'RcptConfig' AND sub_category = 'paydeduct_tender';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getTender.getDescription', 'paydeduct_tender', 'Tender Description', 10, 'text', 'Payroll Deduction', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'method', 'getAmount', 'paydeduct_tender', 'Tender Amount', 20, 'text', '20.00', 'MONEY');
GO

DELETE FROM cfg_code_value WHERE category = 'RcptConfig' AND sub_category = 'YouSavedMessage';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'translation', '_totalAmountSaved', 'YouSavedMessage', 'Total amount saved label', 10, 'translation', '_totalAmountSaved', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'translation', '_totalAmountSaved2', 'YouSavedMessage', 'Total amount saved', 20, 'translation', '_totalAmountSaved2', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'Class', 'dtv.pos.common.rcpt.TotalAmountSavedDocBuilderField', 'YouSavedMessage', 'Amount saved', 30, 'text', '25', NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'RcptConfig' AND sub_category = 'Receipt_Misc_text';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'text', ' ', 'Receipt_Misc_text', 'Space', 60, 'text', ' ', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'text', ':', 'Receipt_Misc_text', ':', 20, 'text', ':', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'text', ', ', 'Receipt_Misc_text', ', ', 50, 'text', ', ', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'text', '#', 'Receipt_Misc_text', '#', 30, 'text', '#', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'text', '@', 'Receipt_Misc_text', '@', 40, 'text', '@', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'text', '**********', 'Receipt_Misc_text', '**********', 50, 'text', '********************************************', NULL);
IF NOT EXISTS (SELECT 1 FROM cfg_code_value WHERE category = 'RcptConfig' AND sub_category = 'soldItemsCount' AND config_name = 'translation')
	INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
	  VALUES ('RcptConfig', 'translation', '_soldItemCount', 'soldItemsCount', 'Sold item count label', 10, 'translation', '_soldItemCount', NULL);
IF NOT EXISTS (SELECT 1 FROM cfg_code_value WHERE category = 'RcptConfig' AND sub_category = 'soldItemsCount' AND config_name = 'Class')
	INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
	  VALUES ('RcptConfig', 'Class', 'dtv.pos.common.rcpt.SoldItemsCountDocBuilderField', 'soldItemsCount', 'Item sold count', 20, 'text', '15', NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'RcptConfig' AND sub_category = 'returnedItemsCount';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'translation', '_returnedItemCount', 'returnedItemsCount', 'Return item count label', 10, 'translation', '_returnedItemCount', NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('RcptConfig', 'Class', 'dtv.pos.common.rcpt.ReturnedItemsCountDocBuilderField', 'returnedItemsCount', 'Item returned count', 20, 'text', '3', NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---RegisterConfig---Browser---Type';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---Browser---Type', 'JDIC', 'System', '_JDICBrowser', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---Browser---Type', 'IE', 'System', '_IEBrowser', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---RegisterConfig---CommissionedAssociates---DefaultCommissionMethod';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---CommissionedAssociates---DefaultCommissionMethod', 'CURRENT_CASHIER', 'System', '_CURRENT_CASHIER', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---CommissionedAssociates---DefaultCommissionMethod', 'HOUSE_ACCOUNT', 'System', '_HOUSE_ACCOUNT', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---CommissionedAssociates---DefaultCommissionMethod', 'NONE', 'System', '_NO_COMM_ASSOCIATE_FOR_RETURN', 30, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---RegisterConfig---Location---OvertimeRuleType';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---Location---OvertimeRuleType', 'WEEKLYOVER40', 'Store', '_WEEKLYOVER40', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---Location---OvertimeRuleType', 'DAILYOVER8', 'Store', '_DAILYOVER8', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---Location---OvertimeRuleType', 'CALIFORNIA', 'Store', '_CALIFORNIA', 30, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---Location---OvertimeRuleType', 'MASSACHUSETTS', 'Store', '_MASSACHUSETTS', 40, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---Location---OvertimeRuleType', 'RHODE_ISLAND', 'Store', '_RHODE_ISLAND', 50, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---Location---OvertimeRuleType', 'WISCONSIN', 'Store', '_WISCONSIN', 60, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---Location---OvertimeRuleType', 'KENTUCKY', 'Store', '_KENTUCKY', 70, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---Location---OvertimeRuleType', 'PR', 'Store', '_PR', 80, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---Location---OvertimeRuleType', 'MB_CANADA', 'Store', '_MB_CANADA', 90, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---Location---OvertimeRuleType', 'NS_CANADA', 'Store', '_NS_CANADA', 100, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---Location---OvertimeRuleType', 'ON_CANADA', 'Store', '_ON_CANADA', 110, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---Location---OvertimeRuleType', 'PE_CANADA', 'Store', '_PE_CANADA', 120, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---Location---OvertimeRuleType', 'AB_CANADA', 'Store', '_AB_CANADA', 130, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---Location---OvertimeRuleType', 'BC_CANADA', 'Store', '_BC_CANADA', 140, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---Location---OvertimeRuleType', 'NB_CANADA', 'Store', '_NB_CANADA', 150, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---Location---OvertimeRuleType', 'SK_CANADA', 'Store', '_SK_CANADA', 160, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---RegisterConfig---OpenClose---AutoClockOutOnRtlLocClose';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---OpenClose---AutoClockOutOnRtlLocClose', 'CLOCK_OUT_ALL', 'Open Close', '_CLOCK_OUT_ALL', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---OpenClose---AutoClockOutOnRtlLocClose', 'CLOCK_OUT_CURRENT_EMP', 'Open Close', '_CLOCK_OUT_CURRENT_EMP', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---OpenClose---AutoClockOutOnRtlLocClose', 'NONE', 'Open Close', '_NO_CLOCK_OUT_EMP_AT_CLOSE', 30, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---RegisterConfig---OpenClose---StartupApplication';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---OpenClose---StartupApplication', 'REGISTER', 'Open Close', '_registerApp', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---OpenClose---StartupApplication', 'BACK_OFFICE', 'Open Close', '_backOfficeApp', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---RegisterConfig---RestrictSaleType';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---RestrictSaleType', 'SALE', 'Register', '_sale', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---RestrictSaleType', 'RETURN', 'Register', '_RETURN', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---RestrictSaleType', 'LAYAWAY', 'Register', '_layaway', 30, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---RestrictSaleType', 'SPECIAL ORDER', 'Register', '_specialOrder', 40, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---RestrictSaleType', 'NONE', 'Register', '_noRestriction', 50, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---RestrictSaleType', 'CUSTOMER_BACKORDER', 'Register', '_CUSTOMER_BACKORDER', 60, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---RestrictSaleType', 'WORK_ORDER', 'Register', '_workOrder', 70, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---RestrictSaleType', 'SHIP_SALE', 'Register', '_sendSale', 80, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---RestrictSaleType', 'REMOTE_SEND', 'Register', '_remoteSend', 90, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---RegisterConfig---SaleEndingChain';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---SaleEndingChain', 'PRE_SALE_TRANSACTION', 'Register', '_PRE_SALE_TRANSACTION', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---RegisterConfig---SaleEndingChain', 'REGISTER_LOGIN', 'Register', '_REGISTER_LOGIN', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---AccountsReceivable---PromptUserInfoMethod';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---AccountsReceivable---PromptUserInfoMethod', 'PROMPT_NAME', 'Accounts Receivable', '_promptARName', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---AccountsReceivable---PromptUserInfoMethod', 'PROMPT_ELEGIBLE_USERS_LIST', 'Accounts Receivable', '_PROMPT_ELEGIBLE_USERS_LIST', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---CurrencyRounding---RoundingMode';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---CurrencyRounding---RoundingMode', 'ROUND_HALF_UP', 'Tender', '_ROUND_HALF_UP', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---CurrencyRounding---RoundingMode', 'ROUND_HALF_DOWN', 'Tender', '_ROUND_HALF_DOWN', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---CurrencyRounding---RoundingMode', 'ROUND_UP', 'Tender', '_ROUND_UP', 30, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---CurrencyRounding---RoundingMode', 'ROUND_DOWN', 'Tender', '_ROUND_DOWN', 40, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---CurrencyRounding---RoundingMode', 'ROUND_CEILING', 'Tender', '_ROUND_CEILING', 50, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---CurrencyRounding---RoundingMode', 'ROUND_FLOOR', 'Tender', '_ROUND_FLOOR', 60, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---CurrencyRounding---RoundingMode', 'ROUND_HALF_EVEN', 'Tender', '_ROUND_HALF_EVEN', 70, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---CurrencyRounding---RoundingMode', 'ROUND_UNNECESSARY', 'Tender', '_ROUND_UNNECESSARY', 80, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---DefaultIDType';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---DefaultIDType', 'DRIVER_LICENSE', 'Tender', '_DRIVER_LICENSE', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---DefaultIDType', 'SOCIAL_SECURITY', 'Tender', '_SOCIAL_SECURITY', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---DefaultIDType', 'MILITARY_ID', 'Tender', '_MILITARY_ID', 30, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---DefaultIDType', 'CREDIT_CARD', 'Tender', '_CREDIT_CARD', 40, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---DefaultIDType', 'EMPLOYEE_ID', 'Tender', '_EMPLOYEE_ID', 50, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---DefaultIDType', 'CUSTOMER_CARD', 'Tender', '_CUSTOMER_CARD', 60, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---DefaultIDType', 'ANY', 'Tender', '_ANYID', 70, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---Discounts---RoundingMode';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Discounts---RoundingMode', 'ROUND_HALF_UP', 'Discount', '_ROUND_HALF_UP', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Discounts---RoundingMode', 'ROUND_HALF_DOWN', 'Discount', '_ROUND_HALF_DOWN', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Discounts---RoundingMode', 'ROUND_UP', 'Discount', '_ROUND_UP', 30, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Discounts---RoundingMode', 'ROUND_DOWN', 'Discount', '_ROUND_DOWN', 40, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Discounts---RoundingMode', 'ROUND_CEILING', 'Discount', '_ROUND_CEILING', 50, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Discounts---RoundingMode', 'ROUND_FLOOR', 'Discount', '_ROUND_FLOOR', 60, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Discounts---RoundingMode', 'ROUND_HALF_EVEN', 'Discount', '_ROUND_HALF_EVEN', 70, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Discounts---RoundingMode', 'ROUND_UNNECESSARY', 'Discount', '_ROUND_UNNECESSARY', 80, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---ItemLookup---DepartmentChild';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemLookup---DepartmentChild', 'SubDepartmentId', 'Item Lookup', '_SubDepartmentId', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemLookup---DepartmentChild', 'ClassId', 'Item Lookup', '_ClassId', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---ItemMessaging---CompositeMessageImagePosition';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemMessaging---CompositeMessageImagePosition', 'top', 'Item Messaging', '_top', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemMessaging---CompositeMessageImagePosition', 'bottom', 'Item Messaging', '_bottom', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemMessaging---CompositeMessageImagePosition', 'right', 'Item Messaging', '_right', 30, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemMessaging---CompositeMessageImagePosition', 'left', 'Item Messaging', '_left', 40, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---ItemReturn---CreditCommissionedAsscMethod---CreditBlindReturnSaleCommissionMethod';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---CreditCommissionedAsscMethod---CreditBlindReturnSaleCommissionMethod', 'ORIGINAL_COMMISSIONED_ASSC', 'Return', '_ORIGINAL_COMMISSIONED_ASSC', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---CreditCommissionedAsscMethod---CreditBlindReturnSaleCommissionMethod', 'CURRENT_CASHIER', 'Return', '_CURRENT_CASHIER', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---CreditCommissionedAsscMethod---CreditBlindReturnSaleCommissionMethod', 'CURRENT_COMMISSIONED_ASSC', 'Return', '_CURRENT_COMMISSIONED_ASSC', 30, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---CreditCommissionedAsscMethod---CreditBlindReturnSaleCommissionMethod', 'HOUSE_ACCOUNT', 'Return', '_HOUSE_ACCOUNT', 40, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---ItemReturn---CreditCommissionedAsscMethod---CreditUnverifiedReturnSaleCommissionMethod';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---CreditCommissionedAsscMethod---CreditUnverifiedReturnSaleCommissionMethod', 'ORIGINAL_COMMISSIONED_ASSC', 'Return', '_ORIGINAL_COMMISSIONED_ASSC', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---CreditCommissionedAsscMethod---CreditUnverifiedReturnSaleCommissionMethod', 'CURRENT_CASHIER', 'Return', '_CURRENT_CASHIER', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---CreditCommissionedAsscMethod---CreditUnverifiedReturnSaleCommissionMethod', 'CURRENT_COMMISSIONED_ASSC', 'Return', '_CURRENT_COMMISSIONED_ASSC', 30, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---CreditCommissionedAsscMethod---CreditUnverifiedReturnSaleCommissionMethod', 'HOUSE_ACCOUNT', 'Return', '_HOUSE_ACCOUNT', 40, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---ItemReturn---CreditCommissionedAsscMethod---CreditVerifiedReturnSaleCommissionMethod';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---CreditCommissionedAsscMethod---CreditVerifiedReturnSaleCommissionMethod', 'ORIGINAL_COMMISSIONED_ASSC', 'Return', '_ORIGINAL_COMMISSIONED_ASSC', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---CreditCommissionedAsscMethod---CreditVerifiedReturnSaleCommissionMethod', 'CURRENT_CASHIER', 'Return', '_CURRENT_CASHIER', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---CreditCommissionedAsscMethod---CreditVerifiedReturnSaleCommissionMethod', 'CURRENT_COMMISSIONED_ASSC', 'Return', '_CURRENT_COMMISSIONED_ASSC', 30, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---CreditCommissionedAsscMethod---CreditVerifiedReturnSaleCommissionMethod', 'HOUSE_ACCOUNT', 'Return', '_HOUSE_ACCOUNT', 40, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---ItemReturn---ProrationTimeUnit';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---ProrationTimeUnit', 'DAY', 'Return', '_ProrationTimeUnitDAY', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemReturn---ProrationTimeUnit', 'MONTH', 'Return', '_ProrationTimeUnitMONTH', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---ItemSale---VerifyItemOnHand';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemSale---VerifyItemOnHand', 'ALWAYS', 'Sale', '_ALWAYS', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemSale---VerifyItemOnHand', 'NEVER', 'Sale', '_Never', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---ItemSale---VerifyItemOnHand', 'FLAGGED_ITEMS', 'Sale', '_FLAGGED_ITEMS', 30, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---Layaway---AccountType';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---AccountType', 'SINGLE', 'Layaway', '_singleLayawayAccountType', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---AccountType', 'MULTIPLE', 'Layaway', '_multipleLayawayAccountType', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---Layaway---DelinquencyFeeType';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---DelinquencyFeeType', 'TRANSACTION', 'Layaway', '_transactionLayawaySetupFeeType', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---DelinquencyFeeType', 'PCT_TOTAL', 'Layaway', '_totalBaseLayawayFeeType', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---DelinquencyFeeType', 'PCT_DELINQUENT', 'Layaway', '_delinquentBaseLayawayFeeType', 30, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---Layaway---DepositFeeType';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---DepositFeeType', 'TRANSACTION', 'Layaway', '_transactionLayawaySetupFeeType', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---DepositFeeType', 'ITEM', 'Layaway', '_itemLayawaySetupFeeType', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---Layaway---RestockingFeeType';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---RestockingFeeType', 'AMT', 'Layaway', '_fixedAmount', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---RestockingFeeType', 'PCT', 'Layaway', '_percentOfItemTotal', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---Layaway---SetupFeeType';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---SetupFeeType', 'TRANSACTION', 'Layaway', '_transactionLayawaySetupFeeType', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Layaway---SetupFeeType', 'ITEM', 'Layaway', '_itemLayawaySetupFeeType', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---LocalCurrencyId';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LocalCurrencyId', 'USD', 'Tender', '_USD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LocalCurrencyId', 'CAD', 'Tender', '_CAD', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LocalCurrencyId', 'EUR', 'Tender', '_EUR', 30, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LocalCurrencyId', 'GBP', 'Tender', '_GBP', 40, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---LocationBasedInventory---InventoryLocationPromptMethod';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LocationBasedInventory---InventoryLocationPromptMethod', 'NEVER', 'Inventory', '_neverPrompt', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LocationBasedInventory---InventoryLocationPromptMethod', 'AMBIGUOUS', 'Inventory', '_promptOfMoreThanOne', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LocationBasedInventory---InventoryLocationPromptMethod', 'ALWAYS', 'Inventory', '_alwaysPrompt', 30, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---LocationBasedInventory---InventoryMethod';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LocationBasedInventory---InventoryMethod', 'LIFO', 'Inventory', '_LIFO', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LocationBasedInventory---InventoryMethod', 'FIFO', 'Inventory', '_FIFO', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---LocationBasedInventory---InventoryValueTrackingMethod';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LocationBasedInventory---InventoryValueTrackingMethod', 'RETAIL', 'Inventory', '_retailPriceItem', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LocationBasedInventory---InventoryValueTrackingMethod', 'COST', 'Inventory', '_actualPriceItem', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---LocationBasedInventory---QuantityUnavailableAction';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LocationBasedInventory---QuantityUnavailableAction', 'ALLOW', 'Inventory', '_QuantityUnavailableActionALLOW', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LocationBasedInventory---QuantityUnavailableAction', 'PROHIBIT', 'Inventory', '_QuantityUnavailableActionPROHIBIT', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LocationBasedInventory---QuantityUnavailableAction', 'OVERRIDE', 'Inventory', '_QuantityUnavailableActionOVERRIDE', 30, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---LoginSecurity---IdType';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LoginSecurity---IdType', 'employee_id', 'Security', '_employeeId', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---LoginSecurity---IdType', 'login_id', 'Security', '_loginId', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---NonMerchItemTypes---IncludeItemCounts';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---NonMerchItemTypes---IncludeItemCounts', 'VOUCHER', 'Non Physical Item', '_voucher', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---NonMerchItemTypes---IncludeItemCounts', 'RESTOCKING_FEE', 'Non Physical Item', '_restockingfee', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---NonMerchItemTypes---IncludeItemCounts', 'LAYAWAY_SETUP_FEE', 'Non Physical Item', '_LAYAWAY_SETUP_FEE', 30, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---NonMerchItemTypes---IncludeItemCounts', 'LAYAWAY_DEPOSIT', 'Non Physical Item', '_LAYAWAY_DEPOSIT', 40, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---NonMerchItemTypes---IncludeItemCounts', 'LAYAWAY_PAYMENT', 'Non Physical Item', '_LAYAWAY_PAYMENT', 50, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---NonMerchItemTypes---IncludeItemCounts', 'LAYAWAY_DELINQUENCY_FEE', 'Non Physical Item', '_LAYAWAY_DELINQUENCY_FEE', 60, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---NonMerchItemTypes---IncludeItemCounts', 'LAYAWAY_RESTOCKING_FEE', 'Non Physical Item', '_LAYAWAY_RESTOCKING_FEE', 70, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---NonMerchItemTypes---IncludeItemCounts', 'SP_ORDER_SERVICE_FEE', 'Non Physical Item', '_SP_ORDER_SERVICE_FEE', 80, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---NonMerchItemTypes---IncludeItemCounts', 'SP_ORDER_DEPOSIT', 'Non Physical Item', '_SP_ORDER_DEPOSIT', 90, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---NonMerchItemTypes---IncludeItemCounts', 'SP_ORDER_PAYMENT', 'Non Physical Item', '_SP_ORDER_PAYMENT', 100, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---NonMerchItemTypes---IncludeItemCounts', 'SP_ORDER_SHIPPING_FEE', 'Non Physical Item', '_SP_ORDER_SHIPPING_FEE', 110, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---NonMerchItemTypes---IncludeItemCounts', 'SP_ORDER_RESTOCKING_FEE', 'Non Physical Item', '_SP_ORDER_RESTOCKING_FEE', 120, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---NonMerchItemTypes---IncludeItemCounts', 'WORK_ORDER', 'Non Physical Item', '_workOrder', 130, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---NonMerchItemTypes---IncludeItemCounts', 'WORK_ORDER_DEPOSIT', 'Non Physical Item', '_WORK_ORDER_DEPOSIT', 140, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---NonMerchItemTypes---IncludeItemCounts', 'SENDSALE_SHIPPING_FEE', 'Non Physical Item', '_SENDSALE_SHIPPING_FEE', 150, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---NonMerchItemTypes---IncludeItemCounts', 'DEFAULT_SPECIAL_ORDER', 'Non Physical Item', '_DEFAULT_SPECIAL_ORDER', 160, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---NonMerchItemTypes---IncludeItemCounts', 'LOYALTY', 'Non Physical Item', '_LOYALTY', 170, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---NonMerchItemTypes---IncludeItemCounts', 'AR_PAYMENT', 'Non Physical Item', '_AR_PAYMENT', 180, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---NonMerchItemTypes---IncludeItemCounts', 'CREDIT_PAYMENT', 'Non Physical Item', '_CREDIT_PAYMENT', 190, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---Payroll---BasicCalendarPayrollWeekendingDay';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Payroll---BasicCalendarPayrollWeekendingDay', '1', 'Payroll', '_Sunday', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Payroll---BasicCalendarPayrollWeekendingDay', '2', 'Payroll', '_Monday', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Payroll---BasicCalendarPayrollWeekendingDay', '3', 'Payroll', '_Tuesday', 30, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Payroll---BasicCalendarPayrollWeekendingDay', '4', 'Payroll', '_Wednesday', 40, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Payroll---BasicCalendarPayrollWeekendingDay', '5', 'Payroll', '_Thursday', 50, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Payroll---BasicCalendarPayrollWeekendingDay', '6', 'Payroll', '_Friday', 60, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Payroll---BasicCalendarPayrollWeekendingDay', '7', 'Payroll', '_Saturday', 70, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---Payroll---PayrollHoursRoundingMinutes';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Payroll---PayrollHoursRoundingMinutes', '15', 'Payroll', '_15Minutes', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Payroll---PayrollHoursRoundingMinutes', '30', 'Payroll', '_30Minutes', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Payroll---PayrollHoursRoundingMinutes', '60', 'Payroll', '_60Minutes', 30, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Payroll---PayrollHoursRoundingMinutes', 'None', 'Payroll', '_none', 40, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---Payroll---PostPayroll---PayrollPostDay';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Payroll---PostPayroll---PayrollPostDay', '1', 'Payroll', '_Sunday', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Payroll---PostPayroll---PayrollPostDay', '2', 'Payroll', '_Monday', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Payroll---PostPayroll---PayrollPostDay', '3', 'Payroll', '_Tuesday', 30, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Payroll---PostPayroll---PayrollPostDay', '4', 'Payroll', '_Wednesday', 40, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Payroll---PostPayroll---PayrollPostDay', '5', 'Payroll', '_Thursday', 50, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Payroll---PostPayroll---PayrollPostDay', '6', 'Payroll', '_Friday', 60, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Payroll---PostPayroll---PayrollPostDay', '7', 'Payroll', '_Saturday', 70, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---Receiving---ReceivingDocumentIdRule';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Receiving---ReceivingDocumentIdRule', 'AUTOGENERATED', 'Receiving', '_AUTOGENERATED', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Receiving---ReceivingDocumentIdRule', 'ENTERED', 'Receiving', '_ENTERED', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Receiving---ReceivingDocumentIdRule', 'ANY', 'Receiving', '_SYSTEM_PROMPT', 30, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---RemoteSend---SendTaxType';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---RemoteSend---SendTaxType', 'DESTINATION', 'Remote Send', '_DESTINATIONSendTaxType', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---RemoteSend---SendTaxType', 'SELLING', 'Remote Send', '_SELLINGSendTaxType', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---RemoteSend---SendTaxType', 'DEST_INSTATE', 'Remote Send', '_DEST_INSTATESendTaxType', 30, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---SendSale---DefaultExternalOrignatorCode';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SendSale---DefaultExternalOrignatorCode', 'UNKNOWN', 'Send Sale', '_unknown', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SendSale---DefaultExternalOrignatorCode', 'STORE', 'Send Sale', '_sendSaleStore', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SendSale---DefaultExternalOrignatorCode', 'WAREHOUSE', 'Send Sale', '_sendSaleWareHouse', 30, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---SendSale---DefaultStoreOrignatorCode';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SendSale---DefaultStoreOrignatorCode', 'STORE', 'Send Sale', '_sendSaleStore', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SendSale---DefaultStoreOrignatorCode', 'OTHER', 'Send Sale', '_other', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---SendSale---SendTaxType';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SendSale---SendTaxType', 'DESTINATION', 'Send Sale', '_DESTINATIONSendTaxType', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SendSale---SendTaxType', 'SELLING', 'Send Sale', '_SELLINGSendTaxType', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SendSale---SendTaxType', 'DEST_INSTATE', 'Send Sale', '_DEST_INSTATESendTaxType', 30, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---SpecialOrder---DepositFeeType';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SpecialOrder---DepositFeeType', 'TRANSACTION', 'Special Order', '_transactionLayawaySetupFeeType', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SpecialOrder---DepositFeeType', 'ITEM', 'Special Order', '_itemLayawaySetupFeeType', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---SpecialOrder---RestockingFeeType';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SpecialOrder---RestockingFeeType', 'AMT', 'Special Order', '_fixedAmount', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SpecialOrder---RestockingFeeType', 'PCT', 'Special Order', '_percentOfItemTotal', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---SpecialOrder---ServiceFeeType';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SpecialOrder---ServiceFeeType', 'TRANSACTION', 'Special Order', '_transactionLayawaySetupFeeType', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SpecialOrder---ServiceFeeType', 'ITEM', 'Special Order', '_itemLayawaySetupFeeType', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---SpecialOrder---ShippingFeeType';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SpecialOrder---ShippingFeeType', 'TRANSACTION', 'Special Order', '_transactionLayawaySetupFeeType', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---SpecialOrder---ShippingFeeType', 'ITEM', 'Special Order', '_itemLayawaySetupFeeType', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---Tender---DefaultChangeTenderIdIfNoneFound';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Tender---DefaultChangeTenderIdIfNoneFound', 'ISSUE_STORE_CREDIT', 'Tender', '_ISSUE_STORE_CREDIT', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Tender---DefaultChangeTenderIdIfNoneFound', 'ISSUE_XPAY_GIFT_CARD', 'Tender', '_ISSUE_XPAY_GIFT_CARD', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---TillAccountability---CashTransferCountMethod';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---CashTransferCountMethod', 'TOTAL_NORMAL', 'Till', '_TOTAL_NORMAL', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---CashTransferCountMethod', 'DENOMINATION', 'Till', '_DENOMINATION', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---TillAccountability---CountSummaryViewTypeCashPickup';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---CountSummaryViewTypeCashPickup', 'MINIMUM', 'Till', '_MINIMUMCashPickup', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---CountSummaryViewTypeCashPickup', 'SIMPLE', 'Till', '_SIMPLECashPickup', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---CountSummaryViewTypeCashPickup', 'DETAILED', 'Till', '_DETAILEDCashPickup', 30, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---CountSummaryViewTypeCashPickup', 'FULL', 'Till', '_FULLCashPickup', 40, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---TillAccountability---CountSummaryViewTypeMidDay';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---CountSummaryViewTypeMidDay', 'MINIMUM', 'Till', '_MINIMUMTillView', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---CountSummaryViewTypeMidDay', 'SIMPLE', 'Till', '_SIMPLETillView', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---CountSummaryViewTypeMidDay', 'DETAILED', 'Till', '_DETAILEDTillView', 30, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---TillAccountability---CountSummaryViewTypeReconcile';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---CountSummaryViewTypeReconcile', 'MINIMUM', 'Till', '_MINIMUMTillView', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---CountSummaryViewTypeReconcile', 'SIMPLE', 'Till', '_SIMPLETillView', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---CountSummaryViewTypeReconcile', 'DETAILED', 'Till', '_DETAILEDTillView', 30, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---TillAccountability---CountSummaryViewTypeStartCount';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---CountSummaryViewTypeStartCount', 'MINIMUM', 'Till', '_MINIMUMCashPickup', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---CountSummaryViewTypeStartCount', 'SIMPLE', 'Till', '_SIMPLECashPickup', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---CountSummaryViewTypeStartCount', 'DETAILED', 'Till', '_DETAILEDCashPickup', 30, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---CountSummaryViewTypeStartCount', 'FULL', 'Till', '_FULLCashPickup', 40, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---TillAccountability---CountSummaryViewTypeTillCount';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---CountSummaryViewTypeTillCount', 'MINIMUM', 'Till', '_MINIMUMTillView', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---CountSummaryViewTypeTillCount', 'SIMPLE', 'Till', '_SIMPLETillView', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---CountSummaryViewTypeTillCount', 'DETAILED', 'Till', '_DETAILEDTillView', 30, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---TillAccountability---StartTillCountMethod';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---StartTillCountMethod', 'TOTAL_NORMAL', 'Till', '_TOTAL_NORMAL', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---StartTillCountMethod', 'DENOMINATION', 'Till', '_DENOMINATION', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---TillAccountability---StoreBankCashDepositCountMethod';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---StoreBankCashDepositCountMethod', 'TOTAL_NORMAL', 'Till', '_TOTAL_NORMAL', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---StoreBankCashDepositCountMethod', 'DENOMINATION', 'Till', '_DENOMINATION', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---TillAccountability---StoreBankEndCountAtStoreCloseMethod';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---StoreBankEndCountAtStoreCloseMethod', 'COMPLETE_DEPOSIT', 'Till', '_COMPLETE_DEPOSIT', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---StoreBankEndCountAtStoreCloseMethod', 'REMAINING_CASH_PROMPT', 'Till', '_REMAINING_CASH_PROMPT', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---StoreBankEndCountAtStoreCloseMethod', 'STOREBANK_COMPLETE_ENDCOUNT', 'Till', '_STOREBANK_COMPLETE_ENDCOUNT', 30, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---TillAccountability---StoreBankOpeningCountMethod';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---StoreBankOpeningCountMethod', 'TOTAL_NORMAL', 'Till', '_TOTAL_NORMAL', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---StoreBankOpeningCountMethod', 'DENOMINATION', 'Till', '_DENOMINATION', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---TillAccountability---TillPickupMethod';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---TillPickupMethod', 'TOTAL_NORMAL', 'Till', '_TOTAL_NORMAL', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---TillAccountability---TillPickupMethod', 'DENOMINATION', 'Till', '_DENOMINATION', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---Timecard---DefaultActiveEmployeeListSortType';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Timecard---DefaultActiveEmployeeListSortType', 'LAST_NAME', 'Timecard', '_LAST_NAME', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Timecard---DefaultActiveEmployeeListSortType', 'DEPARTMENT_ID', 'Timecard', '_DEPARTMENT_ID', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Timecard---DefaultActiveEmployeeListSortType', 'EMPLOYEE_ID', 'Timecard', '_EMPLOYEE_ID', 30, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---Timecard---TimecardEntryRoundingMinutes';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Timecard---TimecardEntryRoundingMinutes', '15', 'Timecard', '_15Minutes', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Timecard---TimecardEntryRoundingMinutes', '30', 'Timecard', '_30Minutes', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Timecard---TimecardEntryRoundingMinutes', '60', 'Timecard', '_60Minutes', 30, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---Timecard---TimecardEntryRoundingMinutes', 'None', 'Timecard', '_none', 40, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---VoucherNumberMask';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---VoucherNumberMask', 'CREDITCARD', 'Voucher', '_VoucherNumberMaskCREDITCARD', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---VoucherNumberMask', 'MASK_LAST_4', 'Voucher', '_VoucherNumberMaskMASK_LAST_4', 20, NULL, NULL, NULL);
GO

DELETE FROM cfg_code_value WHERE category = 'SYSTEM_CONFIG' AND config_name = 'Store---SystemConfig---WorkOrder---WorkItemSerialPromptMethod';
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---WorkOrder---WorkItemSerialPromptMethod', 'ALWAYS', 'Work Order', '_WorkItemSerialPromptMethodALWAYS', 10, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---WorkOrder---WorkItemSerialPromptMethod', 'SERIALIZED', 'Work Order', '_WorkItemSerialPromptMethodSERIALIZED', 20, NULL, NULL, NULL);
INSERT INTO cfg_code_value (category, config_name, code, sub_category, description, sort_order, data1, data2, data3)
  VALUES ('SYSTEM_CONFIG', 'Store---SystemConfig---WorkOrder---WorkItemSerialPromptMethod', 'SERIALIZEDDUMMY', 'Work Order', '_WorkItemSerialPromptMethodSERIALIZEDDUMMY', 30, NULL, NULL, NULL);
GO


-- Tasks and messages
INSERT INTO cfg_sequence_part (organization_id, sequence_id, prefix, suffix, encode_flag, check_digit_algo, numeric_flag, pad_length, pad_character, initial_value, max_value, value_increment, include_store_id, store_pad_length, include_wkstn_id, wkstn_pad_length)
SELECT code, 'EMPLOYEE_MESSAGE', NULL, NULL, 0, NULL, 1, 9, '0', 1, 999999, 1, 0, 0, 0, 0
FROM cfg_code_value c
WHERE category = 'OrganizationId'
AND NOT EXISTS (
  SELECT TOP 1 * 
  FROM cfg_sequence_part p 
  WHERE p.organization_id = c.code AND p.sequence_id = 'EMPLOYEE_MESSAGE')
GO


INSERT INTO cfg_sequence_part (organization_id, sequence_id, prefix, suffix, encode_flag, check_digit_algo, numeric_flag, pad_length, pad_character, initial_value, max_value, value_increment, include_store_id, store_pad_length, include_wkstn_id, wkstn_pad_length)
SELECT code, 'EMPLOYEE_TASK', NULL, NULL, 0, NULL, 1, 9, '0', 1, 999999, 1, 0, 0, 0, 0
FROM cfg_code_value c
WHERE category = 'OrganizationId'
AND NOT EXISTS (
  SELECT TOP 1 * 
  FROM cfg_sequence_part p 
  WHERE p.organization_id = c.code AND p.sequence_id = 'EMPLOYEE_TASK')
GO

DELETE cfg_menu_config WHERE menu_name = 'EMP_MAINTENANCE' OR parent_menu_name = 'EMP_MAINTENANCE';
INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, title, menu_type, sort_order, view_id, action_expression, active_flag, config_type, security_privilege)
VALUES ('MAIN_MENU', 'EMP_MAINTENANCE', 'CONFIGURATOR_FEATURE', '_menutextEmpMaint', 'LINK_GROUP', 100, NULL, NULL, 1, NULL, NULL);

INSERT INTO cfg_menu_config ( category, menu_name, parent_menu_name, title, menu_type, sort_order, view_id, action_expression, active_flag, config_type, security_privilege)
VALUES ('MAIN_MENU', 'EMP_MESSAGE', 'EMP_MAINTENANCE', '_messageMaintenance', 'LINK', 10, '/configurator/hrs/empMessage.xhtml', NULL, 1, 'EMP_MESSAGE', 'CFG_EMPLOYEE_MESSAGE');

INSERT INTO cfg_menu_config (category, menu_name, parent_menu_name, title, menu_type, sort_order, view_id, action_expression, active_flag, config_type, security_privilege)
VALUES ('MAIN_MENU', 'EMP_TASK', 'EMP_MAINTENANCE', '_taskMaintenance', 'LINK', 20, '/configurator/hrs/empTask.xhtml', NULL, 1, 'EMP_TASK', 'CFG_EMPLOYEE_TASK');
GO

-- Alert severity thresholds 
DELETE FROM cfg_alert_severity_threshold;
INSERT INTO cfg_alert_severity_threshold (alert_type, medium_threshold, high_threshold, critical_threshold)
VALUES ('XCENTER_PERSISTENCE_FAILURE', 1, 1, 1);

INSERT INTO cfg_alert_severity_threshold (alert_type, medium_threshold, high_threshold, critical_threshold)
VALUES ('XSTORE_REPLICATION_BACKLOG', 5, 10, 15);

INSERT INTO cfg_alert_severity_threshold (alert_type, medium_threshold, high_threshold, critical_threshold)
VALUES ('XSTORE_REPLICATION_ERRORS', 5, 10, 15);

INSERT INTO cfg_alert_severity_threshold (alert_type, medium_threshold, high_threshold, critical_threshold)
VALUES ('XSTORE_REPLICATION_READERROR', 5, 10, 15);

INSERT INTO cfg_alert_severity_threshold (alert_type, medium_threshold, high_threshold, critical_threshold)
VALUES ('XCENTER_CLIENT_OFFLINE', 3, 5, 10);

INSERT INTO cfg_alert_severity_threshold (alert_type, medium_threshold, high_threshold, critical_threshold)
VALUES ('DATA_FAILOVER_OFFLINE', 3, 5, 10);

INSERT INTO cfg_alert_severity_threshold (alert_type, medium_threshold, high_threshold, critical_threshold)
VALUES ('PAYMENT_SYSTEM_OFFLINE', 3, 5, 10);

INSERT INTO cfg_alert_severity_threshold (alert_type, medium_threshold, high_threshold, critical_threshold)
VALUES ('SERVICE_OFFLINE', 3, 5, 10);

INSERT INTO cfg_alert_severity_threshold (alert_type, medium_threshold, high_threshold, critical_threshold)
VALUES ('ORDER_BROKER_SERVICE_OFFLINE', 3, 5, 10);

INSERT INTO cfg_alert_severity_threshold (alert_type, medium_threshold, high_threshold, critical_threshold)
VALUES ('ORDER_BROKER_SERVICE_ERROR', 3, 5, 10);

INSERT INTO cfg_alert_severity_threshold (alert_type, medium_threshold, high_threshold, critical_threshold)
VALUES ('PRE_FLIGHT_ERROR', 1, 2, 3);

INSERT INTO cfg_alert_severity_threshold (alert_type, medium_threshold, high_threshold, critical_threshold)
VALUES ('SSL_EXPIRATION', 1, 1, 1);

INSERT INTO cfg_alert_severity_threshold (alert_type, medium_threshold, high_threshold, critical_threshold)
VALUES ('HELPDESK_ERROR', 1, 1, 1);

INSERT INTO cfg_alert_severity_threshold (alert_type, medium_threshold, high_threshold, critical_threshold)
VALUES ('HELPDESK_OUT_OF_MEMORY', 1, 1, 1);

INSERT INTO cfg_alert_severity_threshold (alert_type, medium_threshold, high_threshold, critical_threshold)
VALUES ('MISTORE_CRASH', 1, 1, 1);

INSERT INTO cfg_alert_severity_threshold (alert_type, medium_threshold, high_threshold, critical_threshold)
VALUES ('MISTORE_LOW_MEMORY', 1, 1, 1);

INSERT INTO cfg_alert_severity_threshold (alert_type, medium_threshold, high_threshold, critical_threshold)
VALUES ('INTRADAY_RESTARTS', 3, 5, 10);

INSERT INTO cfg_alert_severity_threshold (alert_type, medium_threshold, high_threshold, critical_threshold)
VALUES ('MISSED_CLOSING', 3, 5, 10);

INSERT INTO cfg_alert_severity_threshold (alert_type, medium_threshold, high_threshold, critical_threshold)
VALUES ('HARDWARE_INIT_ERROR', 1, 2, 3);

INSERT INTO cfg_alert_severity_threshold (alert_type, medium_threshold, high_threshold, critical_threshold)
VALUES ('JPOS_ERROR', 3, 5, 10);

INSERT INTO cfg_alert_severity_threshold (alert_type, medium_threshold, high_threshold, critical_threshold)
VALUES ('LOW_DISK_SPACE', 1, 1, 1);

INSERT INTO cfg_alert_severity_threshold (alert_type, medium_threshold, high_threshold, critical_threshold)
VALUES ('UPDATE_SERVICE_OFFLINE', 1, 2, 3);

INSERT INTO cfg_alert_severity_threshold (alert_type, medium_threshold, high_threshold, critical_threshold)
VALUES ('DATALOADER_ERROR', 1, 2, 3);

INSERT INTO cfg_alert_severity_threshold (alert_type, medium_threshold, high_threshold, critical_threshold)
VALUES ('CONFIG_UPDATES_FAILURE', 1, 2, 3);

INSERT INTO cfg_alert_severity_threshold (alert_type, medium_threshold, high_threshold, critical_threshold)
VALUES ('XSTORE_VERSION_CONFLICT', 3, 5, 10);

INSERT INTO cfg_alert_severity_threshold (alert_type, medium_threshold, high_threshold, critical_threshold)
VALUES ('CONFIG_VERSION_CONFLICT', 3, 5, 10);

INSERT INTO cfg_alert_severity_threshold (alert_type, medium_threshold, high_threshold, critical_threshold)
VALUES ('XENV_VERSION_CONFLICT', 3, 5, 10);


-- **************************************************** --
-- * Always keep Default User Creation at end of file * --
-- **************************************************** --
-- DEFAULT USER
IF NOT EXISTS (SELECT 1 FROM cfg_user)
BEGIN
	INSERT INTO cfg_user (user_name, first_name, last_name, locale, role_id, org_code, org_value) VALUES ('1', 'Default', 'User', 'en_US', 'ADMINISTRATOR', '*', '*');

	DELETE FROM cfg_user_password WHERE user_name = '1';
	INSERT INTO cfg_user_password (user_name, password, effective_date) VALUES ( '1', 'tZxnvxlqR1gZHkL3ZnDOug==', '1970-01-01');

	DELETE FROM cfg_role WHERE role_id = 'ADMINISTRATOR';
	INSERT INTO cfg_role (role_id, role_desc, system_role_flag) VALUES ('ADMINISTRATOR', 'Administrator', 1);

	DELETE FROM cfg_role_privilege WHERE role_id = 'ADMINISTRATOR';
	INSERT INTO cfg_role_privilege (role_id, privilege_id, has_attributes_flag, allow_create_flag, allow_read_flag, allow_update_flag, allow_delete_flag)
	SELECT 'ADMINISTRATOR', privilege_id, 0, 0, 0, 0, 0 FROM cfg_privilege WHERE has_attributes_flag = 0;
	INSERT INTO cfg_role_privilege (role_id, privilege_id, has_attributes_flag, allow_create_flag, allow_read_flag, allow_update_flag, allow_delete_flag)
	SELECT 'ADMINISTRATOR', privilege_id, 1, 1, 1, 1, 1 FROM cfg_privilege WHERE has_attributes_flag = 1;
END
GO

-- Only Customer specific Sql belongs in this file --
